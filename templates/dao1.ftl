<#-- Template para gera��o de uma classe DAO (Data Access Object) -->
<#assign last_att_key = 0>
<#list atributos as a>
<#if a.isPk>
<#assign last_att_key = a_index>
</#if>
</#list>
/*
 *  Copyright (C) 2012 ${autor}
 *  sandrosilveira@gmail.com
 *
 *  Este programa foi desenvolvido para o trabalho de conclus�o de 
 *  curso apresentado como requisito parcial � obten��o do grau de
 *  Bacharel em Ci�ncia da Computa��o pela Universidade Feevale.
 *
 *  Este programa n�o pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autoriza��o do autor.
 * 
 */
package ${package};

import database.DBStaticConn;
import model.${entidade};
import model.${entidade}Pk;
import model.${entidade}DaoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Classe DAO (Data Access Object) para acesso a tabela ${tabela}
 *
 * @author ${autor}
 */
public class ${entidade}Dao {

    /**
     * Conex�o com o banco de dados
     */
    private Connection conexao;
    /**
     * M�todos de busca podem passar este valor para o m�todo JDBC setMaxRows
     */
    private int maxLinhas;
    /**
     * Indica se deve registrar trace de opera��es na tabela ${tabela}
     */
    private boolean registraTrace = false;
    /**
     * Comando Select padr�o para m�todos de busca da tabela ${tabela}
     */
    private final String SQL_SELECT = "SELECT <#list atributos as a>${a.coluna}<#if a_has_next>, </#if></#list> FROM ${tabela}";
    /**
     * Comando Insert padr�o para a tabela ${tabela}
     */
    protected final String SQL_INSERT = "INSERT INTO ${tabela} (<#list atributos as a>${a.coluna}<#if a_has_next>, </#if></#list>) VALUES (<#list atributos as a>?<#if a_has_next>, </#if></#list>)";
    /**
     * Comando Update padr�o para a tabela ${tabela}
     */
    protected final String SQL_UPDATE = "UPDATE ${tabela} SET <#list atributos as a>${a.coluna} = ?<#if a_has_next>, </#if></#list><#list primary_key as p><#if p_index = 0> WHERE </#if>${p.nome} = ?<#if p_has_next> AND </#if></#list>";
    /**
     * Comando Delete padr�o para a tabela ${tabela}
     */
    protected final String SQL_DELETE = "DELETE FROM ${tabela}<#list primary_key as p><#if p_index = 0> WHERE </#if>${p.nome} = ?<#if p_has_next> AND </#if></#list>";
    /**
     * Comando Delete para exclus�o de todos os dados da tabela ${tabela}
     */
    protected final String SQL_DELETE_ALL = "DELETE FROM ${tabela}";
<#list atributos as a>
    /**
     * Indice da coluna ${a.coluna}
     */
    protected static final int COLUNA_${a.coluna?upper_case} = ${a_index + 1};
<#if !a_has_next>
    /**
     * N�mero de colunas da tabela
     */
    protected static final int NRO_COLUNAS_TABELA = ${a_index + 1};
</#if>    
</#list>

    /**
     * Construtor padr�o
     */
    public ${entidade}Dao() {
    }
    
    /**
     * Construtor informando a conex�o com o banco de dados
     * @param conn Conex�o com o banco de dados
     */
    public ${entidade}Dao(Connection conexaoUsuario) {
        this.conexao = conexaoUsuario;
    }

    /**
     * Seta o n�mero m�ximo de colunas a retornar
     */
    public void setMaxRows(int maxRows) {
        this.maxLinhas = maxRows;
    }

    /**
     * Retorna o n�mero m�ximo de linhas a retornar
     */
    public int getMaxRows() {
        return maxLinhas;
    }

    /**
     * Indica que deve registrar trace de opera��es na tabela ${tabela}
     */
    public void setTraceOn() {
        this.registraTrace = true;
    }

    /**
     * Indica que n�o deve registrar trace de opera��es na tabela ${tabela} (default)
     */
    public void setTraceOff() {
        this.registraTrace = false;
    }

    /**
     * Registra trace de opera��es na tabela ${tabela}
     * @param Texto a registrar, mostrando o passo-a-passo de execu��o
     */
    private void trace(String texto) {
        if (registraTrace) {
            System.out.println(texto);
        }
    }

    /**
     * Insere uma nova linha na tabela ${tabela} 
     *
     * @param ${entidade?uncap_first} Refer�ncia para o objeto a inserir
     * @return Primary Key
     * @throws ${entidade}DaoException
     */
    public ${entidade}Pk insert(${entidade} ${entidade?uncap_first}) throws ${entidade}DaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : DBStaticConn.getConnection();
            stmt = conn.prepareStatement(SQL_INSERT);
<#list atributos as a>
            stmt.${a.metodo_set}(COLUNA_${a.coluna?upper_case}, ${entidade?uncap_first}.get${a.nome?cap_first}());
</#list>
            trace("Executando " + SQL_INSERT + " com: " + ${entidade?uncap_first}.toString());
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linha inclu�da em (" + (t2 - t1) + " ms)");
            return new ${entidade}Pk(<#list atributos as a><#if a.isPk>${entidade?uncap_first}.get${a.nome?cap_first}()<#if a_index < last_att_key>, </#if></#if></#list>);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ${entidade}DaoException("Falha ao inserir: " + ex.getMessage(), ex);
        } finally {
            DBStaticConn.close(stmt);
            if (!isConnSupplied) {
                DBStaticConn.close(conn);
            }
        }
    }

    /**
     * Atualiza uma linha na tabela ${tabela}
     * @param pk Primary key
     * @param ${entidade?uncap_first} Refer�ncia para o objeto a atualizar
     * @throws ${entidade}DaoException 
     */
    public void update(${entidade}Pk pk, ${entidade} ${entidade?uncap_first}) throws ${entidade}DaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : DBStaticConn.getConnection();
            trace("Executando " + SQL_UPDATE + " com: " + ${entidade?uncap_first}.toString());
            stmt = conn.prepareStatement(SQL_UPDATE);
<#list atributos as a>
            stmt.${a.metodo_set}(COLUNA_${a.coluna?upper_case}, ${entidade?uncap_first}.get${a.nome?cap_first}());
</#list>
<#list primary_key as p>
<#if p_index = 0>
            int indexKey = NRO_COLUNAS_TABELA;
</#if>
</#list>
<#list atributos as a>
<#if a.isPk>
            stmt.${a.metodo_set}(++indexKey, pk.get${a.nome?cap_first}());
</#if>
</#list>
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linha atualizada em (" + (t2 - t1) + " ms)");
        } catch (Exception _e) {
            _e.printStackTrace();
            throw new ${entidade}DaoException("Falha ao atualizar: " + _e.getMessage(), _e);
        } finally {
            DBStaticConn.close(stmt);
            if (!isConnSupplied) {
                DBStaticConn.close(conn);
            }
        }
    }

    /**
     * Deleta um linha da tabela ${tabela}
     * @param pk Primary Key
     * @throws ${entidade}DaoException
     */
    public void delete(${entidade}Pk pk) throws ${entidade}DaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : DBStaticConn.getConnection();
            trace("Executando " + SQL_DELETE + " com: " + pk.toString());
            stmt = conn.prepareStatement(SQL_DELETE);
            int indexKey = 0;
<#list atributos as a>
<#if a.isPk>
            stmt.${a.metodo_set}(++indexKey, pk.get${a.nome?cap_first}());
</#if>
</#list>
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linha exclu�da em (" + (t2 - t1) + " ms)");
        } catch (Exception _e) {
            _e.printStackTrace();
            throw new ${entidade}DaoException("Falha ao excluir: " + _e.getMessage(), _e);
        } finally {
            DBStaticConn.close(stmt);
            if (!isConnSupplied) {
                DBStaticConn.close(conn);
            }
        }
    }

    /**
     * Deleta todas as linhas da tabela ${tabela}
     * @throws ${entidade}DaoException 
     */
    public void deleteAll() throws ${entidade}DaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : DBStaticConn.getConnection();
            trace("Executando " + SQL_DELETE_ALL);
            stmt = conn.prepareStatement(SQL_DELETE_ALL);
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linhas exclu�das em (" + (t2 - t1) + " ms)");
        } catch (Exception _e) {
            _e.printStackTrace();
            throw new ${entidade}DaoException("Falha ao excluir todas as linhas da tabela: " + _e.getMessage(), _e);
        } finally {
            DBStaticConn.close(stmt);
            if (!isConnSupplied) {
                DBStaticConn.close(conn);
            }
        }
    }

    /**
     * Retorna uma linha da tabela ${tabela} pela Primary Key
     *
     * @param pk Primary Key
     * @return Objeto do tipo ${entidade}
     * @throws ${entidade}DaoException
     */
    public ${entidade} buscaPrimaryKey(${entidade}Pk pk) throws ${entidade}DaoException {
        return buscaPrimaryKey(<#list primary_key as p>pk.get${p.referencia?cap_first}()<#if p_has_next>, </#if></#list>);
    }

    /**
     * Retorna uma linha da tabela ${tabela} comparando os atributos da Primary Key
     *
<#list atributos as a>
<#if a.isPk>
     * @param ${a.nome} ${a.descricao} 
</#if>
</#list>
     * @return Array de objetos do tipo ${entidade}
     * @throws ${entidade}DaoException
     */
    public ${entidade} buscaPrimaryKey(<#list atributos as a><#if a.isPk>${a.tipo} ${a.nome}<#if a_index < last_att_key>, </#if></#if></#list>) throws ${entidade}DaoException {
        ${entidade} ${entidade?uncap_first}s[] = buscaPorSelectDinamico(SQL_SELECT + "<#list primary_key as p><#if p_index = 0> WHERE </#if>${p.nome} = ?<#if p_has_next> AND </#if></#list>", new Object[]{<#list atributos as a><#if a.isPk>new ${a.objeto_compat}(${a.nome})<#if a_index < last_att_key>, </#if></#if></#list>});
        return ${entidade?uncap_first}s.length == 0 ? null : ${entidade?uncap_first}s[0];
    }

    /**
     * Retorna linhas da tabela ${tabela} de acordo com a senten�a SQL
     * @param sql SQL a executar
     * @param sqlParams Par�metros
     * @return Array de objetos do tipo ${entidade}
     * @throws ${entidade}DaoException
     */
    public ${entidade}[] buscaPorSelectDinamico(String sql, Object[] sqlParams) throws ${entidade}DaoException {
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = isConnSupplied ? this.conexao : DBStaticConn.getConnection();
            final String SQL = sql;
            trace("Executando " + SQL);
            stmt = conn.prepareStatement(SQL);
            stmt.setMaxRows(maxLinhas);
            // Passa os par�metros de sele��o
            for (int i = 0; sqlParams != null && i < sqlParams.length; i++) {
                stmt.setObject(i + 1, sqlParams[i]);
            }
            // Executa a query
            rs = stmt.executeQuery();
            // Busca os resultados
            Collection resultList = new ArrayList();
            while (rs.next()) {
                ${entidade} ${entidade?uncap_first} = new ${entidade}();
<#list atributos as a>
                ${entidade?uncap_first}.set${a.nome?cap_first}(rs.${a.metodo_set?replace("set", "get")}(COLUNA_${a.coluna?upper_case}));
</#list>
                resultList.add(${entidade?uncap_first});
            }
            ${entidade} retorno[] = new ${entidade}[resultList.size()];
            resultList.toArray(retorno);
            return retorno;
        } catch (Exception _e) {
            _e.printStackTrace();
            throw new ${entidade}DaoException("Erro: " + _e.getMessage(), _e);
        } finally {
            DBStaticConn.close(rs);
            DBStaticConn.close(stmt);
            if (!isConnSupplied) {
                DBStaticConn.close(conn);
            }
        }
    }
}
