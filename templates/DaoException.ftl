/*
 *  Código gerado automaticamente pelo gerador de código GCExp, não deve ser alterado diretamente
 *
 */
package ${package};

/**
 * Classe para tratamento de exceções do Dao da entidade ${entidade}
 *
 * @author ${autor}
 */
public class ${entidade}DAOException extends Exception {

    /**
     * Cria uma nova exceção passando uma mensagem e a causa
     * @param msg
     * @param cause
     */
    public ${entidade}DAOException(String msg, Throwable cause) {
        super(msg, cause);
    }

    /**
     * Cria uma nova exceção passando apenas mensagem
     * @param msg
     */
    public ${entidade}DAOException(String msg) {
        this(msg, null);
    }

}
