<#assign last_att_key = 0>
<#list atributos as a>
<#if a.isPk>
<#assign last_att_key = a_index>
</#if>
</#list>
/*
 *  Código gerado automaticamente pelo gerador de código GCExp, não deve ser alterado diretamente
 *
 */
package ${package};

import br.com.rech.pw.model.db.DBStaticConn;
import java.sql.Connection;
<#list atributos as a>
<#if a.tipo = 'Date'>
import java.sql.Date;
<#break>
</#if>
</#list>
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Classe DAO (Data Access Object) para acesso a tabela ${tabela}
 *
 * @author ${autor}
 */
public class ${entidade}DAO {

    /**
     * Conexão com o banco de dados
     */
    private Connection conexao;
    /**
     * Métodos de busca podem passar este valor para o JDBC setMaxRows
     */
    private int maxLinhas;
    /**
     * Indica se deve registrar trace de operações na tabela ${tabela}
     */
    private boolean registraTrace = false;
    /**
     * Comando Select padrão para métodos de busca da tabela ${tabela}
     */
    private final String SQL_SELECT = "SELECT <#list atributos as a>${a.coluna}<#if a_has_next>, </#if></#list> FROM ${tabela}";
    /**
     * Comando Insert padrão para a tabela ${tabela}
     */
    protected final String SQL_INSERT = "INSERT INTO ${tabela} (<#list atributos as a>${a.coluna}<#if a_has_next>, </#if></#list>) VALUES (<#list atributos as a>?<#if a_has_next>, </#if></#list>)";
    /**
     * Comando Update padrão para a tabela ${tabela}
     */
    protected final String SQL_UPDATE = "UPDATE ${tabela} SET <#list atributos as a>${a.coluna} = ?<#if a_has_next>, </#if></#list><#list primary_key as p><#if p_index = 0> WHERE </#if>${p.nome} = ?<#if p_has_next> AND </#if></#list>";
    /**
     * Comando Delete padrão para a tabela ${tabela}
     */
    protected final String SQL_DELETE = "DELETE FROM ${tabela}<#list primary_key as p><#if p_index = 0> WHERE </#if>${p.nome} = ?<#if p_has_next> AND </#if></#list>";
    /**
     * Comando Delete para exclusão de todos os dados da tabela ${tabela}
     */
    protected final String SQL_DELETE_ALL = "DELETE FROM ${tabela}";
<#list atributos as a>
    /**
     * Indice da coluna ${a.coluna}
     */
    protected static final int COLUNA_${a.coluna?upper_case} = ${a_index + 1};
<#if !a_has_next>
    /**
     * Número de colunas da tabela
     */
    protected static final int NRO_COLUNAS_TABELA = ${a_index + 1};
</#if>
</#list>

    /**
     * Construtor padrão
     */
    public ${entidade}DAO() {}

    /**
     * Construtor informando a conexão com o banco de dados
     * @param conn Conexão com o banco de dados
     */
    public ${entidade}DAO(Connection conexaoUsuario) {
        this.conexao = conexaoUsuario;
    }

    /**
     * Seta o número máximo de colunas a retornar
     */
    public void setMaxRows(int maxRows) {
        this.maxLinhas = maxRows;
    }

    /**
     * Retorna o número máximo de linhas a retornar
     */
    public int getMaxRows() {
        return maxLinhas;
    }

    /**
     * Indica que deve registrar trace de operações na tabela ${tabela}
     */
    public void setTraceOn() {
        this.registraTrace = true;
    }

    /**
     * Indica que não deve registrar trace de operações na tabela ${tabela} (default)
     */
    public void setTraceOff() {
        this.registraTrace = false;
    }

    /**
     * Registra trace de operações na tabela ${tabela}
     * @param Texto a registrar, mostrando o passo-a-passo de execução
     */
    private void trace(String texto) {
        if (registraTrace) {
            System.out.println(texto);
        }
    }

<#list atributos as a>
<#if a.tipo = 'Date'>
    /**
     * Converte data padrão para o formato do JDBC
     *
     * @param data Data no padr　o dos objetos básicos
     * @return Data convertida para o formato utilizado na API JDBC ou null
     */
    private Date getSqlDate(java.util.Date data) {
        return data == null ? null : new Date(data.getTime());
    }

<#break>
</#if>
</#list>
    /**
     * Insere uma nova linha na tabela ${tabela}
     *
     * @param ${entidade?uncap_first} Referência para o objeto a inserir
     * @return Primary Key
     * @throws ${entidade}DAOException
     */
    public ${entidade}Pk insert(${entidade} ${entidade?uncap_first}) throws ${entidade}DAOException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : DBStaticConn.getConnection();
            stmt = conn.prepareStatement(SQL_INSERT);
<#list atributos as a>
   <#switch a.tipo>
      <#case "boolean">
            stmt.${a.metodo_set}(COLUNA_${a.coluna?upper_case}, ${entidade?uncap_first}.is${a.nome?cap_first}());
          <#break>
      <#case "Date">
            stmt.${a.metodo_set}(COLUNA_${a.coluna?upper_case}, getSqlDate(${entidade?uncap_first}.get${a.nome?cap_first}()));
         <#break>
      <#default>
            stmt.${a.metodo_set}(COLUNA_${a.coluna?upper_case}, ${entidade?uncap_first}.get${a.nome?cap_first}());
         <#break>
   </#switch>
</#list>
            trace("Executando " + SQL_INSERT + " com: " + ${entidade?uncap_first}.toString());
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linha inclu퀀da em (" + (t2 - t1) + " ms)");
            return new ${entidade}Pk(<#list atributos as a><#if a.isPk>${entidade?uncap_first}.get${a.nome?cap_first}()<#if a_index < last_att_key>, </#if></#if></#list>);
        } catch (Exception ex) {
            throw new ${entidade}DAOException("Falha ao inserir: " + ex.getMessage(), ex);
        } finally {
            DBStaticConn.close(stmt);
        }
    }

    /**
     * Atualiza uma linha na tabela ${tabela}
     * @param pk Primary key
     * @param ${entidade?uncap_first} Referência para o objeto a atualizar
     * @throws ${entidade}DAOException
     */
    public void update(${entidade}Pk pk, ${entidade} ${entidade?uncap_first}) throws ${entidade}DAOException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : DBStaticConn.getConnection();
            trace("Executando " + SQL_UPDATE + " com: " + ${entidade?uncap_first}.toString());
            stmt = conn.prepareStatement(SQL_UPDATE);
<#list atributos as a>
   <#switch a.tipo>
      <#case "boolean">
            stmt.${a.metodo_set}(COLUNA_${a.coluna?upper_case}, ${entidade?uncap_first}.is${a.nome?cap_first}());
          <#break>
      <#case "Date">
            stmt.${a.metodo_set}(COLUNA_${a.coluna?upper_case}, getSqlDate(${entidade?uncap_first}.get${a.nome?cap_first}()));
         <#break>
      <#default>
            stmt.${a.metodo_set}(COLUNA_${a.coluna?upper_case}, ${entidade?uncap_first}.get${a.nome?cap_first}());
         <#break>
   </#switch>
</#list>
<#list primary_key as p>
<#if p_index = 0>
            int indexKey = NRO_COLUNAS_TABELA;
</#if>
</#list>
<#list atributos as a>
<#if a.isPk>
            stmt.${a.metodo_set}(++indexKey, pk.get${a.nome?cap_first}());
</#if>
</#list>
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linha atualizada em (" + (t2 - t1) + " ms)");
        } catch (Exception ex) {
            throw new ${entidade}DAOException("Falha ao atualizar: " + ex.getMessage(), ex);
        } finally {
            DBStaticConn.close(stmt);
        }
    }

    /**
     * Deleta um linha da tabela ${tabela}
     * @param pk Primary Key
     * @throws ${entidade}DAOException
     */
    public void delete(${entidade}Pk pk) throws ${entidade}DAOException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : DBStaticConn.getConnection();
            trace("Executando " + SQL_DELETE + " com: " + pk.toString());
            stmt = conn.prepareStatement(SQL_DELETE);
            int indexKey = 0;
<#list atributos as a>
<#if a.isPk>
            stmt.${a.metodo_set}(++indexKey, pk.get${a.nome?cap_first}());
</#if>
</#list>
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linha exclu퀀da em (" + (t2 - t1) + " ms)");
        } catch (Exception ex) {
            throw new ${entidade}DAOException("Falha ao excluir: " + ex.getMessage(), ex);
        } finally {
            DBStaticConn.close(stmt);
        }
    }

    /**
     * Deleta todas as linhas da tabela ${tabela}
     * @throws ${entidade}DAOException
     */
    public void deleteAll() throws ${entidade}DAOException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : DBStaticConn.getConnection();
            trace("Executando " + SQL_DELETE_ALL);
            stmt = conn.prepareStatement(SQL_DELETE_ALL);
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linhas exclu퀀das em (" + (t2 - t1) + " ms)");
        } catch (Exception ex) {
            throw new ${entidade}DAOException("Falha ao excluir todas as linhas da tabela: " + ex.getMessage(), ex);
        } finally {
            DBStaticConn.close(stmt);
        }
    }

    /**
     * Retorna uma linha da tabela ${tabela} pela Primary Key
     *
     * @param pk Primary Key
     * @return Objeto do tipo ${entidade}
     * @throws ${entidade}DAOException
     */
    public ${entidade} buscaPrimaryKey(${entidade}Pk pk) throws ${entidade}DAOException {
        return buscaPrimaryKey(<#list atributos as a><#if a.isPk>pk.get${a.nome?cap_first}()<#if a_index < last_att_key>, </#if></#if></#list>);
    }

    /**
     * Retorna uma linha da tabela ${tabela} comparando os atributos da Primary Key
     *
<#list atributos as a>
<#if a.isPk>
     * @param ${a.nome} ${a.descricao}
</#if>
</#list>
     * @return Array de objetos do tipo ${entidade}
     * @throws ${entidade}DAOException
     */
    public ${entidade} buscaPrimaryKey(<#list atributos as a><#if a.isPk>${a.tipo} ${a.nome}<#if a_index < last_att_key>, </#if></#if></#list>) throws ${entidade}DAOException {
        ${entidade} ${entidade?uncap_first}s[] = buscaPorSelectDinamico("<#list primary_key as p>${p.nome} = ?<#if p_has_next> AND </#if></#list>", new Object[]{<#list atributos as a><#if a.isPk>new ${a.objeto_compat}(${a.nome})<#if a_index < last_att_key>, </#if></#if></#list>});
        return ${entidade?uncap_first}s.length == 0 ? null : ${entidade?uncap_first}s[0];
    }

    /**
     * Retorna linhas da tabela ${tabela} de acordo com a sentença SQL
     * @param sql SQL a executar
     * @param sqlParams Parâmetros
     * @return Array de objetos do tipo ${entidade}
     * @throws ${entidade}DAOException
     */
    public ${entidade}[] buscaPorSelectDinamico(String filtro, Object[] sqlParams) throws ${entidade}DAOException {
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = isConnSupplied ? this.conexao : DBStaticConn.getConnection();
            final String SQL = SQL_SELECT + " WHERE " + filtro;
            trace("Executando " + SQL);
            stmt = conn.prepareStatement(SQL);
            stmt.setMaxRows(maxLinhas);
            // Passa os parâmetros de seleção
            for (int i = 0; sqlParams != null && i < sqlParams.length; i++) {
                stmt.setObject(i + 1, sqlParams[i]);
            }
            // Executa a query
            rs = stmt.executeQuery();
            // Busca os resultados
            Collection resultList = new ArrayList();
            while (rs.next()) {
                ${entidade} ${entidade?uncap_first} = new ${entidade}();
<#list atributos as a>
                ${entidade?uncap_first}.set${a.nome?cap_first}(rs.${a.metodo_set?replace("set", "get")}(COLUNA_${a.coluna?upper_case}));
</#list>
                resultList.add(${entidade?uncap_first});
            }
            ${entidade} retorno[] = new ${entidade}[resultList.size()];
            resultList.toArray(retorno);
            return retorno;
        } catch (Exception ex) {
            throw new ${entidade}DAOException("Erro: " + ex.getMessage(), ex);
        } finally {
            DBStaticConn.close(rs);
            DBStaticConn.close(stmt);
        }
    }
}
