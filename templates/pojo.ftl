/*
 *  Código gerado automaticamente pelo gerador de código GCExp, não deve ser alterado diretamente
 *
 */
package ${package};

<#list atributos as a>
<#if a.tipo = 'Date'>
import java.util.Date;

<#break>
</#if>
</#list>
/**
 * Classe que representa a entidade ${entidade}
 *
 * @author ${autor}
 */
public class ${entidade} {

<#list atributos as a>
    /**
     * ${a.descricao?cap_first}
     */
    ${a.acesso} ${a.tipo} ${a.nome};

</#list>
    /**
     * Construtor principal
     *
     */
    public ${entidade}() {}

<#list atributos as a>
    /**
     * Seta ${a.descricao}
     */
    public void set${a.nome?cap_first}(${a.tipo} ${a.nome}) {
        this.${a.nome} = ${a.nome};
    }

<#if a.tipo == "boolean">
    /**
     * Retorna verdadeiro ${a.descricao}
     */
    public ${a.tipo} is${a.nome?cap_first}() {
        return ${a.nome};
    }
<#else>
    /**
     * Retorna ${a.descricao}
     */
    public ${a.tipo} get${a.nome?cap_first}() {
        return ${a.nome};
    }
</#if>

</#list>
    @Override
    public String toString() {
        return "${entidade}{" +
<#list atributos as a>
<#if a_has_next>
                    "${a.nome}=" + ${a.nome} + ", " +
<#else>
                    "${a.nome}=" + ${a.nome} + "}";
</#if>
</#list>
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ${entidade} outro${entidade} = (${entidade}) obj;
<#list atributos as a>
<#if a.isPrimitive>
        if (this.${a.nome} != outro${entidade}.${a.nome}) {
<#else>
        if (!this.${a.nome}.equals(outro${entidade}.${a.nome})) {
</#if>
            return false;
        }
</#list>
        return true;
    }

}
