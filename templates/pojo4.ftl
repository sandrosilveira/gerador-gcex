<#-- Template para gera��o de um objeto b�sico que encapsula os atributos de uma entidade/objeto -->
/*
 *  Copyright (C) 2012 ${autor}
 *  sandrosilveira@gmail.com
 *
 *  C�digo Gerado pelo gerador de c�digo GCExp
 *
 *  Este programa foi desenvolvido para o trabalho de conclus�o de 
 *  curso apresentado como requisito parcial � obten��o do grau de
 *  Bacharel em Ci�ncia da Computa��o pela Universidade Feevale.
 *
 *  Este programa n�o pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autoriza��o do autor.
 * 
 */
package ${package};

/**
 * Classe que representa a entidade ${entidade} 
 *
 * @author ${autor}
 */
public class ${entidade} {

<#list atributos as a>
    /**
     * ${a.descricao?cap_first}
     */
    ${a.acesso} ${a.tipo} ${a.nome};

</#list>
    /**
     * Construtor principal
     *
     */
    public ${entidade}() {
    }

<#list atributos as a>
    /**
     * Seta ${a.descricao}
     */
    public void set${a.nome?cap_first}(${a.tipo} ${a.nome}) {
        this.${a.nome} = ${a.nome};
    }

<#if a.tipo == "boolean">
    /**
     * Retorna verdadeiro ${a.descricao}
     */
    public ${a.tipo} is${a.nome?cap_first}() {
        return ${a.nome};
    }
<#else>
    /**
     * Retorna ${a.descricao}
     */
    public ${a.tipo} get${a.nome?cap_first}() {
        return ${a.nome};
    }
</#if>

</#list>
    @Override
    public String toString() {
        return "${entidade}{" + 
<#list atributos as a>
<#if a_has_next>
                    "${a.nome}=" + ${a.nome} + ", " +
<#else>
                    "${a.nome}=" + ${a.nome} + "}";
</#if>
</#list>
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ${entidade} outro${entidade} = (${entidade}) obj;
<#list atributos as a>
        if (this.${a.nome} != outro${entidade}.${a.nome}) {
            return false;
        }
</#list>
        return true;
    }

}
