      *>--------------------------------------------------------------------------------------------------------------<*
      *>-> Declara��o do m�todo
      *>--------------------------------------------------------------------------------------------------------------<*
      *>-> Se compilado com NetExpress
      $IF MFNETX DEFINED
           COPY                   OOINIMET.CPY REPLACING
                                  ==(INTERNO)== BY ${nomeInterno}
                                  ==(EXTERNO)== BY "${nomeExterno}".
       LINKAGE                    SECTION.
<#list copys as c>
           COPY                   ${c.nome}.
</#list>
       PROCEDURE                  DIVISION USING 
<#list usings as u>
<#if u_has_next>
                                  ${u.nome},
<#else>
<#if returning != ''>
                                  ${u.nome} RETURNING ${returning}.
<#else>
                                  ${u.nome}.
</#if>
</#if>
</#list>
      *>-> Se compilado com isCobol
      $ELSE
       LINKAGE                    SECTION.
<#list copys as c>
           COPY                   ${c.nome}.
</#list>
      *>--------------------------------------------------------------------------------------------------------------<*
      *>-> Se ainda n�o definiu a procedure global
      $IF W78-PROGLO NOT DEFINED
      *>-> Procedure global - n�o utilizada
       PROCEDURE                  DIVISION.
      *>--------------------------------------------------------------------------------------------------------------<*
      *>-> Indica que j� definiu a procedure global
      $SET CONSTANT W78-PROGLO "S"
      $END
      *>--------------------------------------------------------------------------------------------------------------<*
      *>-> Construtor do isCobol para receber os par�metros do programa
      *>--------------------------------------------------------------------------------------------------------------<*
       IDENTIFICATION             DIVISION.
       METHOD-ID.                 MET-NEW AS "new".
       DATA                       DIVISION.
       WORKING-STORAGE            SECTION.
<#list usings as u>
       78  W78-TAM-${u.nome} VALUE IS LENGTH OF ${u.nome}.
</#list>
       LINKAGE                    SECTION.
<#list usings as u>
       01  ${u.nome}-CP PIC IS X(W78-TAM-${u.nome}).
</#list>
       PROCEDURE                  DIVISION USING
<#list usings as u>
<#if u_has_next>
                                  ${u.nome}-CP,
<#else>
                                  ${u.nome}-CP.
</#if>
</#list>
<#list usings as u>
      *>-> Linkagem do par�metro
           SET     ADDRESS OF ${u.nome} TO ADDRESS OF ${u.nome}-CP.
</#list>
       END METHOD.
      *>--------------------------------------------------------------------------------------------------------------<*
           COPY                   OOINIMET.CPY REPLACING
                                  ==(INTERNO)== BY ${nomeInterno}
                                  ==(EXTERNO)== BY "${nomeExterno}".
<#if returning != ''>
       PROCEDURE                  DIVISION RETURNING ${returning}.
<#else>
       PROCEDURE                  DIVISION.
</#if>
      $END
      *>--------------------------------------------------------------------------------------------------------------<*
