<#-- Template para geração de um objeto básico que encapsula os atributos de uma entidade/objeto -->
/*
 * Autor: ${autor}
 *
 */
package ${package};

public class ${entidade} {

<#list atributos as a>
    /**
     * ${a.descricao?cap_first}
     */
    ${a.acesso} ${a.tipo} ${a.nome};

</#list>
<#list atributos as a>
    /**
     * Seta ${a.descricao}
     */
    public void set${a.nome?cap_first}(${a.tipo} ${a.nome}) {
        this.${a.nome} = ${a.nome};
    }

    /**
     * Retorna ${a.descricao}
     */
    public ${a.tipo} get${a.nome?cap_first}() {
        return ${a.nome};
    }

</#list>
}
