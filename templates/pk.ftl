<#assign last_att_key = 0>
<#list atributos as a>
<#if a.isPk>
<#assign last_att_key = a_index>
</#if>
</#list>
/*
 *  Código gerado automaticamente pelo gerador de código GCExp, não deve ser alterado diretamente
 *
 */
package ${package};

/**
 * Classe que representa a primary key da tabela ${tabela}
 *
 * @author ${autor}
 */
public class ${entidade}Pk {

<#list atributos as a>
<#if a.isPk>
    /**
     * ${a.descricao?cap_first}
     */
    private ${a.tipo} ${a.nome};

</#if>
</#list>
    /**
     * Construtor principal
     *
     */
    public ${entidade}Pk() {}

    /**
     * Construtor passando os atributos da primary key da tabela ${tabela}
     *
     */
    public ${entidade}Pk(<#list atributos as a><#if a.isPk>${a.tipo} ${a.nome}<#if a_index < last_att_key>, </#if></#if></#list>) {
<#list atributos as a>
<#if a.isPk>
        this.${a.nome} = ${a.nome};
</#if>
</#list>
    }

<#list atributos as a>
<#if a.isPk>
    /**
     * Seta ${a.descricao}
     */
    public void set${a.nome?cap_first}(${a.tipo} ${a.nome}) {
        this.${a.nome} = ${a.nome};
    }

<#if a.tipo == "boolean">
    /**
     * Retorna verdadeiro ${a.descricao}
     */
    public ${a.tipo} is${a.nome?cap_first}() {
        return ${a.nome};
    }
<#else>
    /**
     * Retorna ${a.descricao}
     */
    public ${a.tipo} get${a.nome?cap_first}() {
        return ${a.nome};
    }
</#if>

</#if>
</#list>
    @Override
    public String toString() {
        return "${entidade}Pk{" +
<#list atributos as a>
<#if a.isPk>
<#if a_index < last_att_key>
                    "${a.nome}=" + ${a.nome} + ", " +
<#else>
                    "${a.nome}=" + ${a.nome} + "}";
</#if>
</#if>
</#list>
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ${entidade}Pk outro${entidade} = (${entidade}Pk) obj;
<#list atributos as a>
<#if a.isPk>
<#if a.isPrimitive>
        if (this.${a.nome} != outro${entidade}.get${a.nome?cap_first}()) {
<#else>
        if (!this.${a.nome}.equals(outro${entidade}.get${a.nome?cap_first}())) {
</#if>
            return false;
        }
</#if>
</#list>
        return true;
    }

}
