
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import org.gcex.nucleo.NucleoGeradorCodigo;

/*
 * Exemplo de carga e manipulação da estrutura da especificação carregada pelo gerador de código
 */
public class TesteGetMap {

    private static Map<String, Object> nodo;
    private static int nivel = 1;
    private static boolean[] ultimo = new boolean[64];

    public static void main(String[] args) {
        NucleoGeradorCodigo nucleo = new NucleoGeradorCodigo();
        Map<String, Object> especificacao = nucleo.getEspecificacao("xml", "tstBanco");

        System.out.println("");
        System.out.println("  (\\)");
        mostraMap(especificacao);
        System.out.println("");
    }

    /**
     * Mostra a estrutura de um determinado nodo da especificação
     *
     * @param nodo Map com a estrutura de um nodo
     */
    private static void mostraMap(Map<String, Object> nodo) {
        int seq = 0;
        for (Map.Entry<String, Object> entry : nodo.entrySet()) {
            mostraEntry(entry, ++seq, nodo.size());
        }
    }

    /**
     * Mostra a estrutura de uma entrada de um nodo da especificação (pares chave/valor ou outro nodo)
     *
     * @param entry
     */
    private static void mostraEntry(Map.Entry<String, Object> entry, int seq, int size) {
        String key = entry.getKey();
        Object value = entry.getValue();
        ultimo[nivel] = seq == size;
        // Avalia tipo do valor armazenado na estrutura
        if (value instanceof String) {
            if (!key.equals("nodeName")) {
                mostraIdentado("+- " + key + " = " + value);
            }
        } else if (value instanceof Integer) {
            mostraIdentado("+- " + key + " = " + (Integer) value);
        } else if (value instanceof Date) {
            mostraIdentado("+- " + key + " = " + (Date) value);
        } else if (value instanceof Boolean) {
            mostraIdentado("+- " + key + " = " + (Boolean) value);
        } else if (value instanceof ArrayList) {
            mostraIdentado("+- " + key + " (arrayList)");
            nivel++;
            int seqArray = 0;
            ArrayList lista = (ArrayList) value;
            for (Object o : lista) {
                ultimo[nivel] = ++seqArray == lista.size();
                nodo = (Map<String, Object>) o;
                // Se tem uma entrada com o nome do nodo
                if (nodo.containsKey("nodeName")) {
                    mostraIdentado("+- " + nodo.get("nodeName") + "#" + seqArray);
                } else {
                    mostraIdentado("+- " + key + "#" + seqArray);
                }
                nivel++;
                mostraMap(nodo);
                nivel--;
            }
            nivel--;
        } else {
            mostraIdentado("+- " + key + " = <tipo de dado inválido>");
        }
    }

    /**
     * Mostra uma entrada da estrutura da especificação de forma identada, de acordo com o nível hierárquico
     *
     * @param str
     */
    private static void mostraIdentado(String str) {
        mostraIdentacao();
        System.out.println("|");
        mostraIdentacao();
        System.out.println(str);
    }

    /**
     * Mostra os espaços ou barras antes do conteúdo identado
     */
    private static void mostraIdentacao() {
        for (int i = 0; i < nivel; i++) {
            if (i == 0 || ultimo[i]) {
                System.out.print("   ");
            } else {
                System.out.print("|  ");
            }
        }
    }
}
