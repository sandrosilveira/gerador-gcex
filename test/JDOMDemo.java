

/*
 * Copyright (c) Ian F. Darwin, http://www.darwinsys.com/, 1996-2002.
 * All rights reserved. Software written by Ian F. Darwin and others.
 * $Id: LICENSE,v 1.8 2004/02/09 03:33:38 ian Exp $
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * Java, the Duke mascot, and all variants of Sun's Java "steaming coffee
 * cup" logo are trademarks of Sun Microsystems. Sun's, and James Gosling's,
 * pioneering role in inventing and promulgating (and standardizing) the Java 
 * language and environment is gratefully acknowledged.
 * 
 * The pioneering role of Dennis Ritchie and Bjarne Stroustrup, of AT&T, for
 * inventing predecessor languages C and C++ is also gratefully acknowledged.
 */
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.jdom2.*;
import org.jdom2.input.DOMBuilder;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.DOMOutputter;

/*
 * Simple demo of JDOM
 */
public class JDOMDemo {

    public static void main(String[] args) {

        // Must be at least one file or URL argument
        // if (args.length == 0) {
        //     System.out.println("Usage: java JDOMDemo URL [...]");
        // }

        SAXBuilder saxBuilder = new SAXBuilder();
        DOMBuilder domBuilder = new DOMBuilder();

        try {
            Document jdomDocument = saxBuilder.build("TrabalhoSeguradora.xmi");

            DOMOutputter domOutputter = new DOMOutputter();

            /*
             * Test getting DOM Document from JDOM Document org.w3c.dom.Document domDocument =
             * domOutputter.output(doc);
             */

            /*
             * Test getting DOM Element from JDOM Element
             */
            org.w3c.dom.Element domElement =
                    domOutputter.output(jdomDocument.getRootElement());

            /*
             * Test getting JDOM Element from DOM Element
             */
            org.jdom2.Element jdomElement = domBuilder.build(domElement);
            demo(jdomElement, 0);

        } catch (JDOMException e) { // indicates a well-formedness or other error
            System.out.println(" is not a well formed XML document.");
            System.out.println(e.getMessage());
        } catch (IOException ex) {
            System.out.println("Input or Output error:" + ex);
        }
    }

    public static void demo(Document doc, int depth) {
        printSpaces(depth);
        
        List children = doc.getContent();
        Iterator iterator = children.iterator();
        while (iterator.hasNext()) {
            Object o = iterator.next();
            if (o instanceof Element) {
                demo((Element) o, depth + 1);
            } else if (o instanceof Comment) {
                doComment((Comment) o);
            } else if (o instanceof ProcessingInstruction) {
                doPI((ProcessingInstruction) o);
            }
        }
    }

    public static void demo(Element element, int depth) {

        printSpaces(depth);
        
        System.out.println("Element " + element.getName());

        List attributes = element.getAttributes();
        Iterator it = attributes.iterator();
        while (it.hasNext()) {
            Object o = it.next();
            Attribute att = (Attribute) o;
            printSpaces(depth);
            System.out.println("Atributo: " + att.getName() + "=" + att.getValue());
        }
        
        List children = element.getContent();
        Iterator it2 = children.iterator();
        while (it2.hasNext()) {
            Object o = it2.next();
            if (o instanceof Element) {
                demo((Element) o, depth + 1);
            } else if (o instanceof Comment) {
                doComment((Comment) o);
            } else if (o instanceof ProcessingInstruction) {
                doPI((ProcessingInstruction) o);
            } else if (o instanceof String) {
                System.out.println("String: " + o);
            }
        }
    }

    public static void doComment(Comment c) {
        System.out.println("Comment: " + c);
    }

    public static void doPI(ProcessingInstruction pi) {
        System.out.println("PI: " + pi);
    }
    
    private static void printSpaces(int n) {
        for (int i = 0; i < n; i++) {
            System.out.print(' ');
        }
    }
    
}