///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//
//
//import org.gcex.utilit.Texto;
//import org.junit.*;
//import static org.junit.Assert.*;
//
///**
// *
// * @author Sandro
// */
//public class TextoTest {
//
//    public TextoTest() {
//    }
//
//    @BeforeClass
//    public static void setUpClass() throws Exception {
//    }
//
//    @AfterClass
//    public static void tearDownClass() throws Exception {
//    }
//
//    @Before
//    public void setUp() {
//    }
//
//    @After
//    public void tearDown() {
//    }
//
//    /**
//     * Test of converteNomeAtributo method, of class Texto.
//     */
//    @Test
//    public void testConverteNomeAtributo() {
//        System.out.println("Executando teste do método converteNomeAtributo()");
//
//        String txt = "item_pedido";
//        String expResult = "itemPedido";
//        String result = Texto.converteNomeAtributo(txt);
//        assertEquals(expResult, result);
//
//        txt = "NP_COD";
//        expResult = "npCod";
//        result = Texto.converteNomeAtributo(txt);
//        assertEquals(expResult, result);
//
//        txt = "NOme proprietário";
//        expResult = "nomeProprietario";
//        result = Texto.converteNomeAtributo(txt);
//        assertEquals(expResult, result);
//
//        txt = "nome_campo_banco";
//        expResult = "nomeCampoBanco";
//        result = Texto.converteNomeAtributo(txt);
//        assertEquals(expResult, result);
//
//        txt = "% IRRF";
//        expResult = "percIrrf";
//        result = Texto.converteNomeAtributo(txt);
//        assertEquals(expResult, result);
//    }
//
//    /**
//     * Test of converteNomeClasse method, of class Texto.
//     */
//    @Test
//    public void testConverteNomeClasse() {
//        System.out.println("Executando teste do método converteNomeClasse()");
//
//        String txt = "item_pedido";
//        String expResult = "ItemPedido";
//        String result = Texto.converteNomeClasse(txt);
//        assertEquals(expResult, result);
//
//        txt = "NOME CLASSE";
//        expResult = "NomeClasse";
//        result = Texto.converteNomeClasse(txt);
//        assertEquals(expResult, result);
//
//        txt = "nome_classe";
//        expResult = "NomeClasse";
//        result = Texto.converteNomeClasse(txt);
//        assertEquals(expResult, result);
//
//        txt = "veículos AUTOMOTORES";
//        expResult = "VeiculosAutomotores";
//        result = Texto.converteNomeClasse(txt);
//        assertEquals(expResult, result);
//    }
//
//    /**
//     * Test of converteNomeClasse method, of class Texto.
//     */
//    @Test
//    public void testRetiraAcentuacao() {
//        System.out.println("Executando teste do método retiraAcentuacao()");
//
//        String txt = "çÇáéíóúýÁÉÍÓÚÝàèìòùÀÈÌÒÙãõñäëïöüÿÄËÏÖÜÃÕÑâêîôûÂÊÎÔÛ";
//        String expResult = "cCaeiouyAEIOUYaeiouAEIOUaonaeiouyAEIOUAONaeiouAEIOU";
//        String result = Texto.retiraAcentuacao(txt);
//        assertEquals(expResult, result);
//
//        txt = "Texto com Acentuação";
//        expResult = "Texto com Acentuacao";
//        result = Texto.retiraAcentuacao(txt);
//        assertEquals(expResult, result);
//
//        txt = "áéíóú";
//        expResult = "aeiou";
//        result = Texto.retiraAcentuacao(txt);
//        assertEquals(expResult, result);
//
//    }
//
//}
