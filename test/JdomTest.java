/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import java.util.Iterator;
import org.jdom2.Content;
import org.jdom2.Document;
import org.jdom2.Text;
import org.jdom2.input.SAXBuilder;

public class JdomTest {
  public static void main(String[] args) throws Exception {
    SAXBuilder builder = new SAXBuilder();
    Document doc = builder.build("origem/TrabalhoSeguradora.xmi");

    for (Iterator it = doc.getRootElement().getContent().iterator(); it.hasNext();) {
      Content content = (Content) it.next();
      if (content instanceof Text) {
        System.out.println("[Text: " + ((Text) content).getTextNormalize());
      } else {
        System.out.println(content.toString());
      }
    }
  }

}