
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.jdom2.*;
import org.jdom2.input.SAXBuilder;

public class ListaDataTypes {

    public static void main(String[] args) {

        // if (args.length == 0) {
        //    System.out.println("Usage: java NodeLister URL");
        //    return;
        // }

        SAXBuilder builder = new SAXBuilder();

        try {
            Document doc = builder.build("origem/TrabalhoSeguradora.xmi");
            listNodes(doc, 0);
        } // indicates a well-formedness error
        catch (JDOMException e) {
            System.out.println(args[0] + " is not well-formed.");
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e);
        }

    }

    public static void listNodes(Object o, int depth) {

        //printSpaces(depth);

        if (o instanceof Element) {
            Element element = (Element) o;
//            System.out.println("Element: " + element.getName());
            
//            List atributos = element.getAttributes();
//            Iterator iterator = atributos.iterator();
//            while (iterator.hasNext()) {
//                Attribute atributo = (Attribute) iterator.next();
//                System.out.print(atributo.getName() + "=" + atributo.getValue()+ ";");
//            }
//            System.out.println();

//            if (element.getName().equals("Class")) {
//                System.out.println("Classe encontrada!");
//                System.out.println("name=" + element.getAttributeValue("name"));
//                
//            }
//            
//            if (element.getName().equals("Attribute")) {
//                System.out.println("Atributo encontrado!");
//                System.out.println("name=" + element.getAttributeValue("name"));
//                
//            }

            if (element.getName().equals("DataType")) {
                System.out.println("DataType encontrado!");
                System.out.println("xmi.id=" + element.getAttributeValue("xmi.id"));
                System.out.println("name=" + element.getAttributeValue("name"));
            }
            
            List children = element.getContent();
            Iterator iterator = children.iterator();
            while (iterator.hasNext()) {
                Object child = iterator.next();
                listNodes(child, depth + 1);
            }
        } else if (o instanceof Document) {
            System.out.println("Document");
            Document doc = (Document) o;
            List children = doc.getContent();
            Iterator iterator = children.iterator();
            while (iterator.hasNext()) {
                Object child = iterator.next();
                listNodes(child, depth + 1);
            }
        } else if (o instanceof Comment) {
            //System.out.println("Comment");
        } else if (o instanceof CDATA) {
            //System.out.println("CDATA section");
        } else if (o instanceof Text) {
            Text t1 = (Text) o;
            //System.out.println("Text");
            
            // System.out.println(t1.getText());
            
        } else if (o instanceof EntityRef) {
            //System.out.println("Entity reference");
        } else if (o instanceof ProcessingInstruction) {
            //System.out.println("Processing Instruction");
        } else {  // This really shouldn't happen
            //System.out.println("Unexpected type: " + o.getClass());
        }

    }

    private static void printSpaces(int n) {

        for (int i = 0; i < n; i++) {
            System.out.print(' ');
        }

    }
}