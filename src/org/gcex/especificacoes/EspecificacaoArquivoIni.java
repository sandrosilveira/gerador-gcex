/*
 *  Copyright (C) 2012 Sandro Madruga Silveira
 *  sandrosilveira@gmail.com
 *
 *  Este programa foi desenvolvido para o trabalho de conclusão de
 *  curso apresentado como requisito parcial à obtenção do grau de
 *  Bacharel em Ciência da Computação pela Universidade Feevale.
 *
 *  Este programa não pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autorização do autor.
 *
 */
package org.gcex.especificacoes;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import org.gcex.especificacoes.estrutura.EspecificacaoBase;
import org.gcex.especificacoes.estrutura.EspecificacaoInterface;

/**
 * Especificação de entrada para arquivos INI.
 *
 * @author Sandro Madruga Silveira
 */
public class EspecificacaoArquivoIni extends EspecificacaoBase implements EspecificacaoInterface {

    /**
     * Construtor da classe de especificação, recebendo o elemento de origem a ser processado
     *
     * @param iniFileName Nome do arquivo ini a processar
     */
    public EspecificacaoArquivoIni(String iniFileName) {
        this.nomeElemento = iniFileName;
        this.fileName = "origem/" + iniFileName + ".ini";
    }

    /**
     * Retorna uma lista com todas os elementos disponíveis para a especificação de entrada a partir de um
     * filtro específico.
     *
     * @param filtro Diretório para examinar arquivos ini disponíveis
     * @return Lista contendo os arquivos ini disponíveis em um determinado diretório
     */
    @Override
    public ArrayList<String> getListaDisponiveis(String filtro) {
        // TODO: Implementar
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Retorna a referência para a estrutura da especificação de entrada que foi carregada
     *
     * @return Estrutura da especificação de entrada carregada
     */
    @Override
    public Map getEspecificacao() {
        // Abre o arquivo ini para leitura
        BufferedReader iniFile = abreArquivo(fileName);
        // Cria a raiz da especificação
        root = new LinkedHashMap();

        // Coloca o nome da entidade no nodo raiz
        // TODO: Criar método para extrair o nome base do arquivo sem ponto/extensão
        root.put("entidade", nomeElemento);

        // Coloca informações extras no nodo raiz
        // TODO: Criar método para fazer isto, adicionar dados ao root ou aos nodos
        root.put("autor", "Sandro Silveira");
        root.put("package", "model");
        addAtributosGeracao();

        // Cria uma lista de atributos que serão inseridos no nodo raiz, permitindo a iteração
        // dos elementos nos templates
        nodos = new ArrayList();
        root.put("atributos", nodos);

        // Processa o arquivo ini
        String line;
        try {
            while ((line = iniFile.readLine()) != null) {
                parseLine(line);
            }
        } catch (IOException ex) {
            System.out.println("Erro de leitura!");
        }

        // Fecha o arquivo em processamento
        fechaArquivo(iniFile);
        // retorna a árvore com a especificação carregada
        return root;
    }

    /**
     * Processa uma linha do arquivo ini
     *
     * @param s Linha do arquivo ini a processar
     */
    private void parseLine(String s) {
        // Remove os comentarios
        String line = removeComents(s);
        if (line.isEmpty()) {
            return;
        }
        if (isSection(line)) {
            parseSection(line);
        } else {
            if (isProperty(line)) {
                parseProperty(line);
            } else {
                throw new RuntimeException("Erro processando arquivo ini, conteúdo da linha:\n" + line);
            }
        }
    }

    /**
     * Processa uma definição de seção do arquivo ini
     *
     * @param line Linha do arquivo ini a processar
     */
    private void parseSection(String line) {
        String[] code;
        code = line.split("\\[|\\]");
        if (code.length > 1) {
        // REFATORAR: tem na classe base
            // Cria um nodo para conter os pares chave/valor de cada seção do ini
            nodo = new LinkedHashMap();
            // Associa a seção ao nodo raiz
            nodos.add(nodo);
            // Coloca o nome da seção do ini como um atributo
            nodo.put("nome", code[1]);
        }
    }

    /**
     * Processa uma definição de propriedade do arquivo ini
     *
     * @param line Linha do arquivo ini a processar
     */
    private void parseProperty(String line) {
        String key, value;
        String[] tokens = line.split("=");
        for (int i = 0; i < tokens.length; i++) {
            tokens[i] = tokens[i].trim();
        }
        if (tokens.length == 0) {
            throw new RuntimeException("Erro processando arquivo ini, conteúdo da linha:\n" + line);
        }
        key = tokens[0].toLowerCase();
        if (tokens.length > 1) {
            value = tokens[1];
        } else {
            value = "";
        }
        // TODO: REFATORAR tem na classe base
        nodo.put(key, value);
    }

    /**
     * Retorna se a linha contém uma seção
     *
     * @param line Linha do arquivo ini a processar
     * @return Verdadeiro se a linha contém uma seção
     */
    private boolean isSection(String line) {
        return line.matches("^\\s*\\[.*");
    }

    /**
     * Retorna se a linha contém uma propriedade
     *
     * @param line Linha do arquivo ini a processar
     * @return Verdadeiro se a linha contém uma propriedade
     */
    private boolean isProperty(String line) {
        return line.matches(".*=.*");
    }

    /**
     * Retorna uma linha sem os comentários
     *
     * @param line Linha do arquivo ini a processar
     * @return linha do arquivo ini sem os comentários
     */
    private String removeComents(String line) {
        return line.split("#")[0];
    }
}
