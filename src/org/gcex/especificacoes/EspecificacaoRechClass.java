/*
 *  Copyright (C) 2012 Sandro Madruga Silveira
 *  sandrosilveira@gmail.com
 *
 *  Este programa foi desenvolvido para o trabalho de conclusão de
 *  curso apresentado como requisito parcial à obtenção do grau de
 *  Bacharel em Ciência da Computação pela Universidade Feevale.
 *
 *  Este programa não pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autorização do autor.
 *
 */
package org.gcex.especificacoes;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FilenameUtils;
import org.gcex.especificacoes.estrutura.EspecificacaoBase;
import org.gcex.especificacoes.estrutura.EspecificacaoInterface;
import org.gcex.utilit.Arquivo;

/**
 * Especificação de entrada para classes em programas Cobol
 *
 * @author Sandro Madruga Silveira
 */
public class EspecificacaoRechClass extends EspecificacaoBase implements EspecificacaoInterface {

    /**
     * Construtor da classe de especificação, recebendo o elemento de origem a ser processado
     *
     * @param cobolFile Nome da classe (programa) a processar
     */
    public EspecificacaoRechClass(String cobolFile) {
        this.nomeElemento = FilenameUtils.getBaseName(cobolFile);
        this.fileName = cobolFile;
    }

    /**
     * Retorna uma lista com todas os elementos disponíveis para a especificação de entrada a partir de um
     * filtro específico.
     *
     * @param filtro Diretório para examinar arquivos ini disponíveis
     * @return Lista contendo os arquivos ini disponíveis em um determinado diretório
     */
    @Override
    public ArrayList<String> getListaDisponiveis(String filtro) {
        // TODO: Implementar
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Retorna a referência para a estrutura da especificação de entrada que foi carregada
     *
     * @return Estrutura da especificação de entrada carregada
     */
    @Override
    public Map getEspecificacao() {
        // Abre o arquivo ini para leitura
        BufferedReader cobolSource = abreArquivo(fileName);
        // Cria a raiz da especificação
        root = new LinkedHashMap();

        // Coloca o nome da entidade no nodo raiz
        // TODO: Criar método para extrair o nome base do arquivo sem ponto/extensão
        root.put("entidade", nomeElemento);

        // Coloca informações extras no nodo raiz
        // TODO: Criar método para fazer isto, adicionar dados ao root ou aos nodos
        root.put("autor", "Sandro Silveira");
        root.put("package", "model");
        addAtributosGeracao();

        // Cria uma lista de atributos que serão inseridos no nodo raiz, permitindo a iteração
        // dos elementos nos templates
        nodos = new ArrayList();
        root.put("metodos", nodos);

        // Processa o fonte Cobol
        String line;
        try {
            while ((line = cobolSource.readLine()) != null) {
                parseLine(line);
            }
        } catch (IOException ex) {
            System.out.println("Erro de leitura!");
        }

        // Fecha o arquivo em processamento
        fechaArquivo(cobolSource);
        // retorna a árvore com a especificação carregada
        return root;
    }

    /**
     * Processa uma linha do fonte Cobol
     *
     * @param s Linha do fonte Cobol a processar
     */
    private void parseLine(String line) {
        // Remove os comentarios
        if (line.isEmpty()) {
            return;
        }
        if (isTagComent(line)) {
//            System.out.println("Encontrei:" + line);
            parseTag(line);
        }

    }

    /**
     * Processa uma definição de tag em linha de comentário
     *
     * @param line Linha do arquivo ini a processar
     */
    private void parseTag(String line) {
        // Identifica a tag
        String[] tag;
        tag = line.split("\\*\\>\\-\\> ");
        // Se encontrou uma tag em um comentário
        if (tag.length > 1) {
            // Separa conteúdo da tag em Chave e Valor
            String[] chaveValor;
            chaveValor = tag[1].split("=");
            // Chave
            String chave = "";
            if (chaveValor.length > 0) {
                chave = chaveValor[0].replaceAll("^\\s+", "").trim();
            }
            // Valor
            String valor = "";
            if (chaveValor.length > 1) {
                valor = chaveValor[1].replaceAll("^\\s+", "").trim();
            }
            // Se for a definição de um método
            if (chave.equalsIgnoreCase("@Metodo")) {
                // Cria um nodo para conter os pares chave/valor de cada método
                nodo = new LinkedHashMap();
                // Associa o método ao nodo pai
                nodos.add(nodo);
                // Coloca o nome do elemento iterável como primeira entrada do nodo
                nodo.put("nodeName", "metodo");
                // Se tem nome do método
                if (!valor.isEmpty()) {
                    nodo.put("nomeInterno", valor);
                    nodo.put("nomeExterno", valor.replaceAll("-", "_"));
                }
            }
            // Se for a definição de nome Interno de um método
            if (chave.equalsIgnoreCase("@NomeInterno")) {
                nodo.put("nomeInterno", valor);
            }
            // Se for a definição de nome Externo de um método
            if (chave.equalsIgnoreCase("@NomeExterno")) {
                nodo.put("nomeExterno", valor);
            }
            // Se for a definição de Copys de um método
            if (chave.equalsIgnoreCase("@Copys")) {
                // Cria um ArrayList para conter os Copys
                List<Map> nodoCopys = new ArrayList();
                nodo.put("copys", nodoCopys);
                // Obtém os Copys
                String[] copys = valor.split(";");
                // Inclui os Copys na estrutura
                for (String copy : copys) {
                    // Cria novo nodo para um copy
                    Map<String, Object> nodoCopy = new LinkedHashMap();
                    nodoCopys.add(nodoCopy);
                    // Coloca o nome do elemento iterável como primeira entrada do nodo
                    nodoCopy.put("nodeName", "copy");
                    nodoCopy.put("nome", copy);
                }
            }
            // Se for a definição Usings de um método
            if (chave.equalsIgnoreCase("@Using")) {
                // Cria um ArrayList para conter os Usings
                List<Map> nodoUsings = new ArrayList();
                nodo.put("usings", nodoUsings);
                // Obtém os Usings
                String[] usings = valor.split(";");
                // Inclui os Usings na estrutura
                for (String using : usings) {
                    // Cria novo nodo para um copy
                    Map<String, Object> nodoUsing = new LinkedHashMap();
                    nodoUsings.add(nodoUsing);
                    // Coloca o nome do elemento iterável como primeira entrada do nodo
                    nodoUsing.put("nodeName", "using");
                    nodoUsing.put("nome", using);
                }
            }
            // Se for a definição de nome Using de um método
            if (chave.equalsIgnoreCase("@Returning")) {
                nodo.put("returning", valor);
            }
        }
    }

    /**
     * Retorna se a linha contém uma tag em linha de comentário
     *
     * @param line Linha do fonte Cobol a processar
     * @return Verdadeiro se a linha uma tag em linha de comentário
     */
    private boolean isTagComent(String line) {
        return line.startsWith("      *>-> @");
    }

}
