/*
 *  Copyright (C) 2012 Sandro Madruga Silveira
 *  sandrosilveira@gmail.com
 *
 *  Este programa foi desenvolvido para o trabalho de conclusão de
 *  curso apresentado como requisito parcial à obtenção do grau de
 *  Bacharel em Ciência da Computação pela Universidade Feevale.
 *
 *  Este programa não pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autorização do autor.
 *
 */
package org.gcex.especificacoes;

import java.io.IOException;
import java.util.*;
import org.gcex.especificacoes.estrutura.EspecificacaoBase;
import org.gcex.especificacoes.estrutura.EspecificacaoInterface;
import org.gcex.utilit.Arquivo;
import org.gcex.utilit.Texto;
import org.apache.commons.io.FilenameUtils;
import org.jdom2.*;
import org.jdom2.input.SAXBuilder;

/**
 * Especificação de entrada para arquivos XML
 *
 * @author Sandro Madruga Silveira
 */
public class EspecificacaoXML extends EspecificacaoBase implements EspecificacaoInterface {

    // Nome do arquivo XML a processar
    String nomeArqXml = "origem/TrabalhoSeguradora.xmi";
    // Nome da classe atual em processamento
    String classeAtual;
    // Map para carregar os tipos de dados existentes na modelagem
    Map<String, String> mapaTipoDados;
    // Nível de processamento do XML
    int nivel = 0;
    // Nível anterior de processamento do XML
    int nivelAnterior = 0;
    // Nodo pai onde serão acrescentados os demais
    Map<String, Object> pai;
    // Nome e valor de um elemento XML
    String nomeElemento;
    String valorElemento;
    // Indica se deve exibir a estrutura do XML (usado para debug)
    boolean mostraEstrutura = false;
    // Indica se o próximo elemento deve ser iterável em um Arraylist
    boolean iteravel;
    // Nome do elemento que tem repetição para ser iterável
    String elementoIteravel;

    /**
     * Construtor da classe de especificação, recebendo o elemento de origem a ser processado
     *
     * @param nomeElemento Nome do Elemento XML a processar
     */
    public EspecificacaoXML(String nomeElemento) {
        // Se passou nome sem extensão
        if (FilenameUtils.getExtension(nomeElemento).isEmpty()) {
            this.nomeArqXml = "origem/" + nomeElemento + ".xml";
            this.nomeElemento = nomeElemento;
        } else {
            this.nomeArqXml = nomeElemento;
            this.nomeElemento = FilenameUtils.getBaseName(nomeElemento);
        }
    }

    /**
     * Retorna uma lista com todas os elementos disponíveis para a especificação de entrada a partir de um
     * filtro específico.
     *
     * @param filtro Elemento e atributo a encontrar
     * @return Lista contendo os elementos disponíveis de acordo com o filtro aplicado
     */
    @Override
    public ArrayList<String> getListaDisponiveis(String filtro) {
        // Cria uma lista de elementos disponíveis no arquivo XML informado
        ArrayList<String> listaElementos = new ArrayList();
        SAXBuilder builder = new SAXBuilder();
        try {
            Document doc = builder.build(nomeArqXml);
            getXmlNodesClass(doc, listaElementos);
        } catch (JDOMException e) {
            System.out.println(nomeArqXml + " não é um xml formatado corretamente!");
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e);
        }

        // Retorna a lista de elementos disponíveis no arquivo XML informado
        return listaElementos;

    }

    /**
     * Busca os elementos XML, colocando na lista de elementos disponíveis
     *
     * @param o o Nodo xml a ser processado
     * @param lst ArrayList que armazena a lista de elementos disponíveis
     */
    private void getXmlNodesClass(Object o, ArrayList<String> lst) {
        if (o instanceof Element) {
            Element element = (Element) o;

            System.out.println("e:" + element.getName() + "=");

            if (element.getName().equals("Class")) {
                if (element.getAttributeValue("name") != null &&
                        !element.getAttributeValue("name").equals("") &&
                        element.getAttributeValue("visibility") != null &&
                        element.getAttributeValue("visibility").equals("public")) {
                    lst.add(element.getAttributeValue("name"));
                }
            }
            List children = element.getContent();
            Iterator iterator = children.iterator();
            while (iterator.hasNext()) {
                Object child = iterator.next();
                getXmlNodesClass(child, lst);
            }
        } else if (o instanceof Document) {
            Document doc = (Document) o;
            List children = doc.getContent();
            Iterator iterator = children.iterator();
            while (iterator.hasNext()) {
                Object child = iterator.next();
                getXmlNodesClass(child, lst);
            }
        } else if (o instanceof Comment) {
        } else if (o instanceof CDATA) {
        } else if (o instanceof Text) {
        } else if (o instanceof EntityRef) {
        } else if (o instanceof ProcessingInstruction) {
        } else {
            System.out.println("Tipo inesperado: " + o.getClass());
        }
    }

    /**
     * Retorna a referência para a estrutura da especificação de entrada que foi carregada
     *
     * @return Estrutura da especificação de entrada carregada
     */
    @Override
    public Map getEspecificacao() {
        // Cria a raiz da especificação
        root = new LinkedHashMap();
        // Coloca o nome da entidade no nodo raiz
        root.put("entidade", Texto.converteNomeClasse(nomeElemento));
        // Coloca informações extras no nodo raiz
        root.put("package", "model");
        // Adiciona os atributos extras passados por linha de comando
        addAtributosGeracao();
        // Nodo atual
        nodo = root;
        // Processa o XML
        SAXBuilder builder = new SAXBuilder();
        try {
            Document doc = builder.build(nomeArqXml);
            getXmlNodesAttribute(doc);
        } catch (JDOMException e) {
            System.out.println(nomeArqXml + " não é um xml formatado corretamente!");
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(nomeArqXml + " não conseguiu abrir o XML!");
            System.out.println(e);
        }
        // retorna a árvore com a especificação carregada
        return root;
    }

    /**
     * Busca os atributos de um elemento XML, adicionando a especificação
     *
     * @param o Nodo xml a ser processado
     */
    private void getXmlNodesAttribute(Object o) {
        // Trata cada um dos tipos de dados XML
        if (o instanceof Document) {
            // Obtém o documento
            Document doc = (Document) o;
            // Varre todos os filhos do documento
            List children = doc.getContent();
            Iterator iterator = children.iterator();
            while (iterator.hasNext()) {
                Object child = iterator.next();
                getXmlNodesAttribute(child);
            }
        } else if (o instanceof Element) {
            // Incrementa o nível hierárquico (nivel de elemento)
            nivel++;
            // Obtém o elemento
            Element elemento = (Element) o;
            // Executa o processamento do elemento
            processaElemento(elemento);
            // Incrementa o nível hierárquico (nivel de atributo)
            nivel++;
            // Processa os atributos do elemento
            List atributes = elemento.getAttributes();
            Iterator iteratox = atributes.iterator();
            while (iteratox.hasNext()) {
                Object o2 = iteratox.next();
                if (o2 instanceof Attribute) {
                    getXmlNodesAttribute(o2);
                }
            }
            // Encerra o nível de atributos
            nivel--;
            // Salva o nome do elemento iterável
            String saveElementoIteravel = elementoIteravel;
            // Salva o array atual
            List<Map> saveNodos = nodos;
            // Salva o nodo atual
            Map<String, Object> saveNodo = nodo;
            // Obtém os elementos filhos deste elemento
            List children = elemento.getContent();
            nivelAnterior = nivel;
            Iterator iterator = children.iterator();
            while (iterator.hasNext()) {
                Object child = iterator.next();
                getXmlNodesAttribute(child);
            }
            // Restaura o nome do elemento iteravel
            elementoIteravel = saveElementoIteravel;
            // Restaura o array atual
            nodos = saveNodos;
            // Restaura o nodo atual
            nodo = saveNodo;
            // Encerra o nível de elemento
            nivel--;
        } else if (o instanceof Attribute) {
            Attribute atributo = (Attribute) o;
            processaAtributo(atributo);
            // adicionaCaracteristica(chave, valor);
        } else if (o instanceof Comment) {
        } else if (o instanceof CDATA) {
        } else if (o instanceof Text) {
        } else if (o instanceof EntityRef) {
        } else if (o instanceof ProcessingInstruction) {
        } else {
            System.out.println("Tipo inesperado: " + o.getClass());
        }
    }

    /**
     * Processa um elemento XML
     *
     * @param elemento
     */
    private void processaElemento(Element elemento) {
        nomeElemento = elemento.getName();
        valorElemento = elemento.getText().trim();
        mostraEstruturaXml(nivel, "element", nomeElemento, valorElemento);
        // Se o elemento é iterável em um Arraylist
        if (iteravel) {
            // Nome do elemento que tem repetição para ser iterável
            elementoIteravel = nomeElemento;
            // Desliga o flag, visto que só deve setar para o prímeiro elemento do grupo que repete
            iteravel = false;
        }
        // Se for o nome do elemento iterável
        if (nomeElemento.equals(elementoIteravel)) {
            // Cria um novo nodo para incluir no ArrayList
            nodo = new LinkedHashMap();
            // Coloca o novo nodo no arrayList
            nodos.add(nodo);
            // Coloca o nome do elemento iterável como primeira entrada do nodo
            nodo.put("nodeName", elementoIteravel);
        } else {
//            if (!valorElemento.isEmpty()) {
                adicionaCaracteristica(nomeElemento, valorElemento);
//            }
        }
    }

    /**
     * Processa um atributo XML
     *
     * @param atributo
     */
    private void processaAtributo(Attribute atributo) {
        String chave = atributo.getName();
        String valor = atributo.getValue().trim();
        mostraEstruturaXml(nivel, "attribute", chave, valor);
        // Se encontrou atributo de grupo iterador
        if (nivel > 2 && valorElemento.isEmpty() && chave.equalsIgnoreCase("iterador") && valor.equalsIgnoreCase("sim")) {
            // Cria uma lista de nodos que serão inseridos no nodo atual, permitindo a iteração
            // destes elementos nos templates
            nodos = new ArrayList();
            // Coloca a lista de nodos no nodo atual nomeando o mesmo
            nodo.put(nomeElemento, nodos);
            // Indica que o próximo elemento deve ser iterável em um Arraylist
            iteravel = true;
        } else {
            adicionaCaracteristica(chave, valor);
        }
    }

    /**
     * Mostra estrutura do xml (usado para debug)
     */
    private void mostraEstruturaXml(int niv, String tipo, String chave, String valor) {
        if (mostraEstrutura) {
            mostraIdentacaoNivel();
            System.out.println(niv + " " + tipo + "::" + chave + "=" + valor);
        }
    }

    /**
     * Mostra identação de acordo com o nível hierárquico do XML
     */
    private void mostraIdentacaoNivel() {
        for (int i = 0; i < nivel; i++) {
            System.out.print("  ");
        }
    }
}
