/*
 *  Copyright (C) 2012 Sandro Madruga Silveira
 *  sandrosilveira@gmail.com
 *
 *  Este programa foi desenvolvido para o trabalho de conclusão de
 *  curso apresentado como requisito parcial à obtenção do grau de
 *  Bacharel em Ciência da Computação pela Universidade Feevale.
 *
 *  Este programa não pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autorização do autor.
 *
 */
package org.gcex.especificacoes;

import java.io.IOException;
import java.util.*;
import org.gcex.especificacoes.estrutura.EspecificacaoBase;
import org.gcex.especificacoes.estrutura.EspecificacaoInterface;
import org.gcex.utilit.Texto;
import org.jdom2.*;
import org.jdom2.input.SAXBuilder;

/**
 * Especificação de entrada para arquivos XML
 *
 * @author Sandro Madruga Silveira
 */
public class EspecificacaoArgoUML extends EspecificacaoBase implements EspecificacaoInterface {

    // Nome do arquivo XML a processar
    String nomeArqXml = "origem/TrabalhoSeguradora.xmi";
    // Nome da classe atual em processamento
    String classeAtual;
    // Map para carregar os tipos de dados existentes na modelagem
    Map<String, String> mapaTipoDados;

    /**
     * Construtor da classe de especificação, recebendo o elemento de origem a ser processado
     *
     * @param nomeElemento Nome do Elemento XML a processar
     */
    public EspecificacaoArgoUML(String nomeElemento) {
        this.nomeElemento = nomeElemento;
    }

    /**
     * Retorna uma lista com todas os elementos disponíveis para a especificação de entrada a partir de um
     * filtro específico.
     *
     * @param filtro Elemento e atributo a encontrar
     * @return Lista contendo os elementos disponíveis de acordo com o filtro aplicado
     */
    @Override
    public ArrayList<String> getListaDisponiveis(String filtro) {
        // Cria uma lista de elementos disponíveis no arquivo XML informado
        ArrayList<String> listaElementos = new ArrayList();

        SAXBuilder builder = new SAXBuilder();
        try {
            Document doc = builder.build(nomeArqXml);
            getXmlNodesClass(doc, listaElementos);
        } catch (JDOMException e) {
            System.out.println(nomeArqXml + " não é um xml formatado corretamente!");
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e);
        }

        // Retorna a lista de elementos disponíveis no arquivo XML informado
        return listaElementos;

    }

    /**
     * Busca os elementos XML, colocando na lista de elementos disponíveis
     *
     * @param obj o Nodo xml a ser processado
     * @param lst ArrayList que armazena a lista de elementos disponíveis
     */
    private void getXmlNodesClass(Object obj, ArrayList<String> lst) {
        if (obj instanceof Element) {
            Element element = (Element) obj;
            if (element.getName().equals("Class")) {
                if (element.getAttributeValue("name") != null
                        && !element.getAttributeValue("name").equals("")
                        && element.getAttributeValue("visibility") != null
                        && element.getAttributeValue("visibility").equals("public")) {
                    lst.add(element.getAttributeValue("name"));
                }
            }
            List children = element.getContent();
            Iterator iterator = children.iterator();
            while (iterator.hasNext()) {
                Object child = iterator.next();
                getXmlNodesClass(child, lst);
            }
        } else if (obj instanceof Document) {
            Document doc = (Document) obj;
            List children = doc.getContent();
            Iterator iterator = children.iterator();
            while (iterator.hasNext()) {
                Object child = iterator.next();
                getXmlNodesClass(child, lst);
            }
        } else if (obj instanceof Comment) {
        } else if (obj instanceof CDATA) {
        } else if (obj instanceof Text) {
        } else if (obj instanceof EntityRef) {
        } else if (obj instanceof ProcessingInstruction) {
        } else {
            System.out.println("Tipo inesperado: " + obj.getClass());
        }
    }

    /**
     * Retorna a referência para a estrutura da especificação de entrada que foi carregada
     *
     * @return Estrutura da especificação de entrada carregada
     */
    @Override
    public Map getEspecificacao() {
        // Cria a raiz da especificação
        root = new LinkedHashMap();

        // Coloca o nome da entidade no nodo raiz
        root.put("entidade", Texto.converteNomeClasse(nomeElemento));

        // Coloca informações extras no nodo raiz
        root.put("autor", "Sandro Silveira");
        root.put("package", "model");
        root.put("tabela", nomeElemento.toUpperCase());
        addAtributosGeracao();

        // Cria uma lista de atributos que serão inseridos no nodo raiz, permitindo a iteração
        // destes elementos nos templates
        nodos = new ArrayList();
        root.put("atributos", nodos);

        // Extrai mapa com os tipos de dados usados no modelo
        extraiMapaTipoDados();

        SAXBuilder builder = new SAXBuilder();
        try {
            Document doc = builder.build(nomeArqXml);
            getXmlNodesAttribute(doc);
        } catch (JDOMException e) {
            System.out.println(nomeArqXml + " não é um xml formatado corretamente!");
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e);
        }

        // retorna a árvore com a especificação carregada
        return root;
    }

    /**
     * Busca os atributos de um elemento XML, adicionando a especificação
     *
     * @param o o Nodo xml a ser processado
     */
    private void getXmlNodesAttribute(Object o) {
        if (o instanceof Element) {
            Element element = (Element) o;
            // Extrai o nome da classe
            if (element.getName().equals("Class")) {
                if (element.getAttributeValue("name") != null) {
                    classeAtual = element.getAttributeValue("name");
                    if (classeAtual.equals(nomeElemento)) {
                        // Adiciona o atributo chave em um novo nodo
                        adicionaNomeAtributo("id");
                        // Adiciona as características do atributo
                        adicionaCaracteristica("acesso", "public");
                        adicionaCaracteristica("isPk", true);
                        adicionaCaracteristica("descricao", "identificador");
                        adicionaCaracteristica("tipo", "int");
                    }
                }
            }
            // Extrai o atributo se for da classe que está sendo carregada
            if (element.getName().equals("Attribute") && classeAtual.equals(nomeElemento)) {
                // Adiciona o atributo em um novo nodo
                adicionaNomeAtributo(Texto.converteNomeAtributo(element.getAttributeValue("name")));
                // Adiciona as características do atributo
                adicionaCaracteristica("acesso", element.getAttributeValue("visibility"));
                adicionaCaracteristica("isPk", false);
                adicionaCaracteristica("descricao", "atributo " + element.getAttributeValue("name"));
            }
            // Extrai o tipo de atributo se for da classe que está sendo carregada
            if (element.getName().equals("StructuralFeature.type") && classeAtual.equals(nomeElemento)) {
                // Busca os elementos filhos
                List filhos = element.getContent();
                Iterator iterator = filhos.iterator();
                while (iterator.hasNext()) {
                    Object o2 = iterator.next();
                    if (o2 instanceof Element) {
                        Element filho = (Element) o2;
                        if (filho.getName().equals("DataType") || filho.getName().equals("Class")) {
                            String idRef = filho.getAttributeValue("xmi.idref");
                            adicionaCaracteristica("tipo", mapaTipoDados.get(idRef));
                        }
                    }
                }
            }

            // Extrai o comentário sobre o atributo que está sendo carregado
            if (element.getName().equals("ModelElement.taggedValue") && classeAtual.equals(nomeElemento)) {
                // Busca os elementos filhos
                List filhos = element.getContent();
                Iterator iterator = filhos.iterator();
                while (iterator.hasNext()) {
                    Object o2 = iterator.next();
                    if (o2 instanceof Element) {
                        Element filho = (Element) o2;
                        if (filho.getName().equals("TaggedValue")) {
                            // Busca os elementos netos
                            List netos = filho.getContent();
                            Iterator iterator2 = netos.iterator();
                            while (iterator2.hasNext()) {
                                Object o3 = iterator2.next();
                                if (o3 instanceof Element) {
                                    Element neto = (Element) o3;
                                    if (neto.getName().equals("TaggedValue.dataValue")) {
                                        String desc = neto.getText();
                                        adicionaCaracteristica("descricao", desc);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // Busca os elementos filhos
            List children = element.getContent();
            Iterator iterator = children.iterator();
            while (iterator.hasNext()) {
                Object child = iterator.next();
                getXmlNodesAttribute(child);
            }
        } else if (o instanceof Document) {
            Document doc = (Document) o;
            List children = doc.getContent();
            Iterator iterator = children.iterator();
            while (iterator.hasNext()) {
                Object child = iterator.next();
                getXmlNodesAttribute(child);
            }
        } else if (o instanceof Comment) {
        } else if (o instanceof CDATA) {
        } else if (o instanceof Text) {
        } else if (o instanceof EntityRef) {
        } else if (o instanceof ProcessingInstruction) {
        } else {
            System.out.println("Tipo inesperado: " + o.getClass());
        }
    }

    /**
     * Carrega o mapa com os tipos de dados existentes na modelagem
     */
    private void extraiMapaTipoDados() {
        // Cria o mapa com os tipos de dados
        mapaTipoDados = new LinkedHashMap();

        SAXBuilder builder = new SAXBuilder();
        try {
            Document doc = builder.build(nomeArqXml);
            getXmlNodesDataType(doc);
        } catch (JDOMException e) {
            System.out.println(nomeArqXml + " não é um xml formatado corretamente!");
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    /**
     * Busca os tipos de dados de um modelo em XML, adicionando a um mapa de tipos de dados
     *
     * @param o o Nodo xml a ser processado
     */
    private void getXmlNodesDataType(Object o) {
        if (o instanceof Element) {
            Element element = (Element) o;
            // Se encontrou a definição de um tipo de dados
            if (element.getName().equals("DataType")) {
                if (element.getAttributeValue("xmi.id") != null && element.getAttributeValue("name") != null) {
                    String idTipoDados = element.getAttributeValue("xmi.id");
                    String nomeTipoDados = element.getAttributeValue("name");
                    mapaTipoDados.put(idTipoDados, nomeTipoDados);
                }
            }
            // Se encontrou a definição de um tipo de dados nomeado
            if (element.getName().equals("Namespace.ownedElement")) {
                // Busca os elementos filhos
                List filhos = element.getContent();
                Iterator iterator = filhos.iterator();
                while (iterator.hasNext()) {
                    Object o2 = iterator.next();
                    if (o2 instanceof Element) {
                        Element filho = (Element) o2;
                        if (filho.getName().equals("Class")) {
                            if (filho.getAttributeValue("xmi.id") != null && filho.getAttributeValue("name") != null) {
                                String idTipoDados = filho.getAttributeValue("xmi.id");
                                String nomeTipoDados = filho.getAttributeValue("name");
                                if (nomeTipoDados.equals("Money")) {
                                    nomeTipoDados = "double";
                                }
                                mapaTipoDados.put(idTipoDados, nomeTipoDados);
                            }
                        }
                    }
                }
            }

            // Busca os elementos filhos
            List children = element.getContent();
            Iterator iterator = children.iterator();
            while (iterator.hasNext()) {
                Object child = iterator.next();
                getXmlNodesDataType(child);
            }
        } else if (o instanceof Document) {
            Document doc = (Document) o;
            List children = doc.getContent();
            Iterator iterator = children.iterator();
            while (iterator.hasNext()) {
                Object child = iterator.next();
                getXmlNodesDataType(child);
            }
        } else if (o instanceof Comment) {
        } else if (o instanceof CDATA) {
        } else if (o instanceof Text) {
        } else if (o instanceof EntityRef) {
        } else if (o instanceof ProcessingInstruction) {
        } else {
            System.out.println("Tipo inesperado: " + o.getClass());
        }
    }
}
