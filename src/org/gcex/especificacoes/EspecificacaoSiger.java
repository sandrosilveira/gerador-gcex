/*
 *  Copyright (C) 2012 Sandro Madruga Silveira
 *  sandrosilveira@gmail.com
 *
 *  Este programa foi desenvolvido para o trabalho de conclusão de
 *  curso apresentado como requisito parcial à obtenção do grau de
 *  Bacharel em Ciência da Computação pela Universidade Feevale.
 *
 *  Este programa não pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autorização do autor.
 *
 */
package org.gcex.especificacoes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.gcex.especificacoes.estrutura.EspecificacaoBase;
import org.gcex.especificacoes.estrutura.EspecificacaoInterface;
import org.gcex.utilit.DBException;
import org.gcex.utilit.DBManager;
import org.gcex.utilit.Texto;

/**
 * Especificação de entrada para dicionário de dados do SIGER
 *
 * @author Sandro Madruga Silveira
 */
public class EspecificacaoSiger extends EspecificacaoBase implements EspecificacaoInterface {

    private String tableName;
    DBManager db;
    int nroTabDic = 0;

    /**
     * Construtor da classe de especificação, recebendo o elemento de origem a ser processado
     *
     * @param table Nome da tabela do banco de dados a processar
     */
    public EspecificacaoSiger(String tableName) {
        this.nomeElemento = tableName;
        carregaConfiguracaoBancoDados("siger");
    }

    /**
     * Retorna uma lista com todas os elementos disponíveis para a especificação de entrada a partir de um
     * filtro específico.
     *
     * @param filtro Esquema do banco de dados, ou se não for informado retorna uma lista de esquemas
     * @return Lista contendo os arquivos .INI disponíveis em um determinado diretório
     */
    @Override
    public ArrayList<String> getListaDisponiveis(String schema) {

        // Cria uma lista de tabelas disponíveis na conexão atual
        ArrayList<String> listaTabelas = new ArrayList();

        // Conecta com o banco de dados
        try {
            db = new DBManager(driver, url, usuario, senha);
        } catch (DBException ex) {
            ex.printStackTrace();
            return null;
        }

        // Carrega a lista de tabelas disponíveis no dicionário de dados do SIGER
        try {
            Statement stmt = db.getConexao().createStatement();
            ResultSet rs;
            rs = stmt.executeQuery("SELECT YA_L25 FROM YDICTB ORDER BY YA_L25");
            while (rs.next()) {
                listaTabelas.add(rs.getString("YA_L25"));
            }
            rs.close();
            stmt.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        // Fecha a conexão com o banco de dados
        try {
            db.desconecta();
        } catch (DBException ex) {
            Logger.getLogger(EspecificacaoBancoDados.class.getName()).log(Level.SEVERE, null, ex);
        }

        // Retorna a lista de tabelas do dicionário de dados do SIGER
        return listaTabelas;
    }

    /**
     * Retorna a referência para a estrutura da especificação de entrada que foi carregada
     *
     * @return Estrutura da especificação de entrada carregada
     */
    @Override
    public Map getEspecificacao() {
        // Conecta com o banco de dados
        try {
            db = new DBManager(driver, url, usuario, senha);
        } catch (DBException ex) {
            ex.printStackTrace();
            return null;
        }

        // Cria a raiz da especificação
        root = new LinkedHashMap();

        // Nome da entidade
        String nomeCurto = null;
        try {
            Statement stmt = db.getConexao().createStatement();
            ResultSet rs;
            rs = stmt.executeQuery("SELECT YA_L25 FROM YDICTB where YDICTB.YA_NBD = '" + nomeElemento + "'");
            while (rs.next()) {
                nomeCurto = rs.getString("YA_L25");
            }
            rs.close();
            stmt.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        // Coloca o nome da entidade no nodo raiz
        root.put("entidade", Texto.converteNomeClasse(nomeCurto));

        // Coloca informações extras no nodo raiz
        root.put("autor", "Sandro Silveira");
        root.put("package", "model");
        root.put("tabela", nomeElemento.toUpperCase());
        addAtributosGeracao();

        // Cria uma lista de atributos que serão inseridos no nodo raiz, permitindo a iteração
        // destes elementos nos templates
        nodos = new ArrayList();
        root.put("atributos", nodos);

        // Varre a estrutura da tabela extraindo seus atributos
        try {
            Statement stmt = db.getConexao().createStatement();
            ResultSet rs;
            rs = stmt.executeQuery("SELECT * FROM YDICTB "
                    + "INNER JOIN YDICCA ON YDICTB.YA_TAB = YDICCA.YI_CAR "
                    + "WHERE YDICTB.YA_ARQ = '" + nomeElemento + "' AND YI_BDA = 1 "
                    + "ORDER BY YI_SEQ");
            while (rs.next()) {
                // Atualiza o número da tabela no dicionário de dados do SIGER
                if (nroTabDic == 0) {
                    nroTabDic = rs.getInt("YI_CAR");
                }
                // Adiciona o atributo em um novo nodo
                adicionaNomeAtributo(Texto.converteNomeAtributo(rs.getString("YI_LEG")));
                // Adiciona as características do atributo
                adicionaCaracteristica("descricao", rs.getString("YI_L25"));
                adicionaCaracteristica("coluna", rs.getString("YI_NBD"));
                adicionaCaracteristica("tipoBanco", getTipoDadosSiger(rs.getInt("YI_TBD")));
                adicionaCaracteristica("isPk", isPk(rs.getInt("YI_PCP")));
                adicionaCaracteristica("acesso", "private");
                adicionaCaracteristica("nulos", false);
                adicionaCaracteristica("tipo", getTipoDadosJava(getTipoDadosSiger(rs.getInt("YI_TBD"))));
                adicionaCaracteristica("metodo_set", getMetodoSet(getTipoDadosSiger(rs.getInt("YI_TBD"))));
                adicionaCaracteristica("objeto_compat", getObjetoCompativel(getTipoDadosSiger(rs.getInt("YI_TBD"))));
                adicionaCaracteristica("tamanho", rs.getInt("YI_TAM"));
                adicionaCaracteristica("decimais", rs.getInt("YI_DEC"));
            }
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        // Cria uma lista de colunas da tabela que serão inseridas no nodo raiz, permitindo a iteração
        // destes elementos nos templates
        nodos = new ArrayList();
        root.put("primary_key", nodos);

        // Varre a estrutura da tabela extraindo seus atributos
        try {
            Statement stmt = db.getConexao().createStatement();
            ResultSet rs;
            rs = stmt.executeQuery("SELECT YV_NOM, YV_CCH, YV_SEQ, YV_TAB, YV_CHP, YI_LEG, YI_NBD FROM YDICCH "
                    + "INNER JOIN YDICCA ON YV_CCH = YI_CAM  "
                    + "WHERE YV_TAB = " + nroTabDic + " AND YV_CHP = 1 AND YI_BDA = 1 "
                    + "ORDER BY YV_SEQ");
            while (rs.next()) {
                // Adiciona a coluna que compõe a pk em novo nodo
                adicionaNomeAtributo(rs.getString("YI_NBD"));
                // Adiciona as características da coluna na pk
                adicionaCaracteristica("referencia", Texto.converteNomeAtributo(rs.getString("YI_LEG")));
                adicionaCaracteristica("key_seq", rs.getInt("YV_SEQ"));
                adicionaCaracteristica("pk_name", rs.getString("YV_NOM"));
            }
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        // Fecha a conexão com o banco de dados
        try {
            db.desconecta();
        } catch (DBException ex) {
            Logger.getLogger(EspecificacaoSiger.class.getName()).log(Level.SEVERE, null, ex);
        }
        // retorna a árvore com a especificação carregada
        return root;
    }

    /**
     * Verifica se a coluna faz parte da Primary Key
     *
     * @param flag Flag indicador de campo que faz parte da chave principal (1=Sim, 2=Não)
     * @return Verdadeiro se a coluna faz parte da Primary Key
     */
    private Boolean isPk(int flag) {
        if (flag == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Busca o tipo de dados do SIGER a partir do código do tipo de dados
     *
     * @param codigoTipo Inteiro com o código do tipo de dados do banco de dados do SIGER
     * @return String com o tipo de dados
     */
    private String getTipoDadosSiger(int codigoTipo) {
        switch (codigoTipo) {
            case 1:
                return "CHAR";
            case 2:
                return "VARCHAR";
            case 3:
                return "SMALLINT";
            case 4:
                return "INTEGER";
            case 5:
                return "DOUBLE";
            case 6:
                return "DECIMAL";
            case 7:
                return "DATE";
            case 8:
                return "TIME";
            case 9:
                return "TIMESTAMP";
            case 10:
                return "BINARY";
            default:
                return "VARCHAR";
        }
    }
}
