/*
 *  Copyright (C) 2012 Sandro Madruga Silveira
 *  sandrosilveira@gmail.com
 *
 *  Este programa foi desenvolvido para o trabalho de conclusão de
 *  curso apresentado como requisito parcial à obtenção do grau de
 *  Bacharel em Ciência da Computação pela Universidade Feevale.
 *
 *  Este programa não pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autorização do autor.
 *
 */
package org.gcex.especificacoes;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringTokenizer;
import org.gcex.especificacoes.estrutura.EspecificacaoBase;
import org.gcex.especificacoes.estrutura.EspecificacaoInterface;
import org.gcex.utilit.Texto;

/**
 * Especificação de entrada para arquivos csv
 *
 * @author Sandro Madruga Silveira
 */
public class EspecificacaoCSV extends EspecificacaoBase implements EspecificacaoInterface {

    /**
     * Array que contém o nome das colunas do arquivo csv
     */
    private String[] columns;

    /**
     * Construtor da classe de especificação, recebendo o elemento de origem a ser processado
     *
     * @param csvFileName Nome do arquivo ini a processar
     */
    public EspecificacaoCSV(String csvFileName) {
        this.nomeElemento = csvFileName;
        this.fileName = "origem/" + csvFileName + ".csv";
    }

    /**
     * Retorna uma lista com todos os elementos disponíveis para a especificação de entrada a partir de um
     * filtro específico.
     *
     * @param filtro Diretório para examinar arquivos ini disponíveis
     * @return Lista contendo os arquivos ini disponíveis em um determinado diretório
     */
    @Override
    public ArrayList<String> getListaDisponiveis(String filtro) {
        // TODO: Implementar
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Retorna a referência para a estrutura da especificação de entrada que foi carregada
     *
     * @return Estrutura da especificação de entrada carregada
     */
    @Override
    public Map getEspecificacao() {
        // Abre o arquivo csv para leitura
        BufferedReader iniFile = abreArquivo(fileName);
        // Cria a raiz da especificação
        root = new LinkedHashMap();

        // Coloca o nome da entidade no nodo raiz
        // TODO: Criar método para extrair o nome base do arquivo sem ponto/extensão
        root.put("entidade", nomeElemento);

        // Coloca informações extras no nodo raiz
        // TODO: Criar método para fazer isto, adicionar dados ao root ou aos nodos
        root.put("autor", "Sandro Silveira");
        root.put("package", "model");
        addAtributosGeracao();

        // Cria uma lista de atributos que serão inseridos no nodo raiz, permitindo a iteração
        // dos elementos nos templates
        nodos = new ArrayList();
        root.put("linhas", nodos);

        // Processa o arquivo csv
        String line;
        try {
            while ((line = iniFile.readLine()) != null) {
                parseLine(line);
            }
        } catch (IOException ex) {
            System.out.println("Erro de leitura!");
        }

        // Fecha o arquivo em processamento
        fechaArquivo(iniFile);
        // retorna a árvore com a especificação carregada
        return root;
    }

    /**
     * Processa uma linha do arquivo csv
     *
     * @param line Linha do arquivo ini a processar
     */
    private void parseLine(String line) {
        // Não processa linhas vazias
        if (line.isEmpty()) {
            return;
        }
        // Contador de colunas
        int col = 0;
        // Separa os elementos da linha
        StringTokenizer st = new StringTokenizer(line, ";");
        // Se ainda não processou o cabeçalho
        if (columns == null) {
            // Cria o array com a descrição das colunas
            columns = new String[st.countTokens()];
            while (st.hasMoreTokens()) {
                columns[col++] = Texto.retiraAspas(st.nextToken());
            }
        } else {
            // Cria um nodo para conter os pares chave/valor de cada linha do csv
            nodo = new LinkedHashMap();
            // Associa a seção ao nodo raiz
            nodos.add(nodo);
            // Coloca os dados da linha na especificação
            while (st.hasMoreTokens()) {
                nodo.put(columns[col++], st.nextToken());
            }
        }
    }
}
