/*
 *  Copyright (C) 2012 Sandro Madruga Silveira
 *  sandrosilveira@gmail.com
 *
 *  Este programa foi desenvolvido para o trabalho de conclusão de
 *  curso apresentado como requisito parcial à obtenção do grau de
 *  Bacharel em Ciência da Computação pela Universidade Feevale.
 *
 *  Este programa não pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autorização do autor.
 */
package org.gcex.especificacoes;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.gcex.especificacoes.estrutura.EspecificacaoBase;
import org.gcex.especificacoes.estrutura.EspecificacaoInterface;
import org.gcex.utilit.DBException;
import org.gcex.utilit.DBManager;
import org.gcex.utilit.Texto;

/**
 * Especificação de entrada que carrega a estrutura de metadados de banco de dados
 *
 * @author Sandro Madruga Silveira
 */
public class EspecificacaoBancoDados extends EspecificacaoBase implements EspecificacaoInterface {

    /** Conexão com o banco de dados */
    private DBManager dataBase;

    /**
     * Construtor da classe de especificação, recebendo o elemento de origem a ser processado
     *
     * @param tableName Nome da tabela do banco de dados a processar
     */
    public EspecificacaoBancoDados(String tableName) {
        carregaConfiguracaoBancoDados("banco");
        this.nomeElemento = tableName;
        // Conecta com o banco de dados
        try {
            dataBase = new DBManager(driver, url, usuario, senha);
        } catch (DBException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Retorna uma lista com todos os elementos disponíveis para a especificação de entrada a partir de um
     * filtro específico.
     *
     * @param filtro Esquema do banco de dados
     * @return Lista contendo as tabelas do esquema informado
     */
    @Override
    public ArrayList<String> getListaDisponiveis(String schema) {
        // Cria uma lista de tabelas disponíveis na conexão atual
        ArrayList<String> listaTabelas = new ArrayList();

        // Conecta com o banco de dados
        try {
            dataBase = new DBManager(driver, url, usuario, senha);
        } catch (DBException ex) {
            ex.printStackTrace();
            return null;
        }

        // Monta a lista de tabelas
        try {
            DatabaseMetaData md;
            try {
                md = dataBase.getConexao().getMetaData();
            } catch (SQLException ex) {
                ex.printStackTrace();
                return null;
            }
            // Lista todas as tabelas de um determinado schema
            ResultSet rs = md.getTables(null, usuario.toUpperCase(), "%", null);
            while (rs.next()) {
                listaTabelas.add(rs.getString(3));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        // Fecha a conexão com o banco de dados
        try {
            dataBase.desconecta();
        } catch (DBException ex) {
            Logger.getLogger(EspecificacaoBancoDados.class.getName()).log(Level.SEVERE, null, ex);
        }

        // Retorna a lista de tabelas
        return listaTabelas;

    }

    /**
     * Retorna a referência para a estrutura da especificação de entrada que foi carregada
     *
     * @return Estrutura da especificação de entrada carregada
     */
    @Override
    public Map getEspecificacao() {
        // Cria a raiz da especificação
        root = new LinkedHashMap();

        // Coloca o nome da entidade no nodo raiz
        root.put("entidade", Texto.converteNomeClasse(nomeElemento));

        // Coloca informações extras no nodo raiz
        root.put("autor", "Sandro Silveira");
        root.put("package", "model");
        root.put("tabela", nomeElemento.toUpperCase());
        addAtributosGeracao();

        // Cria uma lista de atributos que serão inseridos no nodo raiz, permitindo a iteração
        // destes elementos nos templates
        nodos = new ArrayList();
        root.put("atributos", nodos);

        // Varre a estrutura da tabela extraindo seus atributos
        try {
            DatabaseMetaData md = null;
            try {
                md = dataBase.getConexao().getMetaData();
            } catch (SQLException ex) {
                System.out.println("Erro retornando estrutura da tabela do banco de dados!");
            }
            ResultSet rs = md.getColumns(null, null, nomeElemento, null);
            while (rs.next()) {
                // Nome da coluna para verificar configurações especiais
                nomeColuna = rs.getString("COLUMN_NAME");
                // Adiciona o atributo em um novo nodo
                adicionaNomeAtributo(Texto.converteNomeAtributo(rs.getString("COLUMN_NAME")));
                nodo.put("nodeName", "atributo");
                // Adiciona as características do atributo
                adicionaCaracteristica("descricao", rs.getString("REMARKS"));
                adicionaCaracteristica("coluna", rs.getString("COLUMN_NAME"));
                adicionaCaracteristica("tipoBanco", rs.getString("TYPE_NAME"));
                adicionaCaracteristica("isPk", isPk(rs.getString("COLUMN_NAME")));
                adicionaCaracteristica("acesso", "private");
                adicionaCaracteristica("nulos", rs.getBoolean("NULLABLE"));
                adicionaCaracteristica("tipo", getTipoDadosJava(rs.getString("TYPE_NAME")));
                adicionaCaracteristica("isPrimitive", isPrimitive(getTipoDadosJava(rs.getString("TYPE_NAME"))));
                adicionaCaracteristica("isAutoIncrement", isAutoIncrement(rs.getString("IS_AUTOINCREMENT")));
                adicionaCaracteristica("metodo_set", getMetodoSet(rs.getString("TYPE_NAME")));
                adicionaCaracteristica("objeto_compat", getObjetoCompativel(rs.getString("TYPE_NAME")));
                adicionaCaracteristica("tamanho", rs.getInt("COLUMN_SIZE"));
                adicionaCaracteristica("decimais", rs.getInt("DECIMAL_DIGITS"));
            }
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        // Cria uma lista de colunas da tabela que serão inseridas no nodo raiz, permitindo a iteração
        // destes elementos nos templates
        nodos = new ArrayList();
        root.put("primary_key", nodos);

        // Varre a estrutura da tabela extraindo a primary key
        try {
            DatabaseMetaData md = null;
            try {
                md = dataBase.getConexao().getMetaData();
            } catch (SQLException ex) {
                System.out.println("Erro retornando estrutura da primary key!");
            }
            ResultSet rs = md.getPrimaryKeys(null, null, nomeElemento);

            // Busca número de elementos da primary key
            rs.last();
            int numRows = rs.getRow();
            rs.beforeFirst();
            // Cria arrays com os elementos da primary key
            String[] columnName = new String[numRows];
            String[] referencia = new String[numRows];
            int[] sequencia = new int[numRows];
            String[] pkName = new String[numRows];
            // Coloca nos arrays os elementos da primary key, precisa disto porque md.getPrimaryKeys não retorna
            // os elementos devidamente ordenados para o MySQL em algumas situações
            while (rs.next()) {
                int pos = rs.getInt("KEY_SEQ") - 1;
                columnName[pos] = rs.getString("COLUMN_NAME");
                referencia[pos] = Texto.converteNomeAtributo(rs.getString("COLUMN_NAME"));
                sequencia[pos] = rs.getInt("KEY_SEQ");
                pkName[pos] = rs.getString("PK_NAME");
            }
            rs.close();
            // Coloca os elementos da primary key na especificação
            for (int i = 0; i < columnName.length; i++) {
                // Adiciona a coluna que compõe a pk em novo nodo
                adicionaNomeAtributo(columnName[i]);
                // Adiciona as características da coluna na pk
                adicionaCaracteristica("referencia", referencia[i]);
                adicionaCaracteristica("key_seq", sequencia[i]);
                adicionaCaracteristica("pk_name", pkName[i]);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        // Fecha a conexão com o banco de dados
        try {
            dataBase.desconecta();
        } catch (DBException ex) {
            Logger.getLogger(EspecificacaoBancoDados.class.getName()).log(Level.SEVERE, null, ex);
        }
        // retorna a árvore com a especificação carregada
        return root;
    }

    /**
     * Verifica se a coluna faz parte da Primary Key
     * @param colunaTabela Coluna da tabela a verificar se faz parte da Primary Key
     * @return Verdadeiro se a coluna faz parte da Primary Key
     */
    private Boolean isPk(String colunaTabela) {
        // Varre a estrutura da tabela extraindo a primary key
        try {
            DatabaseMetaData md = null;
            try {
                md = dataBase.getConexao().getMetaData();
            } catch (SQLException ex) {
                System.out.println("Erro verificando primary key!");
            }
            ResultSet rs = md.getPrimaryKeys(null, null, nomeElemento);
            while (rs.next()) {
                if (colunaTabela.equals(rs.getString("COLUMN_NAME"))) {
                    rs.close();
                    return true;
                }
            }
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

}
