/*
 *  Copyright (C) 2012 Sandro Madruga Silveira
 *  sandrosilveira@gmail.com
 *
 *  Este programa foi desenvolvido para o trabalho de conclusão de
 *  curso apresentado como requisito parcial à obtenção do grau de
 *  Bacharel em Ciência da Computação pela Universidade Feevale.
 *
 *  Este programa não pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autorização do autor.
 *
 */
package org.gcex.especificacoes.estrutura;

import java.util.ArrayList;
import java.util.Map;

/**
 * Interface que define o modelo que todas as especificações de entrada devem ter
 *
 * @author Sandro
 */
public interface EspecificacaoInterface {

    /**
     * Retorna uma lista com todas os elementos disponíveis para a especificação de entrada a partir de um
     * filtro específico.
     *
     * @param filtro dependente de cada implementação
     * @return Lista contendo os elementos disponíveis para a especificação de entrada
     */
    public ArrayList<String> getListaDisponiveis(String filtro);

    /**
     * Retorna uma especificação de entrada para o núcleo de geração de código
     * @return Map que contém a estrutura com a especificação de entrada
     */
    public Map getEspecificacao();
}
