/*
 *  Copyright (C) 2012 Sandro Madruga Silveira
 *  sandrosilveira@gmail.com
 *
 *  Este programa foi desenvolvido para o trabalho de conclusão de
 *  curso apresentado como requisito parcial à obtenção do grau de
 *  Bacharel em Ciência da Computação pela Universidade Feevale.
 *
 *  Este programa não pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autorização do autor.
 *
 */
package org.gcex.especificacoes.estrutura;

/**
 * Classe para tratamento de exceções das especificações de entrada
 * @author Sandro
 */
public class EspecificacaoException extends Exception {

    /**
     * Cria uma nova exceção passando uma mensagem e a causa
     * @param msg
     * @param cause
     */
    public EspecificacaoException(String msg, Throwable cause) {
        super(msg, cause);
    }

    /**
     * Cria uma nova exceção passando apenas mensagem
     * @param msg
     */
    public EspecificacaoException(String msg) {
        this(msg, null);
    }

}
