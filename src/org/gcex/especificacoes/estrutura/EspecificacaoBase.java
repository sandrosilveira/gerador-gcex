/*
 *  Copyright (C) 2012 Sandro Madruga Silveira
 *  sandrosilveira@gmail.com
 *
 *  Este programa foi desenvolvido para o trabalho de conclusão de
 *  curso apresentado como requisito parcial à obtenção do grau de
 *  Bacharel em Ciência da Computação pela Universidade Feevale.
 *
 *  Este programa não pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autorização do autor.
 *
 */
package org.gcex.especificacoes.estrutura;

import java.io.*;
import java.util.*;
import org.gcex.utilit.CnvTipoDados;

/**
 * Classe base para a implementação de especificações de entrada, contém métodos comumente utilizados
 *
 * @author Sandro Madruga Silveira
 */
public abstract class EspecificacaoBase {

    protected Properties properties;
    protected String driver; // = "com.mysql.jdbc.Driver";
    protected String url; // = "jdbc:mysql://localhost/siger";
    protected String usuario; // = "root";
    protected String senha; // = "root";
    protected String schema; // = "siger";
    protected String nomeElemento;
    protected String nomeColuna;
    protected String fileName;
    protected String atributosGeracao;
    protected Map<String, Object> root;
    protected List<Map> nodos;
    protected Map<String, Object> nodo;
    protected Map<String, String> mapAtributosGeracao;
    private int nivel = 1;
    private boolean[] ultimo = new boolean[64];

    /**
     * Seta o nome do elemento a
     *
     * @return
     */
    public String getNomeElemento() {
        return nomeElemento;
    }

    /**
     *
     * @param nomeElemento
     */
    public void setNomeElemento(String nomeElemento) {
        this.nomeElemento = nomeElemento;
    }

    /**
     * Abre o arquivo
     *
     * @param filename
     */
    public BufferedReader abreArquivo(String fileName) {
        // TODO: Tratamento de text encoding (usando UTF-8 por default)
        // TODO: Tratamento de exceções de arquivo
        FileReader fileReader;
        try {
            fileReader = new FileReader(fileName);
        } catch (FileNotFoundException ex) {
            System.out.println("Arquivo " + fileName + " não encontrado!");
            return null;
        }
        BufferedReader file = new BufferedReader(fileReader);
        return file;
    }

    /**
     * Fecha o arquivo
     *
     * @param file
     */
    public void fechaArquivo(BufferedReader file) {
        // Fecha o arquivo ini
        try {
            file.close();
        } catch (IOException ex) {
            System.out.println("Erro fechando arquivo!");
        }
    }

    /**
     * Passa para a especificação a lista de atributos extras para geração
     *
     * @param atributosGeracao lista de atributos extras (chave:valor) para geração separados por ";"
     */
    public void setMapAtributosGeracao(String atributosGeracao) {
        mapAtributosGeracao = new LinkedHashMap();
        if (atributosGeracao == null || atributosGeracao.isEmpty()) {
            return;
        }
        String[] pares = atributosGeracao.split(";");
        for (int i = 0; i < pares.length; i++) {
            String[] chaveValor = pares[i].split(":");
            mapAtributosGeracao.put(chaveValor[0], chaveValor[1]);
        }
    }

    /**
     * Mostra a estrutura da especificação em forma de árvore
     */
    public void mostraEspecificacao() {
        System.out.println("");
        System.out.println("  (\\)");
        mostraMap(root);
        System.out.println("");
    }

    /**
     * Mostra a estrutura de um determinado nodo da especificação
     *
     * @param nodo Map com a estrutura de um nodo
     */
    private void mostraMap(Map<String, Object> nodo) {
        int seq = 0;
        for (Map.Entry<String, Object> entry : nodo.entrySet()) {
            mostraEntry(entry, ++seq, nodo.size());
        }
    }

    /**
     * Mostra a estrutura de uma entrada de um nodo da especificação (pares chave/valor ou outro nodo)
     *
     * @param entry
     */
    private void mostraEntry(Map.Entry<String, Object> entry, int seq, int size) {
        String key = entry.getKey();
        Object value = entry.getValue();
        ultimo[nivel] = seq == size;
        // Avalia tipo do valor armazenado na estrutura
        if (value instanceof String) {
            if (!key.equals("nodeName")) {
                mostraIdentado("+- " + key + " = " + value);
            }
        } else if (value instanceof Integer) {
            mostraIdentado("+- " + key + " = " + (Integer) value);
        } else if (value instanceof Date) {
            mostraIdentado("+- " + key + " = " + (Date) value);
        } else if (value instanceof Boolean) {
            mostraIdentado("+- " + key + " = " + (Boolean) value);
        } else if (value instanceof ArrayList) {
            mostraIdentado("+- " + key + " (arrayList)");
            nivel++;
            int seqArray = 0;
            ArrayList lista = (ArrayList) value;
            for (Object o : lista) {
                ultimo[nivel] = ++seqArray == lista.size();
                nodo = (Map<String, Object>) o;
                // Se tem uma entrada com o nome do nodo
                if (nodo.containsKey("nodeName")) {
                    mostraIdentado("+- " + nodo.get("nodeName") + "#" + seqArray);
                } else {
                    mostraIdentado("+- " + key + "#" + seqArray);
                }
                nivel++;
                mostraMap(nodo);
                nivel--;
            }
            nivel--;
        } else {
            mostraIdentado("+- " + key + " = <tipo de dado inválido>");
        }
    }

    /**
     * Mostra uma entrada da estrutura da especificação de forma identada, de acordo com o nível hierárquico
     *
     * @param str
     */
    private void mostraIdentado(String str) {
        mostraIdentacao();
        System.out.println("|");
        mostraIdentacao();
        System.out.println(str);
    }

    /**
     * Mostra os espaços ou barras antes do conteúdo identado
     */
    private void mostraIdentacao() {
        for (int i = 0; i < nivel; i++) {
            if (i == 0 || ultimo[i]) {
                System.out.print("   ");
            } else {
                System.out.print("|  ");
            }
        }
    }

    /**
     * Adiciona o atributo em um novo nodo
     *
     * @param nome
     */
    protected void adicionaNomeAtributo(String nome) {
        // Cria um nodo para conter os pares chave/valor com as características do atributo
        nodo = new LinkedHashMap();
        // Associa o atributo ao nodo raiz
        nodos.add(nodo);
        // Coloca o nome do atributo como uma das características
        nodo.put("nome", nome);
    }

    /**
     * Adiciona uma característica ao atributo em um par chave/valor para String
     *
     * @param key
     * @param value
     */
    protected void adicionaCaracteristica(String key, String value) {
        nodo.put(key, value);
    }

    /**
     *
     * /**
     * Adiciona uma característica ao atributo em um par chave/valor para boolean
     *
     * @param key
     * @param value
     */
    protected void adicionaCaracteristica(String key, boolean value) {
        nodo.put(key, value);
    }

    /**
     * Adiciona uma característica ao atributo em um par chave/valor para int
     *
     * @param key
     * @param value
     */
    protected void adicionaCaracteristica(String key, int value) {
        nodo.put(key, value);
    }

    /**
     * Converte o tipo de dados do banco de dados para o tipo de dados Java
     *
     * @param tipoDadosBanco String com o tipo de dados do banco de dados
     * @return String com o tipo de dados Java
     */
    protected String getTipoDadosJava(String tipoDadosBanco) {
        String tipo = properties.getProperty((nomeElemento + "." + nomeColuna + ".tipo").toLowerCase());
        if (tipo != null) {
            return tipo;
        } else {
            return CnvTipoDados.mysqlToJava(tipoDadosBanco);
        }
    }

    /**
     * Converte o tipo de dados do banco de dados para o tipo de método set para JDBC
     *
     * @param tipoDadosBanco String com o tipo de dados do banco de dados
     * @return String com o nome do método set para JDBC
     */
    protected String getMetodoSet(String tipoDadosBanco) {
        return CnvTipoDados.mysqlToMetodoSetJdbc(tipoDadosBanco);
    }

    /**
     * Converte o tipo de dados do banco de dados para o tipo de objeto Java compatível
     *
     * @param tipoDadosBanco String com o tipo de dados do banco de dados
     * @return String com o nome do tipo de objeto Java compatível
     */
    protected String getObjetoCompativel(String tipoDadosBanco) {
        return CnvTipoDados.mysqlToObjetoCompativel(tipoDadosBanco);
    }

    /**
     * Adiciona os atributos extras passados por linha de comando (opção -a)
     *
     */
    protected void addAtributosGeracao() {
        for (Map.Entry<String, String> entry : mapAtributosGeracao.entrySet()) {
            root.put(entry.getKey(), entry.getValue());
        }
    }

    /**
     * Carrega a configuração do banco de dados
     *
     * @param nomeConfiguracao
     */
    protected void carregaConfiguracaoBancoDados(String nomeConfiguracao) {
        properties = new Properties();
        // Lê o arquivo de propriedades
        try {
            properties.load(new FileInputStream("gerador.properties"));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        driver = properties.getProperty(nomeConfiguracao + ".conexao.driver");
        url = properties.getProperty(nomeConfiguracao + ".conexao.url");
        usuario = properties.getProperty(nomeConfiguracao + ".conexao.usuario");
        senha = properties.getProperty(nomeConfiguracao + ".conexao.senha");
        schema = properties.getProperty(nomeConfiguracao + ".conexao.schema");
    }

    /**
     * Verifica se o tipo de dados é primitivo
     *
     * @param tipoDado Tipo de dados a verificar
     * @return Verdadeiro se pode ser mapeado para primitivo
     */
    protected boolean isPrimitive(String tipoDado) {
        return (tipoDado.equals("int") || tipoDado.equals("long") || tipoDado.equals("char") ||
                tipoDado.equals("boolean") || tipoDado.equals("short") || tipoDado.equals("double") ||
                tipoDado.equals("float"));
    }

    /**
     * Verifica se campo é de auto incremento
     *
     * @param yesNo String vinda dos metadados indicando se é ou não auto incremento ("YES" ou "NO" ou vazio)
     * @return boolean
     */
    protected boolean isAutoIncrement(String yesNo) {
        return (yesNo.equals("YES"));
    }
}
