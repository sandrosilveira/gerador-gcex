/*
 *  Copyright (C) 2012 Sandro Madruga Silveira
 *  sandrosilveira@gmail.com
 *
 *  Este programa foi desenvolvido para o trabalho de conclusão de
 *  curso apresentado como requisito parcial à obtenção do grau de
 *  Bacharel em Ciência da Computação pela Universidade Feevale.
 *
 *  Este programa não pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autorização do autor.
 *
 */

package org.gcex.principal;

import org.gcex.nucleo.NucleoGeradorCodigo;

/**
 * Classe principal do Gerador de código, inicia e executa o núcleo de geração.
 *
 * @author Sandro Madruga Silveira
 * @version 1.00
 */
public class Gerador {

    /**
     * Método principal, passa a linha de comando para o núcleo de geração de código
     *
     * @param args argumentos passados pela linha de comando
     */
    public static void main(String[] args) {
       NucleoGeradorCodigo nucleo = new NucleoGeradorCodigo(args);
       nucleo.inicializa();
    }

}
