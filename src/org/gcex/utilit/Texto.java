/*
 *  Copyright (C) 2012 Sandro Madruga Silveira
 *  sandrosilveira@gmail.com
 *
 *  Este programa foi desenvolvido para o trabalho de conclusão de
 *  curso apresentado como requisito parcial à obtenção do grau de
 *  Bacharel em Ciência da Computação pela Universidade Feevale.
 *
 *  Este programa não pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autorização do autor.
 *
 */
package org.gcex.utilit;

import java.text.Normalizer;

/**
 * Classe utilitária de manipulação de texto
 *
 * @author Sandro
 */
public class Texto {

    /**
     * Converte uma String em um nome de atributo Java
     *
     * @param txt Nome de um campo, contendo um ou mais "_", "-", "." e " "
     * @return Nome de um atributo Java
     */
    public static String converteNomeAtributo(String txt) {
        // Converte o string em camelCase com a primeira letra em minúscula
        return converteCamelCase(txt, false);
    }

    /**
     * Converte uma String em um nome de classe Java
     *
     * @param txt Nome de um campo, contendo um ou mais "_", "-", "." e " "
     * @return Nome de um atributo Java com a primeira em maiúscula
     */
    public static String converteNomeClasse(String txt) {
        // Converte o string em camelCase com a primeira letra em maiúscula
        return converteCamelCase(txt, true);
    }

    /**
     * Retira a acentuação de uma String
     *
     * @param txt String contendo acentuação
     * @return String sem acentuação
     */
    public static String retiraAcentuacao(String txt) {
        String txtSemAcentuacao = txt;
        if (txt != null) {
            txtSemAcentuacao = Normalizer.normalize(txt, Normalizer.Form.NFD);
            txtSemAcentuacao = txtSemAcentuacao.replaceAll("[^\\p{ASCII}]", "");
            txtSemAcentuacao = txtSemAcentuacao.replaceAll("\\%", "Perc.");
            txtSemAcentuacao = txtSemAcentuacao.replaceAll("\\(", "");
            txtSemAcentuacao = txtSemAcentuacao.replaceAll("\\)", "");
        }
        return txtSemAcentuacao;
    }

    /**
     * Retira as aspas de uma String
     *
     * @param txt String contendo aspas
     * @return String sem aspas
     */
    public static String retiraAspas(String txt) {
        return txt.replaceAll("\"", "");
    }

    /**
     * Converte uma String em camelCase
     *
     * @param txt Texto a converter
     * @param mai Indica se deve converter a primeira letra em maiúscula
     * @return Texto em camelCase
     */
    private static String converteCamelCase(String txt, boolean mai) {
        StringBuilder sb = new StringBuilder();
        boolean cnvMaiuscula = mai;
        String txtMin = retiraAcentuacao(txt).toLowerCase();
        for (int i = 0; i < txtMin.length(); i++) {
            if (txtMin.charAt(i) == '_' || txtMin.charAt(i) == '-' || txtMin.charAt(i) == '/'
                    || txtMin.charAt(i) == '.' || txtMin.charAt(i) == ' ') {
                cnvMaiuscula = true;
            } else {
                if (cnvMaiuscula) {
                    char maiusculo = Character.toUpperCase(txtMin.charAt(i));
                    sb.append(maiusculo);
                    cnvMaiuscula = false;
                } else {
                    sb.append(txtMin.charAt(i));
                }
            }
        }
        return sb.toString();
    }
}
