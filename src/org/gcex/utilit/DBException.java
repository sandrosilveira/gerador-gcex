/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gcex.utilit;

/**
 * Classe para tratamento de exceções do banco de dados
 * @author Sandro
 */
public class DBException extends Exception {

    /**
     * Cria uma nova exceção passando uma mensagem e a causa
     * @param msg
     * @param cause
     */
    public DBException(String msg, Throwable cause) {
        super(msg, cause);
    }

    /**
     * Cria uma nova exceção passando apenas mensagem
     * @param msg
     */
    public DBException(String msg) {
        this(msg, null);
    }

}
