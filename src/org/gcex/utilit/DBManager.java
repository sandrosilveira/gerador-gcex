/*
 *  Copyright (C) 2012 Sandro Madruga Silveira
 *  sandrosilveira@gmail.com
 *
 *  Este programa foi desenvolvido para o trabalho de conclusão de
 *  curso apresentado como requisito parcial à obtenção do grau de
 *  Bacharel em Ciência da Computação pela Universidade Feevale.
 *
 *  Este programa não pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autorização do autor.
 *
 */
package org.gcex.utilit;

import java.sql.*;

/**
 * Classe para gerenciamento e manipulação do banco de dados
 *
 * @author Sandro
 */
public final class DBManager {

    private String driverJdbc;
    private String url;
    private String usuario;
    private String senha;
    private Connection conn;
    private Statement stmt;
    private PreparedStatement pStmt;
    private ResultSet rs;
    private int nroRegistros;

    /**
     * Construtor
     *
     * @param driverJdbc
     * @param url
     * @param usuario
     * @param senha
     */
    public DBManager(String driverJdbc, String url, String usuario, String senha) throws DBException {
        this.driverJdbc = driverJdbc;
        this.url = url;
        this.usuario = usuario;
        this.senha = senha;
        conecta();
    }

    /**
     * Conecta com o banco de dados
     *
     * @throws DBException
     */
    public void conecta() throws DBException {
        // Inicializa driver JDBC
        Driver driver;
        try {
            Class jdbcDriverClass = Class.forName(driverJdbc);
            driver = (Driver) jdbcDriverClass.newInstance();
            DriverManager.registerDriver(driver);
        } catch (Exception ex) {
            throw new DBException("Falha ao inicializar driver JDBC: " + driverJdbc, ex);
        }
        // Conecta ao banco de dados
        try {
            conn = DriverManager.getConnection(url, usuario, senha);
        } catch (SQLException ex) {
            throw new DBException("Falha ao conectar ao banco de dados: " + url, ex);
        }
        // Desliga Commit automático
        setAutoCommit(false);
    }

    /**
     * Configura autoCommit
     *
     * @param value false = autoCommit Off / true = autoCommit on
     * @throws DBException
     */
    public void setAutoCommit(boolean value) throws DBException {
        try {
            conn.setAutoCommit(value);
        } catch (SQLException ex) {
            throw new DBException("Falha ao setar autoCommit para " + value, ex);
        }
    }

    /**
     * Prepara um statement para posterior execução
     *
     * @param sql Comando sql a preparar
     * @return PreparedStatement para execução
     * @throws DBException
     */
    public PreparedStatement preparaStmt(String sql) throws DBException {
        try {
            // Prepara comando para inserção no banco de dados
            pStmt = conn.prepareStatement(sql);
            return pStmt;
        } catch (SQLException ex) {
            throw new DBException("Falha ao criar prepareStatement: " + sql, ex);
        }
    }

    /**
     * Executa uma instrução SQL
     *
     * @param sql comando SQL a executar imediatamente
     * @throws DBException
     */
    public void executaStmt(String sql) throws DBException {
        // Cria o statement
        try {
            stmt = conn.createStatement();
        } catch (SQLException ex) {
            throw new DBException("Falha ao criar statement: " + sql, ex);
        }
        // Executa o statement
        try {
            //regDeletados = stmt.executeUpdate(cmd);
            nroRegistros = stmt.executeUpdate(sql);
        } catch (SQLException ex) {
            throw new DBException("Falha executando statment: " + sql, ex);
        }
    }

    /**
     * Fecha a conexão com o banco de dados
     */
    public void desconecta() throws DBException {
        // Fecha statement aberto
        fechaPreparedStatment();
        // Fecha resultset aberto
        fechaResultset();
        // Fecha a conexão com o banco de dados
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException ex) {
            throw new DBException("Erro ao desconectar do banco de dados!", ex);
        }
    }

    /**
     * Fecha o PreparedStatment
     */
    public void fechaPreparedStatment() throws DBException {
        try {
            if (pStmt != null) {
                pStmt.close();
            }
        } catch (SQLException ex) {
            throw new DBException("Erro fechando prepared Statement!", ex);
        }
    }

    /**
     * Fecha o resultSet
     */
    public void fechaResultset() throws DBException {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException ex) {
            throw new DBException("Erro fechando resultSet!", ex);
        }
    }

    /**
     * Lista todas as tabelas do schema atual do banco de dados
     */
    public void listaTabelas() {
        try {
            DatabaseMetaData md;
            try {
                md = conn.getMetaData();
            } catch (SQLException ex) {
                ex.printStackTrace();
                return;
            }
            // Lista todas as tabelas de um determinado schema
            ResultSet rs = md.getTables(null, usuario.toUpperCase(), "%", null);
            while (rs.next()) {
                System.out.println(rs.getString(3));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Lista todas as colunas de uma tabela
     */
    public void listaColunas(String table) {
        try {
            DatabaseMetaData md;
            try {
                md = conn.getMetaData();
            } catch (SQLException ex) {
                ex.printStackTrace();
                return;
            }
            // Lista todas as tabelas de um determinado schema
            String format = "|%1$-16.16s|%2$-16.16s|%3$4d|%4$4d|%5$-6.6s|%6$-40.40s|\n";
            System.out.println("+----------------+----------------+----+----+------+----------------------------------------+");
            System.out.println("|Nome            |Tipo            | Tam| Dec|Null  |Comentário                              |");
            System.out.println("+----------------+----------------+----+----+------+----------------------------------------+");
            ResultSet rs = md.getColumns(null, null, table, null);
            while (rs.next()) {
                System.out.format(format, rs.getString("COLUMN_NAME"), rs.getString("TYPE_NAME"), rs.getInt("COLUMN_SIZE"),
                        rs.getInt("DECIMAL_DIGITS"), rs.getBoolean("NULLABLE"), rs.getString("REMARKS"));
            }
            System.out.println("+----------------+----------------+----+----+------+----------------------------------------+");
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Lista a primary key de uma tabela
     */
    public void listaPrimaryKey(String table) {
        try {
            DatabaseMetaData md;
            try {
                md = conn.getMetaData();
            } catch (SQLException ex) {
                ex.printStackTrace();
                return;
            }
            // Lista todas as tabelas de um determinado schema
            String format = "|%1$-16.16s|%2$-16.16s|%3$-16.16s|%4$-16.16s|%5$-16.16s|%6$-16.16s|\n";
            ResultSet rs = md.getPrimaryKeys(null, null, table);

            while (rs.next()) {
                System.out.format(format, rs.getString("TABLE_CAT"), rs.getString("TABLE_SCHEM"), rs.getString("TABLE_NAME"),
                        rs.getString("COLUMN_NAME"), rs.getBoolean("KEY_SEQ"), rs.getString("PK_NAME"));
            }
            rs.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Recupera uma descrição das colunas de chave primária referenciadas pelas colunas de chave estrangeira em uma
     * tabela
     *
     * @param table
     * @See http://msdn.microsoft.com/pt-br/library/ms378951.aspx
     */
    public void listaImportedKeys(String table) {
        try {
            DatabaseMetaData md;
            try {
                md = conn.getMetaData();
            } catch (SQLException ex) {
                ex.printStackTrace();
                return;
            }
            ResultSet rs = md.getImportedKeys(null, null, table);
            ResultSetMetaData rsmd = rs.getMetaData();
            // Display the result set data.
            int cols = rsmd.getColumnCount();
            while (rs.next()) {
                for (int i = 1; i <= cols; i++) {
                    System.out.println(rs.getString(i));
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void executeGetSchemas() {
        try {
            DatabaseMetaData dbmd = conn.getMetaData();
            ResultSet rs = dbmd.getSchemas();
            ResultSetMetaData rsmd = rs.getMetaData();

            // Display the result set data.
            int cols = rsmd.getColumnCount();
            while (rs.next()) {
                for (int i = 1; i <= cols; i++) {
                    System.out.println(rs.getString(i));
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Connection getConexao() {
        return conn;
    }
}
