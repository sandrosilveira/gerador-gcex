/*
 *  Copyright (C) 2012 Sandro Madruga Silveira
 *  sandrosilveira@gmail.com
 *
 *  Este programa foi desenvolvido para o trabalho de conclusão de
 *  curso apresentado como requisito parcial à obtenção do grau de
 *  Bacharel em Ciência da Computação pela Universidade Feevale.
 *
 *  Este programa não pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autorização do autor.
 *
 */
package org.gcex.utilit;

/**
 * Classe utilitária para conversão de tipos de dados
 *
 * @author Sandro
 */
public class CnvTipoDados {
    /**
     * Converte o tipo de dados do banco de dados MySQL para o tipo de dados Java
     *
     * @param tipoDadosBanco String com o tipo de dados do banco de dados
     * @return String com o tipo de dados Java
     * @see http://dev.mysql.com/doc/refman/4.1/en/connector-j-reference-type-conversions.html
     */
    public static String mysqlToJava(String tipoDadosBanco) {
        if (tipoDadosBanco.equals("CHAR")) {
            return "String";
        } else if (tipoDadosBanco.equals("VARCHAR")) {
            return "String";
        } else if (tipoDadosBanco.equals("VARCHAR2")) {
            return "String";
        } else if (tipoDadosBanco.equals("LONGVARCHAR")) {
            return "String";
        } else if (tipoDadosBanco.equals("NUMERIC")) {
            return "java.math.BigDecimal";
        } else if (tipoDadosBanco.equals("DECIMAL")) {
            return "java.math.BigDecimal";
        } else if (tipoDadosBanco.equals("BIT")) {
            return "boolean";
        } else if (tipoDadosBanco.equals("BOOL")) {
            return "boolean";
        } else if (tipoDadosBanco.equals("BOOLEAN")) {
            return "boolean";
        } else if (tipoDadosBanco.equals("TINYINT")) {
            return "int";
        } else if (tipoDadosBanco.equals("TINYINT UNSIGNED")) {
            return "int";
        } else if (tipoDadosBanco.equals("SMALLINT")) {
            return "short";
        } else if (tipoDadosBanco.equals("SMALLINT UNSIGNED")) {
            return "short";
        } else if (tipoDadosBanco.equals("INTEGER")) {
            return "int";
        } else if (tipoDadosBanco.equals("INT")) {
            return "int";
        } else if (tipoDadosBanco.equals("INT UNSIGNED")) {
            return "int";
        } else if (tipoDadosBanco.equals("BIGINT")) {
            return "long";
        } else if (tipoDadosBanco.equals("BIGINT UNSIGNED")) {
            return "long";
        } else if (tipoDadosBanco.equals("REAL")) {
            return "float";
        } else if (tipoDadosBanco.equals("FLOAT")) {
            return "float";
        } else if (tipoDadosBanco.equals("DOUBLE")) {
            return "double";
        } else if (tipoDadosBanco.equals("DATE")) {
            return "Date";
        } else if (tipoDadosBanco.equals("DATETIME")) {
            return "Date";
        } else if (tipoDadosBanco.equals("TIMESTAMP")) {
            return "java.sql.Timestamp";
        } else if (tipoDadosBanco.equals("TIME")) {
            return "java.sql.Time";
        } else if (tipoDadosBanco.equals("BINARY")) {
            return "byte[]";
        } else if (tipoDadosBanco.equals("VARBINARY")) {
            return "byte[]";
        } else if (tipoDadosBanco.equals("TINYBLOB")) {
            return "byte[]";
        } else if (tipoDadosBanco.equals("TINYTEXT")) {
            return "String";
        } else if (tipoDadosBanco.equals("BLOB")) {
            return "byte[]";
        } else if (tipoDadosBanco.equals("TEXT")) {
            return "String";
        }
        System.out.println("Erro: Não identifiquei " + tipoDadosBanco);
        return null;
    }

    /**
     * Converte o tipo de dados do banco de dados MySQL para o tipo de método set para JDBC
     *
     * @param tipoDadosBanco String com o tipo de dados do banco de dados MySQL
     * @return String com o nome do método set para JDBC
     */
    public static String mysqlToMetodoSetJdbc(String tipoDadosBanco) {
        if (tipoDadosBanco.equals("CHAR")) {
            return "setString";
        } else if (tipoDadosBanco.equals("VARCHAR")) {
            return "setString";
        } else if (tipoDadosBanco.equals("VARCHAR2")) {
            return "setString";
        } else if (tipoDadosBanco.equals("LONGVARCHAR")) {
            return "setString";
        } else if (tipoDadosBanco.equals("NUMERIC")) {
            return "setBigDecimal";
        } else if (tipoDadosBanco.equals("DECIMAL")) {
            return "setBigDecimal";
        } else if (tipoDadosBanco.equals("BIT")) {
            return "setBoolean";
        } else if (tipoDadosBanco.equals("BOOL")) {
            return "setBoolean";
        } else if (tipoDadosBanco.equals("BOOLEAN")) {
            return "setBoolean";
        } else if (tipoDadosBanco.equals("TINYINT")) {
            return "setInt";
        } else if (tipoDadosBanco.equals("TINYINT UNSIGNED")) {
            return "setInt";
        } else if (tipoDadosBanco.equals("SMALLINT")) {
            return "setShort";
        } else if (tipoDadosBanco.equals("SMALLINT UNSIGNED")) {
            return "setShort";
        } else if (tipoDadosBanco.equals("INTEGER")) {
            return "setInt";
        } else if (tipoDadosBanco.equals("INT")) {
            return "setInt";
        } else if (tipoDadosBanco.equals("INT UNSIGNED")) {
            return "setInt";
        } else if (tipoDadosBanco.equals("BIGINT")) {
            return "setLong";
        } else if (tipoDadosBanco.equals("BIGINT UNSIGNED")) {
            return "setLong";
        } else if (tipoDadosBanco.equals("REAL")) {
            return "setFloat";
        } else if (tipoDadosBanco.equals("FLOAT")) {
            return "setFloat";
        } else if (tipoDadosBanco.equals("DOUBLE")) {
            return "setDouble";
        } else if (tipoDadosBanco.equals("DATE")) {
            return "setDate";
        } else if (tipoDadosBanco.equals("DATETIME")) {
            return "setDate";
        } else if (tipoDadosBanco.equals("TIMESTAMP")) {
            return "setLong";
        } else if (tipoDadosBanco.equals("TIME")) {
            return "setDate";
        } else if (tipoDadosBanco.equals("BINARY")) {
            return "setBytes";
        } else if (tipoDadosBanco.equals("VARBINARY")) {
            return "setString";
        } else if (tipoDadosBanco.equals("TINYBLOB")) {
            return "setString";
        } else if (tipoDadosBanco.equals("TINYTEXT")) {
            return "setString";
        } else if (tipoDadosBanco.equals("BLOB")) {
            return "setString";
        } else if (tipoDadosBanco.equals("TEXT")) {
            return "setString";
        }
        System.out.println("Erro (2): Não identifiquei " + tipoDadosBanco);
        return null;
    }

    /**
     * Converte o tipo de dados do banco de dados MySQL para o tipo de objeto Java compatível
     *
     * @param tipoDadosBanco String com o tipo de dados do banco de dados
     * @return String com o nome do tipo de objeto Java compatível
     */
    public static String mysqlToObjetoCompativel(String tipoDadosBanco) {
        if (tipoDadosBanco.equals("CHAR")) {
            return "String";
        } else if (tipoDadosBanco.equals("VARCHAR")) {
            return "String";
        } else if (tipoDadosBanco.equals("VARCHAR2")) {
            return "String";
        } else if (tipoDadosBanco.equals("LONGVARCHAR")) {
            return "String";
        } else if (tipoDadosBanco.equals("NUMERIC")) {
            return "BigDecimal";
        } else if (tipoDadosBanco.equals("DECIMAL")) {
            return "BigDecimal";
        } else if (tipoDadosBanco.equals("BIT")) {
            return "Boolean";
        } else if (tipoDadosBanco.equals("BOOL")) {
            return "Boolean";
        } else if (tipoDadosBanco.equals("BOOLEAN")) {
            return "Boolean";
        } else if (tipoDadosBanco.equals("TINYINT")) {
            return "Integer";
        } else if (tipoDadosBanco.equals("TINYINT UNSIGNED")) {
            return "Integer";
        } else if (tipoDadosBanco.equals("SMALLINT")) {
            return "Integer";
        } else if (tipoDadosBanco.equals("SMALLINT UNSIGNED")) {
            return "Short";
        } else if (tipoDadosBanco.equals("INTEGER")) {
            return "Integer";
        } else if (tipoDadosBanco.equals("INT")) {
            return "Integer";
        } else if (tipoDadosBanco.equals("INT UNSIGNED")) {
            return "Integer";
        } else if (tipoDadosBanco.equals("BIGINT")) {
            return "Long";
        } else if (tipoDadosBanco.equals("BIGINT UNSIGNED")) {
            return "Long";
        } else if (tipoDadosBanco.equals("REAL")) {
            return "Float";
        } else if (tipoDadosBanco.equals("FLOAT")) {
            return "Float";
        } else if (tipoDadosBanco.equals("DOUBLE")) {
            return "Double";
        } else if (tipoDadosBanco.equals("DATE")) {
            return "Date";
        } else if (tipoDadosBanco.equals("DATETIME")) {
            return "Date";
        } else if (tipoDadosBanco.equals("TIMESTAMP")) {
            return "Long";
        } else if (tipoDadosBanco.equals("TIME")) {
            return "Date";
        } else if (tipoDadosBanco.equals("BINARY")) {
            return "String";
        } else if (tipoDadosBanco.equals("VARBINARY")) {
            return "String";
        } else if (tipoDadosBanco.equals("TINYBLOB")) {
            return "String";
        } else if (tipoDadosBanco.equals("TINYTEXT")) {
            return "String";
        } else if (tipoDadosBanco.equals("BLOB")) {
            return "String";
        } else if (tipoDadosBanco.equals("TEXT")) {
            return "String";
        }
        System.out.println("Erro (3): Não identifiquei " + tipoDadosBanco);
        return null;
    }
}
