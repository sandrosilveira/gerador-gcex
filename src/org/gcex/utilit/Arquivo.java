/**
 *  Copyright (C) 2012 Sandro Madruga Silveira
 *  sandrosilveira@gmail.com
 *
 *  Este programa foi desenvolvido para o trabalho de conclusão de
 *  curso apresentado como requisito parcial à obtenção do grau de
 *  Bacharel em Ciência da Computação pela Universidade Feevale.
 *
 *  Este programa não pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autorização do autor.
 */
package org.gcex.utilit;

import java.io.File;

/**
 * Classe utilitária de manipulação de arquivos e diretórios.
 *
 * @author Sandro
 */
public class Arquivo {

    /**
     * Cria estrutura de diretório de um arquivo se a mesma ainda não existir
     *
     * @param arquivo Diretório ou diretório e arquivo a testar e criar
     */
    public static void testaCriaDiretoriosArquivo(String arquivo) {
        String estrutura = getDiretorio(arquivo);
        if (!arquivoExiste(estrutura)) {
            criaDiretorios(estrutura);
        }
    }

    /**
     * Retorna o diretório do arquivo informado
     *
     * @param arquivo Diretório e nome de arquivo
     * @return Nome do diretório
     */
    public static String getDiretorio(String arquivo) {
        // Referência para o arquivo
        File arq = new File(arquivo);
        // Nome do diretório
        return arq.getParent();
    }

    /**
     * Retorna verdadeiro se arquivo ou diretório informados existirem
     *
     * @param arquivo Arquivo ou diretório para teste de existência
     * @return verdadeiro se arquivo ou diretório informados existirem
     */
    public static boolean arquivoExiste(String arquivo) {
        if (arquivo == null || arquivo.isEmpty()) {
            return false;
        }
        File file = new File(arquivo);
        return file.exists();
    }

    /**
     * Cria estrutura de diretórios
     *
     * @param estrutura Drive e nomes de diretórios separados por barra
     */
    public static void criaDiretorios(String estrutura) {
        File dir = new File(estrutura);
        dir.mkdirs();
    }

}
