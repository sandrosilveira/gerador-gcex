/**
 * Copyright (C) 2012 Sandro Madruga Silveira
 * sandrosilveira@gmail.com
 *
 * Este programa foi desenvolvido para o trabalho de conclusão de
 * curso apresentado como requisito parcial à obtenção do grau de
 * Bacharel em Ciência da Computação pela Universidade Feevale.
 *
 * Este programa não pode ser utilizado, distribuido ou modificado
 * ao todo ou em parte sem a expressa autorização do autor.
 */
package org.gcex.nucleo;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.util.Map;
import org.apache.commons.io.FilenameUtils;
import org.gcex.especificacoes.*;
import org.gcex.interfaceUsuario.OpcoesExecucao;
import org.gcex.interfaceUsuario.Wizard;
import org.gcex.utilit.Arquivo;

/**
 * Núcleo de geração de código. É a parte principal da ferramenta de geração de código,
 * tem por objetivo integrar todos os componentes da aplicação, promovendo a sua execuçáo.
 *
 * @author Sandro Madruga Silveira
 */
public class NucleoGeradorCodigo {

    /** Opções de execução passadas ao programa */
    private OpcoesExecucao opcoes;
    /** Configurações do template engine carregado */
    private Configuration configuration;
    /** Especificações de origem carregados para executar substituições nos templates */
    private Map especificacao;
    /** Referência ao template que servirá de matriz para dar origem ao código fonte */
    private Template template;
    /** Opções passadas pela linha de comando */
    private final String[] linhaComando;

    /**
     * Construtor do núcleo de geração de código
     */
    public NucleoGeradorCodigo() {
        linhaComando = null;
    }

    /**
     * Construtor do núcleo de geração de código
     *
     * @param args Array com as opções passadas pela linha de comando
     */
    public NucleoGeradorCodigo(String[] args) {
        linhaComando = args;
    }

    /**
     * Método principal, inicializa e comanda as etapas de geração de código
     */
    public final void inicializa() {
        interpretaLinhaComando();
        if (opcoes.isModoWizard()) {
            executaWizard();
            return;
        }
        if (opcoes.getArquivoProjeto() == null) {
            executa();
        }
    }

    /**
     * Executa interface gráfica no modo Wizard (assistente para geração de código)
     */
    private void executaWizard() {
        final NucleoGeradorCodigo nucleo = this;
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Wizard wizard = new Wizard(opcoes, nucleo);
                wizard.setVisible(true);
            }
        });
    }

    /**
     * Executa as etapas de geração de código
     */
    public final void executa() {
        // Se não informou nenhum arquivo de saída apenas carrega a especificação
        if (opcoes.getSaida() == null) {
            carregaEspecificacao();
            return;
        }
        carregaTemplateEngine();
        carregaEspecificacao();
        carregaTemplate();
        geraCodigoFonte();
    }

    /**
     * Retorna um Map com a especificação de entrada carregada
     *
     * @param entrada
     * @param nome
     * @return Map<String, Object>
     */
    public Map<String, Object> getEspecificacao(String entrada, String nome) {
        opcoes = new OpcoesExecucao();
        opcoes.setEntrada(entrada);
        opcoes.setNome(nome);
        opcoes.setExibirAndamento(false);
        carregaEspecificacao();
        return especificacao;
    }

    /**
     * Interpreta a linha de comando passada à aplicação
     */
    public void interpretaLinhaComando() {
        opcoes = new OpcoesExecucao(linhaComando);
    }

    /**
     * Carrega o componente que irá tratar o template bem como suas configurações
     */
    private void carregaTemplateEngine() {
        System.out.println("Carregando template engine Freemarker...");
        configuration = new Configuration();
        configuration.setObjectWrapper(new DefaultObjectWrapper());
    }

    /**
     * Carrega o módulo responsável por fazer a análise da estrutura de entrada e executa a análise dos dados
     * de entrada
     */
    private void carregaEspecificacao() {
        if (opcoes.isExibirAndamento()) {
            System.out.println("Carregando especificação (" + opcoes.getEntrada() + " " + opcoes.getNome() + ")...");
        }
        // TODO: Identificar a especificação pelo nome e carregar a classe byName
        if (opcoes.getEntrada().equalsIgnoreCase("ArquivoINI")) {
            EspecificacaoArquivoIni esp = new EspecificacaoArquivoIni(opcoes.getNome());
            esp.setMapAtributosGeracao(opcoes.getAtributosGeracao());
            especificacao = esp.getEspecificacao();
            // Mostra a especificação carregada
            if (opcoes.isExibirEstruturaEspecificacao()) {
                esp.mostraEspecificacao();
            }
        }
        if (opcoes.getEntrada().equalsIgnoreCase("ArgoUML")) {
            EspecificacaoArgoUML esp = new EspecificacaoArgoUML(opcoes.getNome());
            esp.setMapAtributosGeracao(opcoes.getAtributosGeracao());
            especificacao = esp.getEspecificacao();
            // Mostra a especificação carregada
            if (opcoes.isExibirEstruturaEspecificacao()) {
                esp.mostraEspecificacao();
            }
        }
        if (opcoes.getEntrada().equalsIgnoreCase("XML")) {
            EspecificacaoXML esp = new EspecificacaoXML(opcoes.getNome());
            esp.setMapAtributosGeracao(opcoes.getAtributosGeracao());
            especificacao = esp.getEspecificacao();
            // Mostra a especificação carregada
            if (opcoes.isExibirEstruturaEspecificacao()) {
                esp.mostraEspecificacao();
            }
        }
        if (opcoes.getEntrada().equalsIgnoreCase("CSV")) {
            EspecificacaoCSV esp = new EspecificacaoCSV(opcoes.getNome());
            esp.setMapAtributosGeracao(opcoes.getAtributosGeracao());
            especificacao = esp.getEspecificacao();
            // Mostra a especificação carregada
            if (opcoes.isExibirEstruturaEspecificacao()) {
                esp.mostraEspecificacao();
            }
        }
        if (opcoes.getEntrada().equalsIgnoreCase("BancoDados")) {
            EspecificacaoBancoDados esp = new EspecificacaoBancoDados(opcoes.getNome());
            esp.setMapAtributosGeracao(opcoes.getAtributosGeracao());
            especificacao = esp.getEspecificacao();
            // Mostra a especificação carregada
            if (opcoes.isExibirEstruturaEspecificacao()) {
                esp.mostraEspecificacao();
            }
        }
        if (opcoes.getEntrada().equalsIgnoreCase("DicionarioSiger")) {
            EspecificacaoSiger esp = new EspecificacaoSiger(opcoes.getNome());
            esp.setMapAtributosGeracao(opcoes.getAtributosGeracao());
            especificacao = esp.getEspecificacao();
            // Mostra a especificação carregada
            if (opcoes.isExibirEstruturaEspecificacao()) {
                esp.mostraEspecificacao();
            }
        }
        if (opcoes.getEntrada().equalsIgnoreCase("RechClass")) {
            EspecificacaoRechClass esp = new EspecificacaoRechClass(opcoes.getNome());
            esp.setMapAtributosGeracao(opcoes.getAtributosGeracao());
            especificacao = esp.getEspecificacao();
            // Mostra a especificação carregada
            if (opcoes.isExibirEstruturaEspecificacao()) {
                esp.mostraEspecificacao();
            }
        }
    }

    /**
     * Carrega o template que servirá como modelo do código gerado
     */
    private void carregaTemplate() {
        System.out.println("Carregando template (" + opcoes.getTemplate() + ")...");
        // TODO: Implementar classes de tratamento de exceção
        try {
            // TODO: Configurar o enconding do template e da geração
            if (FilenameUtils.getExtension(opcoes.getTemplate()).equalsIgnoreCase("ftl")) {
                template = configuration.getTemplate("/templates/" + opcoes.getTemplate(), opcoes.getCodificacao());
            } else {
                template = configuration.getTemplate(opcoes.getTemplate(), opcoes.getCodificacao());
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            System.out.println("Erro carregando template!!!" + opcoes.getTemplate() + "!!!");
            System.exit(1);
        }
    }

    /**
     * Faz a geração de código fonte através de merge, ou seja, mescla dados de entrada com o modelo de
     * código, criando em um buffer de memória o código fonte de saída.
     */
    private void geraCodigoFonte() {
        System.out.println("Executando geração de código...");
        StringWriter writer = new StringWriter();
        // TODO: Implementar classes de tratamento de exceção
        try {
            template.process(especificacao, writer);
        } catch (TemplateException ex) {
            System.out.println("Erro no template ao gerar código fonte!!!");
            System.out.println("");
            System.exit(1);
        } catch (IOException ex) {
            System.out.println("Erro de entrada e saída gerando código fonte!!!");
            System.out.println("");
            System.exit(1);
        }
        gravaCodigoFonte(writer.toString());
        System.out.println("Arquivo " + opcoes.getSaida() + " criado com sucesso!");
        System.out.println("");
    }

    /**
     * Grava o arquivo de código fonte gerado
     *
     * @param outText Nome do arquivo texto a gravar o código fonte gerado
     */
    private void gravaCodigoFonte(String outText) {
        System.out.println("Criando código fonte...");
        Arquivo.testaCriaDiretoriosArquivo(opcoes.getSaida());
        BufferedWriter bufferedWriter = null;
        try {
            //bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(opcoes.getSaida()), "ISO-8859-1"));
            bufferedWriter =
                    new BufferedWriter(new OutputStreamWriter(new FileOutputStream(opcoes.getSaida()), opcoes.getCodificacao()));
            bufferedWriter.write(outText);
            // Fecha o bufffer de saída
            bufferedWriter.close();
        } catch (IOException ex) {
            System.out.println("Erro ao gravar código fonte: " + opcoes.getSaida());
            System.out.println("");
            System.exit(1);
        } finally {
            try {
                bufferedWriter.close();
            } catch (IOException ex) {
            }
        }
    }

}
