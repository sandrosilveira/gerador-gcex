/*
 *  Copyright (C) 2012 Sandro Madruga Silveira
 *  sandrosilveira@gmail.com
 *
 *  Este programa foi desenvolvido para o trabalho de conclusão de
 *  curso apresentado como requisito parcial à obtenção do grau de
 *  Bacharel em Ciência da Computação pela Universidade Feevale.
 *
 *  Este programa não pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autorização do autor.
 *
 */
package org.gcex.interfaceUsuario;

/**
 * Classe que carrega e controla as opções de execução do gerador de código
 *
 * @author Sandro Silveira
 */
public final class OpcoesExecucao {

    /**
     * Linha de comando passada para o programa
     */
    private String linhaComando;
    /**
     * Nome da especificação de entrada que deve ser utilizada. Seu nome deve ser o mesmo da classe a ser carregada e
     * executada para interpretação dos dados de origem.
     */
    private String entrada;
    /**
     * Nome do elemento existente na origem será utilizado para geração de código.
     */
    private String nome;
    /**
     * Caminho e nome do template que deverá ser utilizado para geração do código fonte.
     */
    private String template;
    /**
     * Caminho e nome do arquivo de saída o qual deverá se usado para gravar o código fonte gerado.
     */
    private String saida;
    /**
     * Arquivo de projeto
     */
    private String arquivoProjeto;
    /**
     * Atributos para geração de código, pares chave:valor separados por ";"
     */
    private String atributosGeracao;
    /**
     *  Codificação do template e do fonte gerado
     */
    private String codificacao = "UTF-8";
    /**
     * Indica se deve executar no modo wizard
     */
    private boolean modoWizard;
    /**
     * Indica se deve exibir as opções do programa
     */
    private boolean exibirOpcoes;
    /**
     * Indica se deve exibir andamento
     */
    private boolean exibirAndamento = true;
    /**
     * Indica se deve exibir a estrutura da especificação carregada
     */
    private boolean exibirEstruturaEspecificacao;

    /**
     * Construtor padrão da classe de opções de execução
     */
    public OpcoesExecucao() {
    }

    /**
     * Construtor da classe de opções de execução
     *
     * @param args Opções passadas pela linha de comando
     */
    public OpcoesExecucao(String[] args) {
        // Se foi executado sem nenhuma opção
        if (args.length == 0) {
            mostraSintaxeCorreta();
            System.exit(1);
        }
        // Varre as opções passadas na linha de comando
        linhaComando = "";
        for (int i = 0; i < args.length; i++) {
            // Avalia cada uma das opções passadas na linha de comando
            avaliaOpcao(args[i]);
            linhaComando = linhaComando + args[i];
            // Se não for a última opção, acrescenta espaço após a opção
            if (i != (args.length - 1)) {
                linhaComando = linhaComando + " ";
            }
        }
        // Exibe as opções selecionadas
        if (isExibirOpcoes()) {
            System.out.println(this.toString());
        }
        // Se não for modo wizard
        if (!isModoWizard()) {
            // Valida opçöes obrigatórias
            if (getEntrada() == null) {
                System.out.println("Erro: especificação de entrada não informada!");
                mostraSintaxeCorreta();
                System.exit(1);
            }
            if (getNome() == null) {
                System.out.println("Erro: nome do elemento não informado!");
                mostraSintaxeCorreta();
                System.exit(1);
            }
            // Se deve exibir estrutura da especificação
            if (isExibirEstruturaEspecificacao()) {
                // Se não informou template nem saída não valida
                if (getTemplate() == null && getSaida() == null) {
                    return;
                }
            }
            // Valida template não informado
            if (getTemplate() == null) {
                System.out.println("Erro: template não informado!");
                mostraSintaxeCorreta();
                System.exit(1);
            }
            // Valida arquivo de saída não informado
            if (getSaida() == null) {
                System.out.println("Erro: caminho e nome de arquivo não informado!");
                mostraSintaxeCorreta();
                System.exit(1);
            }
        }
    }

    /**
     * Mostra a sintaxe correta para execução do gerador de código
     */

    /*
    * Todo: Criar uma opção de filtro -f="elemento.elemento.elemento equals/contains 'tal coisa'"
    */


    private void mostraSintaxeCorreta() {
        System.out.println("Sintaxe: java -jar Gerador <Opções>\n" +
                "Opções:\n" +
                "-e=   Especificação de entrada a ser utilizada\n" +
                "-n=   Nome do elemento a ser utilizado\n" +
                "-t=   Template para geração de código\n" +
                "-s=   Caminho e nome do arquivo de saída com o código gerado\n" +
                "-p=   Nome do arquivo de projeto\n" +
                "-a=   Atributos para geração de código, pares chave:valor separados por \";\"\n" +
                "-c=   Codificação do template e do fonte gerado, ex: -c=UTF-8 (default), -c=ISO8859-1, etc\n" +
                "-wiz  Exibe um wizard (assistente) para informar as opções de execução\n" +
                "-eop  Exibe as opções de execução do gerador de código\n" +
                "-ean  Exibe andamento de execução do programa (default true)\n" +
                "-ees  Exibe a estrutura da expecifição carregada\n");
    }

    /**
     * Avalia cada opção passada pela linha de comando
     *
     * @param opcao Opção a avaliar
     */
    private void avaliaOpcao(String opcao) {
        if (opcao.toLowerCase().startsWith("-e=")) {
            if (opcao.length() > 3) {
                setEntrada(opcao.substring(3));
            }
            return;
        }
        if (opcao.toLowerCase().startsWith("-n=")) {
            if (opcao.length() > 3) {
                setNome(opcao.substring(3));
            }
            return;
        }
        if (opcao.toLowerCase().startsWith("-t=")) {
            if (opcao.length() > 3) {
                setTemplate(opcao.substring(3));
            }
            return;
        }
        if (opcao.toLowerCase().startsWith("-s=")) {
            if (opcao.length() > 3) {
                setSaida(opcao.substring(3));
            }
            return;
        }
        if (opcao.toLowerCase().startsWith("-p=")) {
            if (opcao.length() > 3) {
                setArquivoProjeto(opcao.substring(3));
            }
            return;
        }
        if (opcao.toLowerCase().startsWith("-a=")) {
            if (opcao.length() > 3) {
                setAtributosGeracao(opcao.substring(3));
            }
            return;
        }
        if (opcao.toLowerCase().startsWith("-c=")) {
            if (opcao.length() > 3) {
                setCodificacao(opcao.substring(3));
            }
            return;
        }
        if (opcao.equalsIgnoreCase("-wiz")) {
            setModoWizard(true);
            return;
        }
        if (opcao.equalsIgnoreCase("-eop")) {
            setExibirOpcoes(true);
            return;
        }
        if (opcao.equalsIgnoreCase("-ean")) {
            setExibirAndamento(true);
            return;
        }
        if (opcao.equalsIgnoreCase("-ees")) {
            setExibirEstruturaEspecificacao(true);
            return;
        }
        System.out.println("Erro: opção inválida: " + opcao + "!!");
        System.exit(1);
    }

    /**
     *
     * @return
     */
    public String getEntrada() {
        return entrada;
    }

    /**
     *
     * @param entrada
     */
    public void setEntrada(String entrada) {
        this.entrada = entrada;
    }

    /**
     *
     * @return
     */
    public String getNome() {
        return nome;
    }

    /**
     *
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     *
     * @return
     */
    public String getSaida() {
        return saida;
    }

    /**
     *
     * @param saida
     */
    public void setSaida(String saida) {
        this.saida = saida;
    }

    /**
     *
     * @return
     */
    public String getArquivoProjeto() {
        return arquivoProjeto;
    }

    /**
     *
     * @param arquivoProjeto
     */
    public void setArquivoProjeto(String arquivoProjeto) {
        this.arquivoProjeto = arquivoProjeto;
    }

    /**
     *
     * @return
     */
    public String getAtributosGeracao() {
        return atributosGeracao;
    }

    /**
     *
     * @param atributosGeracao
     */
    public void setAtributosGeracao(String atributosGeracao) {
        this.atributosGeracao = atributosGeracao;
    }

    /**
     *
     * @return
     */
    public String getCodificacao() {
        return codificacao;
    }

    /**
     *
     * @param codificacao
     */
    public void setCodificacao(String codificacao) {
        this.codificacao = codificacao;
    }

    /**
     *
     * @return
     */
    public String getTemplate() {
        return template;
    }

    /**
     *
     * @param template
     */
    public void setTemplate(String template) {
        this.template = template;
    }

    /**
     * Retorna se deve executar no modo wizard
     *
     * @return
     */
    public boolean isModoWizard() {
        return modoWizard;
    }

    /**
     * Indica se deve ou não executar no modo wizard
     *
     * @param modoWizard
     */
    public void setModoWizard(boolean modoWizard) {
        this.modoWizard = modoWizard;
    }

    /**
     * Retorna se deve exibir as opções do programa
     *
     * @return
     */
    public boolean isExibirOpcoes() {
        return exibirOpcoes;
    }

    /**
     * Indica se deve ou não exibir opções do programa
     *
     * @param exibirOpcoes
     */
    public void setExibirOpcoes(boolean exibirOpcoes) {
        this.exibirOpcoes = exibirOpcoes;
    }

    /**
     * Retorna se deve exibir a estrutura da especificação carregada
     *
     * @return
     */
    public boolean isExibirEstruturaEspecificacao() {
        return exibirEstruturaEspecificacao;
    }

    /**
     * Indica se deve ou não exibir mensagens de andamento de execução
     *
     * @param exibirAndamento
     */
    public void setExibirAndamento(boolean exibirAndamento) {
        this.exibirAndamento = exibirAndamento;
    }

    /**
     * Retorna se deve exibir mensagens de andamento de execução
     *
     * @return boolean
     */
    public boolean isExibirAndamento() {
        return exibirAndamento;
    }

    /**
     * Indica se deve ou não exibir a estrutura da especificação carregada
     *
     * @param exibirEstruturaEspecificacao
     */
    public void setExibirEstruturaEspecificacao(boolean exibirEstruturaEspecificacao) {
        this.exibirEstruturaEspecificacao = exibirEstruturaEspecificacao;
    }

    /**
     * Carrega opções de um arquivo de projeto
     */
    public void carregaProjeto() {
    }

    @Override
    public String toString() {
        return "OpcoesExecucao{" + "linhaComando=" + linhaComando + ", entrada=" + entrada + ", nome=" + nome +
                ", template=" + template + ", saida=" + saida + ", atributosGeracao=" +  atributosGeracao +
                ", codificacao=" + codificacao + ", exibirOpcoes=" + exibirOpcoes +
                ", exibirAndamento=" + exibirAndamento +
                ", exibirEstruturaEspecificacao=" + exibirEstruturaEspecificacao + '}';
    }
}
