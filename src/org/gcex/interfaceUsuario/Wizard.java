/*
 *  Copyright (C) 2012 Sandro Madruga Silveira
 *  sandrosilveira@gmail.com
 *
 *  Este programa foi desenvolvido para o trabalho de conclusão de
 *  curso apresentado como requisito parcial à obtenção do grau de
 *  Bacharel em Ciência da Computação pela Universidade Feevale.
 *
 *  Este programa não pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autorização do autor.
 *
 */
package org.gcex.interfaceUsuario;

import org.gcex.especificacoes.EspecificacaoBancoDados;
import org.gcex.especificacoes.EspecificacaoSiger;
import org.gcex.especificacoes.EspecificacaoArgoUML;
import java.awt.Color;
import java.awt.Cursor;
import java.util.ArrayList;
import javax.swing.UIManager;
import org.gcex.nucleo.NucleoGeradorCodigo;

/**
 * Classe responsável pela interação via Wizard para configurar a geração de código
 *
 * @author Sandro Madruga Silveira
 */
public class Wizard extends javax.swing.JFrame {

    private OpcoesExecucao opcoes;
    private NucleoGeradorCodigo nucleo;
    private int etapa = 1;
    private PanelEspecificacao painelEspecificacao;
    private PanelElemento painelElemento;
    private PanelTemplate painelTemplate;
    private PanelSaida painelSaida;
    private PanelGeracao painelGeracao;
    private ArrayList<String> listaElementos;

    /**
     * Construtor principal da classe Wizard
     *
     * @param opcoes Opções do gerador de código
     * @param nucleo Referência para o núcleo de geração de código iniciado
     */
    public Wizard(OpcoesExecucao opcoes, NucleoGeradorCodigo nucleo) {
        this.opcoes = opcoes;
        this.nucleo = nucleo;
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Exception e) {
            e.printStackTrace();
        }
        initComponents();
        avaliaEtapa();
        setLocationRelativeTo(null);
    }

    /**
     * Avalia a etapa ativa e exibe o painel de interação com o usuário
     *
     */
    private void avaliaEtapa() {
        jLabelEspecificacao.setForeground(Color.black);
        jLabelElemento.setForeground(Color.black);
        jLabelTemplate.setForeground(Color.black);
        jLabelSaida.setForeground(Color.black);
        jLabelGerarCodigoFonte.setForeground(Color.black);
        jButtonFinalizar.setEnabled(false);
        switch (etapa) {
            case 1:
                if (painelEspecificacao == null) {
                    painelEspecificacao = new PanelEspecificacao();
                    //painelEspecificacao.ocultaPainelConexao();
                }
                jScrollPaneDinamico.setViewportView(painelEspecificacao);
                jLabelEspecificacao.setForeground(Color.blue);
                jButtonAnterior.setEnabled(false);
                jButtonProximo.setEnabled(true);
                break;
            case 2:
                Cursor hourglassCursor = new Cursor(Cursor.WAIT_CURSOR);
                setCursor(hourglassCursor);
                if (painelElemento == null) {
                    painelElemento = new PanelElemento();
                }
                String nomeEspec = painelEspecificacao.getEspecificacao();
                listaElementos = null;
                if (nomeEspec.equals("Banco de dados")) {
                    opcoes.setEntrada("BancoDados");
                    EspecificacaoBancoDados esp = new EspecificacaoBancoDados("");
                    listaElementos = esp.getListaDisponiveis("");
                }
                if (nomeEspec.equals("Dicionário SIGER")) {
                    opcoes.setEntrada("DicionarioSIGER");
                    EspecificacaoSiger esp = new EspecificacaoSiger("");
                    listaElementos = esp.getListaDisponiveis("");
                }
                if (nomeEspec.equals("XML") || nomeEspec.equals("ArgoUML")) {
                    opcoes.setEntrada("XML");
                    EspecificacaoArgoUML esp = new EspecificacaoArgoUML("");
                    listaElementos = esp.getListaDisponiveis("");
                }
                painelElemento.adicionaOpcoesCombo(listaElementos);
                jScrollPaneDinamico.setViewportView(painelElemento);
                jLabelElemento.setForeground(Color.blue);
                jButtonAnterior.setEnabled(true);
                jButtonProximo.setEnabled(true);
                Cursor normalCursor = new Cursor(Cursor.DEFAULT_CURSOR);
                setCursor(normalCursor);
                break;
            case 3:
                if (painelTemplate == null) {
                    painelTemplate = new PanelTemplate();
                }
                opcoes.setNome(painelElemento.getElemento());
                jScrollPaneDinamico.setViewportView(painelTemplate);
                jLabelTemplate.setForeground(Color.blue);
                jButtonAnterior.setEnabled(true);
                jButtonProximo.setEnabled(true);
                break;
            case 4:
                if (painelSaida == null) {
                    painelSaida = new PanelSaida();
                }
                opcoes.setTemplate(painelTemplate.getTemplate());
                jScrollPaneDinamico.setViewportView(painelSaida);
                jLabelSaida.setForeground(Color.blue);
                jButtonAnterior.setEnabled(true);
                jButtonProximo.setEnabled(true);
                break;
            case 5:
                if (painelGeracao == null) {
                    painelGeracao = new PanelGeracao();
                }
                opcoes.setSaida(painelSaida.getSaida());
                jScrollPaneDinamico.setViewportView(painelGeracao);
                jLabelGerarCodigoFonte.setForeground(Color.blue);
                jButtonAnterior.setEnabled(true);
                jButtonProximo.setEnabled(false);
                jButtonFinalizar.setEnabled(true);
                break;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this
     * code. The content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelEtapas = new javax.swing.JPanel();
        jLabelEtapa = new javax.swing.JLabel();
        jPanelSeparador = new javax.swing.JPanel();
        jLabelEspecificacao = new javax.swing.JLabel();
        jLabelElemento = new javax.swing.JLabel();
        jLabelTemplate = new javax.swing.JLabel();
        jLabelSaida = new javax.swing.JLabel();
        jLabelGerarCodigoFonte = new javax.swing.JLabel();
        jLabelImagem = new javax.swing.JLabel();
        jPanelDinamico = new javax.swing.JPanel();
        jScrollPaneDinamico = new javax.swing.JScrollPane();
        jPanelInferior = new javax.swing.JPanel();
        jButtonSair = new javax.swing.JButton();
        jButtonAnterior = new javax.swing.JButton();
        jButtonProximo = new javax.swing.JButton();
        jButtonFinalizar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Gerador de código fonte vs 1.0");

        jPanelEtapas.setBackground(new java.awt.Color(251, 252, 254));

        jLabelEtapa.setText("Etapas");

        jPanelSeparador.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jPanelSeparadorLayout = new javax.swing.GroupLayout(jPanelSeparador);
        jPanelSeparador.setLayout(jPanelSeparadorLayout);
        jPanelSeparadorLayout.setHorizontalGroup(
            jPanelSeparadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 185, Short.MAX_VALUE)
        );
        jPanelSeparadorLayout.setVerticalGroup(
            jPanelSeparadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jLabelEspecificacao.setText("1. Definir especificação de entrada");

        jLabelElemento.setText("2. Definir elemento de origem");

        jLabelTemplate.setText("3. Definir Template");

        jLabelSaida.setText("4. Definir arquivo de saída");

        jLabelGerarCodigoFonte.setText("5. Gerar código fonte");

        jLabelImagem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/generate_mini.jpg"))); // NOI18N

        javax.swing.GroupLayout jPanelEtapasLayout = new javax.swing.GroupLayout(jPanelEtapas);
        jPanelEtapas.setLayout(jPanelEtapasLayout);
        jPanelEtapasLayout.setHorizontalGroup(
            jPanelEtapasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelEtapasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelEtapasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanelSeparador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelEtapa, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelEspecificacao)
                    .addComponent(jLabelElemento)
                    .addComponent(jLabelTemplate)
                    .addComponent(jLabelSaida)
                    .addComponent(jLabelGerarCodigoFonte))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanelEtapasLayout.createSequentialGroup()
                .addComponent(jLabelImagem)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanelEtapasLayout.setVerticalGroup(
            jPanelEtapasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelEtapasLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelEtapa)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelSeparador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabelEspecificacao)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelElemento)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelTemplate)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelSaida)
                .addGap(28, 28, 28)
                .addComponent(jLabelGerarCodigoFonte)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabelImagem))
        );

        jPanelDinamico.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        javax.swing.GroupLayout jPanelDinamicoLayout = new javax.swing.GroupLayout(jPanelDinamico);
        jPanelDinamico.setLayout(jPanelDinamicoLayout);
        jPanelDinamicoLayout.setHorizontalGroup(
            jPanelDinamicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPaneDinamico)
        );
        jPanelDinamicoLayout.setVerticalGroup(
            jPanelDinamicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPaneDinamico, javax.swing.GroupLayout.DEFAULT_SIZE, 317, Short.MAX_VALUE)
        );

        jButtonSair.setMnemonic('S');
        jButtonSair.setText("Sair");
        jButtonSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSairActionPerformed(evt);
            }
        });

        jButtonAnterior.setMnemonic('A');
        jButtonAnterior.setText("< Anterior");
        jButtonAnterior.setEnabled(false);
        jButtonAnterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAnteriorActionPerformed(evt);
            }
        });

        jButtonProximo.setMnemonic('P');
        jButtonProximo.setText("Próximo >");
        jButtonProximo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonProximoActionPerformed(evt);
            }
        });

        jButtonFinalizar.setMnemonic('P');
        jButtonFinalizar.setText("Finalizar");
        jButtonFinalizar.setEnabled(false);
        jButtonFinalizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonFinalizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelInferiorLayout = new javax.swing.GroupLayout(jPanelInferior);
        jPanelInferior.setLayout(jPanelInferiorLayout);
        jPanelInferiorLayout.setHorizontalGroup(
            jPanelInferiorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelInferiorLayout.createSequentialGroup()
                .addContainerGap(270, Short.MAX_VALUE)
                .addComponent(jButtonAnterior, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonProximo, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonFinalizar, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(jButtonSair, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanelInferiorLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jButtonAnterior, jButtonProximo, jButtonSair});

        jPanelInferiorLayout.setVerticalGroup(
            jPanelInferiorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelInferiorLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelInferiorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonSair)
                    .addComponent(jButtonAnterior)
                    .addComponent(jButtonProximo)
                    .addComponent(jButtonFinalizar))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanelEtapas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanelDinamico, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(jPanelInferior, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanelEtapas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanelDinamico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanelInferior, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSairActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButtonSairActionPerformed

    private void jButtonProximoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonProximoActionPerformed
        etapa++;
        avaliaEtapa();
    }//GEN-LAST:event_jButtonProximoActionPerformed

    private void jButtonAnteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAnteriorActionPerformed
        etapa--;
        avaliaEtapa();
    }//GEN-LAST:event_jButtonAnteriorActionPerformed

    private void jButtonFinalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonFinalizarActionPerformed
        nucleo.executa();
    }//GEN-LAST:event_jButtonFinalizarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAnterior;
    private javax.swing.JButton jButtonFinalizar;
    private javax.swing.JButton jButtonProximo;
    private javax.swing.JButton jButtonSair;
    private javax.swing.JLabel jLabelElemento;
    private javax.swing.JLabel jLabelEspecificacao;
    private javax.swing.JLabel jLabelEtapa;
    private javax.swing.JLabel jLabelGerarCodigoFonte;
    private javax.swing.JLabel jLabelImagem;
    private javax.swing.JLabel jLabelSaida;
    private javax.swing.JLabel jLabelTemplate;
    private javax.swing.JPanel jPanelDinamico;
    private javax.swing.JPanel jPanelEtapas;
    private javax.swing.JPanel jPanelInferior;
    private javax.swing.JPanel jPanelSeparador;
    private javax.swing.JScrollPane jScrollPaneDinamico;
    // End of variables declaration//GEN-END:variables
}
