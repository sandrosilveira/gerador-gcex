/*
 *  Copyright (C) 2012 Sandro Silveira
 *  sandrosilveira@gmail.com
 *
 *  C�digo Gerado pelo gerador de c�digo GCExp
 *
 *  Este programa foi desenvolvido para o trabalho de conclus�o de 
 *  curso apresentado como requisito parcial � obten��o do grau de
 *  Bacharel em Ci�ncia da Computa��o pela Universidade Feevale.
 *
 *  Este programa n�o pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autoriza��o do autor.
 * 
 */
package model;

import model.ItemPedido;
import model.ItemPedidoPk;
import exceptions.ItemPedidoDaoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Classe DAO (Data Access Object) para acesso a tabela ITEM_PEDIDO
 *
 * @author Sandro Silveira
 */
public class ItemPedidoDao {

    /**
     * Conex�o com o banco de dados
     */
    private Connection conexao;
    /**
     * M�todos de busca podem passar este valor para o m�todo JDBC setMaxRows
     */
    private int maxLinhas;
    /**
     * Indica se deve registrar trace de opera��es na tabela ITEM_PEDIDO
     */
    private boolean registraTrace = false;
    /**
     * Comando Select padr�o para m�todos de busca da tabela ITEM_PEDIDO
     */
    private final String SQL_SELECT = "SELECT id_pedido, id_produto, quantidade FROM ITEM_PEDIDO";
    /**
     * Comando Insert padr�o para a tabela ITEM_PEDIDO
     */
    protected final String SQL_INSERT = "INSERT INTO ITEM_PEDIDO (id_pedido, id_produto, quantidade) VALUES (?, ?, ?)";
    /**
     * Comando Update padr�o para a tabela ITEM_PEDIDO
     */
    protected final String SQL_UPDATE = "UPDATE ITEM_PEDIDO SET id_pedido = ?, id_produto = ?, quantidade = ? WHERE id_pedido = ? AND id_produto = ?";
    /**
     * Comando Delete padr�o para a tabela ITEM_PEDIDO
     */
    protected final String SQL_DELETE = "DELETE FROM ITEM_PEDIDO WHERE id_pedido = ? AND id_produto = ?";
    /**
     * Comando Delete para exclus�o de todos os dados da tabela ITEM_PEDIDO
     */
    protected final String SQL_DELETE_ALL = "DELETE FROM ITEM_PEDIDO";
    /**
     * Indice da coluna id_pedido
     */
    protected static final int COLUNA_ID_PEDIDO = 1;
    /**
     * Indice da coluna id_produto
     */
    protected static final int COLUNA_ID_PRODUTO = 2;
    /**
     * Indice da coluna quantidade
     */
    protected static final int COLUNA_QUANTIDADE = 3;
    /**
     * N�mero de colunas da tabela
     */
    protected static final int NRO_COLUNAS_TABELA = 3;

    /**
     * Construtor padr�o
     */
    public ItemPedidoaDao() {
    }
    
    /**
     * Construtor informando a conex�o com o banco de dados
     * @param conn Conex�o com o banco de dados
     */
    public ItemPedidoDao(Connection conexaoUsuario) {
        this.conexao = conexaoUsuario;
    }

    /**
     * Seta o n�mero m�ximo de colunas a retornar
     */
    public void setMaxRows(int maxRows) {
        this.maxLinhas = maxRows;
    }

    /**
     * Retorna o n�mero m�ximo de linhas a retornar
     */
    public int getMaxRows() {
        return maxLinhas;
    }

    /**
     * Indica que deve registrar trace de opera��es na tabela ITEM_PEDIDO
     */
    public void setTraceOn() {
        this.registraTrace = true;
    }

    /**
     * Indica que n�o deve registrar trace de opera��es na tabela ITEM_PEDIDO (default)
     */
    public void setTraceOn() {
        this.registraTrace = false;
    }

    /**
     * Registra trace de opera��es na tabela ITEM_PEDIDO
     * @param Texto a registrar, mostrando o passo-a-passo de execu��o
     */
    private void trace(String texto) {
        if (registraTrace) {
            System.out.println(texto);
        }
    }

    /**
     * Insere uma nova linha na tabela ITEM_PEDIDO 
     *
     * @param itemPedido Refer�ncia para o objeto a inserir
     * @return Primary Key
     * @throws ItemPedidoDaoException
     */
    public ItemPedidoPk insert(ItemPedido itemPedido) throws ItemPedidoDaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : ResourceManager.getConnection();
            stmt = conn.prepareStatement(SQL_INSERT);
            stmt.setInt(COLUNA_ID_PEDIDO, itemPedido.getIdPedido());
            stmt.setInt(COLUNA_ID_PRODUTO, itemPedido.getIdProduto());
            stmt.setShort(COLUNA_QUANTIDADE, itemPedido.getQuantidade());
            trace("Executando " + SQL_INSERT + " com: " + itemPedido.toString());
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linha inclu�da em (" + (t2 - t1) + " ms)");
            return itemPedido.createPk();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ItemPedidoDaoException("Falha ao inserir: " + ex.getMessage(), ex);
        } finally {
            ResourceManager.close(stmt);
            if (!isConnSupplied) {
                ResourceManager.close(conn);
            }
        }
    }

    /**
     * Atualiza uma linha na tabela ITEM_PEDIDO
     * @param pk Primary key
     * @param itemPedido Refer�ncia para o objeto a atualizar
     * @throws ItemPedidoDaoException 
     */
    public void update(ItemPedidoPk pk, ItemPedido itemPedido) throws ItemPedidoDaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : ResourceManager.getConnection();
            trace("Executando " + SQL_UPDATE + " com: " + itemPedido.toString());
            stmt = conn.prepareStatement(SQL_UPDATE);
            stmt.setInt(COLUNA_ID_PEDIDO, itemPedido.getIdPedido());
            stmt.setInt(COLUNA_ID_PRODUTO, itemPedido.getIdProduto());
            stmt.setShort(COLUNA_QUANTIDADE, itemPedido.getQuantidade());
            int indexKey = NRO_COLUNAS_TABELA;
            stmt.setInt(++indexKey, pk.getIdPedido());
            stmt.setInt(++indexKey, pk.getIdProduto());
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linha atualizada em (" + (t2 - t1) + " ms)");
        } catch (Exception _e) {
            _e.printStackTrace();
            throw new ItemPedidoDaoException("Falha ao atualizar: " + _e.getMessage(), _e);
        } finally {
            ResourceManager.close(stmt);
            if (!isConnSupplied) {
                ResourceManager.close(conn);
            }
        }
    }

    /**
     * Deleta um linha da tabela ITEM_PEDIDO
     * @param pk Primary Key
     * @throws ItemPedidoDaoException
     */
    public void delete(ItemPedidoPk pk) throws ItemPedidoDaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : ResourceManager.getConnection();
            trace("Executando " + SQL_DELETE + " com: " + pk.toString());
            stmt = conn.prepareStatement(SQL_DELETE);
            int indexKey = 0;
            stmt.setInt(++indexKey, pk.getIdPedido());
            stmt.setInt(++indexKey, pk.getIdProduto());
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linha exclu�da em (" + (t2 - t1) + " ms)");
        } catch (Exception _e) {
            _e.printStackTrace();
            throw new ItemPedidoDaoException("Falha ao excluir: " + _e.getMessage(), _e);
        } finally {
            ResourceManager.close(stmt);
            if (!isConnSupplied) {
                ResourceManager.close(conn);
            }
        }
    }

    /**
     * Deleta todas as linhas da tabela ITEM_PEDIDO
     * @throws ItemPedidoDaoException 
     */
    public void deleteAll() throws ItemPedidoDaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : ResourceManager.getConnection();
            trace("Executando " + SQL_DELETE_ALL);
            stmt = conn.prepareStatement(SQL_DELETE_ALL);
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linhas exclu�das em (" + (t2 - t1) + " ms)");
        } catch (Exception _e) {
            _e.printStackTrace();
            throw new ItemPedidoDaoException("Falha ao excluir todas as linhas da tabela: " + _e.getMessage(), _e);
        } finally {
            ResourceManager.close(stmt);
            if (!isConnSupplied) {
                ResourceManager.close(conn);
            }
        }
    }

    /**
     * Retorna uma linha da tabela ITEM_PEDIDO pela Primary Key
     *
     * @param pk Primary Key
     * @return Objeto do tipo ItemPedido
     * @throws ItemPedidoDaoException
     */
    public ItemPedido findByPrimaryKey(ItemPedidoPk pk) throws ItemPedidoDaoException {
        return findByPrimaryKey(pk.getIdPedido(), pk.getIdProduto());
    }

    /**
     * Retorna uma linha da tabela ITEM_PEDIDO comparando os atributos da Primary Key
     *
     * @param idPedido Identificador do pedido do cliente 
     * @param idProduto Identificador do pedido 
     * @return Array de objetos do tipo ItemPedido
     * @throws ItemPedidoDaoException
     */
    public ItemPedido findByPrimaryKey(int idPedido, int idProduto) throws ItemPedidoDaoException {
        ItemPedido itemPedidos[] = findByDynamicSelect(SQL_SELECT + " WHERE id_pedido = ? AND id_produto = ?", new Object[]{new Integer(idPedido), new Integer(idProduto)});
        return itemPedidos.length == 0 ? null : itemPedidos[0];
    }




    
}
