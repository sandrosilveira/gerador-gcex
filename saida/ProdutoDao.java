/*
 *  Copyright (C) 2012 Sandro Silveira
 *  sandrosilveira@gmail.com
 *
 *  C�digo Gerado pelo gerador de c�digo GCExp
 *
 *  Este programa foi desenvolvido para o trabalho de conclus�o de 
 *  curso apresentado como requisito parcial � obten��o do grau de
 *  Bacharel em Ci�ncia da Computa��o pela Universidade Feevale.
 *
 *  Este programa n�o pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autoriza��o do autor.
 * 
 */
package model;

import model.Produto;
import model.ProdutoPk;
import exceptions.ProdutoDaoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Classe DAO (Data Access Object) para acesso a tabela PRODUTO
 *
 * @author Sandro Silveira
 */
public class ProdutoDao {

    /**
     * Conex�o com o banco de dados
     */
    private Connection conexao;
    /**
     * M�todos de busca podem passar este valor para o m�todo JDBC setMaxRows
     */
    private int maxLinhas;
    /**
     * Indica se deve registrar trace de opera��es na tabela PRODUTO
     */
    private boolean registraTrace = false;
    /**
     * Comando Select padr�o para m�todos de busca da tabela PRODUTO
     */
    private final String SQL_SELECT = "SELECT id, nome, preco, descricao, data_alteracao, id_categoria FROM PRODUTO";
    /**
     * Comando Insert padr�o para a tabela PRODUTO
     */
    protected final String SQL_INSERT = "INSERT INTO PRODUTO (id, nome, preco, descricao, data_alteracao, id_categoria) VALUES (?, ?, ?, ?, ?, ?)";
    /**
     * Comando Update padr�o para a tabela PRODUTO
     */
    protected final String SQL_UPDATE = "UPDATE PRODUTO SET id = ?, nome = ?, preco = ?, descricao = ?, data_alteracao = ?, id_categoria = ? WHERE id = ?";
    /**
     * Comando Delete padr�o para a tabela PRODUTO
     */
    protected final String SQL_DELETE = "DELETE FROM PRODUTO WHERE id = ?";
    /**
     * Comando Delete para exclus�o de todos os dados da tabela PRODUTO
     */
    protected final String SQL_DELETE_ALL = "DELETE FROM PRODUTO";
    /**
     * Indice da coluna id
     */
    protected static final int COLUNA_ID = 1;
    /**
     * Indice da coluna nome
     */
    protected static final int COLUNA_NOME = 2;
    /**
     * Indice da coluna preco
     */
    protected static final int COLUNA_PRECO = 3;
    /**
     * Indice da coluna descricao
     */
    protected static final int COLUNA_DESCRICAO = 4;
    /**
     * Indice da coluna data_alteracao
     */
    protected static final int COLUNA_DATA_ALTERACAO = 5;
    /**
     * Indice da coluna id_categoria
     */
    protected static final int COLUNA_ID_CATEGORIA = 6;
    /**
     * N�mero de colunas da tabela
     */
    protected static final int NRO_COLUNAS_TABELA = 6;

    /**
     * Construtor padr�o
     */
    public ProdutoaDao() {
    }
    
    /**
     * Construtor informando a conex�o com o banco de dados
     * @param conn Conex�o com o banco de dados
     */
    public ProdutoDao(Connection conexaoUsuario) {
        this.conexao = conexaoUsuario;
    }

    /**
     * Seta o n�mero m�ximo de colunas a retornar
     */
    public void setMaxRows(int maxRows) {
        this.maxLinhas = maxRows;
    }

    /**
     * Retorna o n�mero m�ximo de linhas a retornar
     */
    public int getMaxRows() {
        return maxLinhas;
    }

    /**
     * Indica que deve registrar trace de opera��es na tabela PRODUTO
     */
    public void setTraceOn() {
        this.registraTrace = true;
    }

    /**
     * Indica que n�o deve registrar trace de opera��es na tabela PRODUTO (default)
     */
    public void setTraceOn() {
        this.registraTrace = false;
    }

    /**
     * Registra trace de opera��es na tabela PRODUTO
     * @param Texto a registrar, mostrando o passo-a-passo de execu��o
     */
    private void trace(String texto) {
        if (registraTrace) {
            System.out.println(texto);
        }
    }

    /**
     * Insere uma nova linha na tabela PRODUTO 
     *
     * @param produto Refer�ncia para o objeto a inserir
     * @return Primary Key
     * @throws ProdutoDaoException
     */
    public ProdutoPk insert(Produto produto) throws ProdutoDaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : ResourceManager.getConnection();
            stmt = conn.prepareStatement(SQL_INSERT);
            stmt.setInt(COLUNA_ID, produto.getId());
            stmt.setString(COLUNA_NOME, produto.getNome());
            stmt.setDecimal(COLUNA_PRECO, produto.getPreco());
            stmt.setString(COLUNA_DESCRICAO, produto.getDescricao());
            stmt.setLong(COLUNA_DATA_ALTERACAO, produto.getDataAlteracao());
            stmt.setInt(COLUNA_ID_CATEGORIA, produto.getIdCategoria());
            trace("Executando " + SQL_INSERT + " com: " + produto.toString());
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linha inclu�da em (" + (t2 - t1) + " ms)");
            return produto.createPk();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ProdutoDaoException("Falha ao inserir: " + ex.getMessage(), ex);
        } finally {
            ResourceManager.close(stmt);
            if (!isConnSupplied) {
                ResourceManager.close(conn);
            }
        }
    }

    /**
     * Atualiza uma linha na tabela PRODUTO
     * @param pk Primary key
     * @param produto Refer�ncia para o objeto a atualizar
     * @throws ProdutoDaoException 
     */
    public void update(ProdutoPk pk, Produto produto) throws ProdutoDaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : ResourceManager.getConnection();
            trace("Executando " + SQL_UPDATE + " com: " + produto.toString());
            stmt = conn.prepareStatement(SQL_UPDATE);
            stmt.setInt(COLUNA_ID, produto.getId());
            stmt.setString(COLUNA_NOME, produto.getNome());
            stmt.setDecimal(COLUNA_PRECO, produto.getPreco());
            stmt.setString(COLUNA_DESCRICAO, produto.getDescricao());
            stmt.setLong(COLUNA_DATA_ALTERACAO, produto.getDataAlteracao());
            stmt.setInt(COLUNA_ID_CATEGORIA, produto.getIdCategoria());
            int indexKey = NRO_COLUNAS_TABELA;
            stmt.setInt(++indexKey, pk.getId());
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linha atualizada em (" + (t2 - t1) + " ms)");
        } catch (Exception _e) {
            _e.printStackTrace();
            throw new ProdutoDaoException("Falha ao atualizar: " + _e.getMessage(), _e);
        } finally {
            ResourceManager.close(stmt);
            if (!isConnSupplied) {
                ResourceManager.close(conn);
            }
        }
    }

    /**
     * Deleta um linha da tabela PRODUTO
     * @param pk Primary Key
     * @throws ProdutoDaoException
     */
    public void delete(ProdutoPk pk) throws ProdutoDaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : ResourceManager.getConnection();
            trace("Executando " + SQL_DELETE + " com: " + pk.toString());
            stmt = conn.prepareStatement(SQL_DELETE);
            int indexKey = 0;
            stmt.setInt(++indexKey, pk.getId());
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linha exclu�da em (" + (t2 - t1) + " ms)");
        } catch (Exception _e) {
            _e.printStackTrace();
            throw new ProdutoDaoException("Falha ao excluir: " + _e.getMessage(), _e);
        } finally {
            ResourceManager.close(stmt);
            if (!isConnSupplied) {
                ResourceManager.close(conn);
            }
        }
    }

    /**
     * Deleta todas as linhas da tabela PRODUTO
     * @throws ProdutoDaoException 
     */
    public void deleteAll() throws ProdutoDaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : ResourceManager.getConnection();
            trace("Executando " + SQL_DELETE_ALL);
            stmt = conn.prepareStatement(SQL_DELETE_ALL);
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linhas exclu�das em (" + (t2 - t1) + " ms)");
        } catch (Exception _e) {
            _e.printStackTrace();
            throw new ProdutoDaoException("Falha ao excluir todas as linhas da tabela: " + _e.getMessage(), _e);
        } finally {
            ResourceManager.close(stmt);
            if (!isConnSupplied) {
                ResourceManager.close(conn);
            }
        }
    }

    /**
     * Retorna uma linha da tabela PRODUTO pela Primary Key
     *
     * @param pk Primary Key
     * @return Objeto do tipo Produto
     * @throws ProdutoDaoException
     */
    public Produto findByPrimaryKey(ProdutoPk pk) throws ProdutoDaoException {
        return findByPrimaryKey(pk.getId());
    }

    /**
     * Retorna uma linha da tabela PRODUTO comparando os atributos da Primary Key
     *
     * @param id  
     * @return Array de objetos do tipo Produto
     * @throws ProdutoDaoException
     */
    public Produto findByPrimaryKey(int id) throws ProdutoDaoException {
        Produto produtos[] = findByDynamicSelect(SQL_SELECT + " WHERE id = ?", new Object[]{new Integer(id)});
        return produtos.length == 0 ? null : produtos[0];
    }




    
}
