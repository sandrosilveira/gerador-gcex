/*
 * Autor: Sandro Silveira 
 *
 */
package model;

public class Pedido {

    /**
     * 
     */
    private int id;

    /**
     * Valor total do pedido
     */
    private java.math.BigDecimal total;

    /**
     * Data do pedido
     */
    private java.sql.Timestamp dataPedido;

    /**
     * N�mero da confirma��o do cliente
     */
    private int numeroConfirmacao;

    /**
     * Cliente
     */
    private int idCliente;

    /**
     * Construtor principal
     *
     */
    public Pedido() {
    }

    /**
     * Seta 
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Retorna 
     */
    public int getId() {
        return id;
    }

    /**
     * Seta Valor total do pedido
     */
    public void setTotal(java.math.BigDecimal total) {
        this.total = total;
    }

    /**
     * Retorna Valor total do pedido
     */
    public java.math.BigDecimal getTotal() {
        return total;
    }

    /**
     * Seta Data do pedido
     */
    public void setDataPedido(java.sql.Timestamp dataPedido) {
        this.dataPedido = dataPedido;
    }

    /**
     * Retorna Data do pedido
     */
    public java.sql.Timestamp getDataPedido() {
        return dataPedido;
    }

    /**
     * Seta N�mero da confirma��o do cliente
     */
    public void setNumeroConfirmacao(int numeroConfirmacao) {
        this.numeroConfirmacao = numeroConfirmacao;
    }

    /**
     * Retorna N�mero da confirma��o do cliente
     */
    public int getNumeroConfirmacao() {
        return numeroConfirmacao;
    }

    /**
     * Seta Cliente
     */
    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    /**
     * Retorna Cliente
     */
    public int getIdCliente() {
        return idCliente;
    }

    @Override
    public String toString() {
        return "Pedido{" + 
                    "id=" + id + ", " +
                    "total=" + total + ", " +
                    "dataPedido=" + dataPedido + ", " +
                    "numeroConfirmacao=" + numeroConfirmacao + ", " +
                    "idCliente=" + idCliente + "}";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pedido outroPedido = (Pedido) obj;
        if (this.id != outroPedido.id) {
            return false;
        }
        if (this.total != outroPedido.total) {
            return false;
        }
        if (this.dataPedido != outroPedido.dataPedido) {
            return false;
        }
        if (this.numeroConfirmacao != outroPedido.numeroConfirmacao) {
            return false;
        }
        if (this.idCliente != outroPedido.idCliente) {
            return false;
        }
        return true;
    }

}
