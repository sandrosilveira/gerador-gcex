/*
 *  Copyright (C) 2012 Sandro Silveira
 *  sandrosilveira@gmail.com
 *
 *  C�digo Gerado pelo gerador de c�digo GCExp
 *
 *  Este programa foi desenvolvido para o trabalho de conclus�o de 
 *  curso apresentado como requisito parcial � obten��o do grau de
 *  Bacharel em Ci�ncia da Computa��o pela Universidade Feevale.
 *
 *  Este programa n�o pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autoriza��o do autor.
 * 
 */
package model;

import model.Pedido;
import model.PedidoPk;
import exceptions.PedidoDaoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Classe DAO (Data Access Object) para acesso a tabela PEDIDO
 *
 * @author Sandro Silveira
 */
public class PedidoDao {

    /**
     * Conex�o com o banco de dados
     */
    private Connection conexao;
    /**
     * M�todos de busca podem passar este valor para o m�todo JDBC setMaxRows
     */
    private int maxLinhas;
    /**
     * Indica se deve registrar trace de opera��es na tabela PEDIDO
     */
    private boolean registraTrace = false;
    /**
     * Comando Select padr�o para m�todos de busca da tabela PEDIDO
     */
    private final String SQL_SELECT = "SELECT id, total, data_pedido, numero_confirmacao, id_cliente FROM PEDIDO";
    /**
     * Comando Insert padr�o para a tabela PEDIDO
     */
    protected final String SQL_INSERT = "INSERT INTO PEDIDO (id, total, data_pedido, numero_confirmacao, id_cliente) VALUES (?, ?, ?, ?, ?)";
    /**
     * Comando Update padr�o para a tabela PEDIDO
     */
    protected final String SQL_UPDATE = "UPDATE PEDIDO SET id = ?, total = ?, data_pedido = ?, numero_confirmacao = ?, id_cliente = ? WHERE id = ?";
    /**
     * Comando Delete padr�o para a tabela PEDIDO
     */
    protected final String SQL_DELETE = "DELETE FROM PEDIDO WHERE id = ?";
    /**
     * Comando Delete para exclus�o de todos os dados da tabela PEDIDO
     */
    protected final String SQL_DELETE_ALL = "DELETE FROM PEDIDO";
    /**
     * Indice da coluna id
     */
    protected static final int COLUNA_ID = 1;
    /**
     * Indice da coluna total
     */
    protected static final int COLUNA_TOTAL = 2;
    /**
     * Indice da coluna data_pedido
     */
    protected static final int COLUNA_DATA_PEDIDO = 3;
    /**
     * Indice da coluna numero_confirmacao
     */
    protected static final int COLUNA_NUMERO_CONFIRMACAO = 4;
    /**
     * Indice da coluna id_cliente
     */
    protected static final int COLUNA_ID_CLIENTE = 5;
    /**
     * N�mero de colunas da tabela
     */
    protected static final int NRO_COLUNAS_TABELA = 5;

    /**
     * Construtor padr�o
     */
    public PedidoaDao() {
    }
    
    /**
     * Construtor informando a conex�o com o banco de dados
     * @param conn Conex�o com o banco de dados
     */
    public PedidoDao(Connection conexaoUsuario) {
        this.conexao = conexaoUsuario;
    }

    /**
     * Seta o n�mero m�ximo de colunas a retornar
     */
    public void setMaxRows(int maxRows) {
        this.maxLinhas = maxRows;
    }

    /**
     * Retorna o n�mero m�ximo de linhas a retornar
     */
    public int getMaxRows() {
        return maxLinhas;
    }

    /**
     * Indica que deve registrar trace de opera��es na tabela PEDIDO
     */
    public void setTraceOn() {
        this.registraTrace = true;
    }

    /**
     * Indica que n�o deve registrar trace de opera��es na tabela PEDIDO (default)
     */
    public void setTraceOn() {
        this.registraTrace = false;
    }

    /**
     * Registra trace de opera��es na tabela PEDIDO
     * @param Texto a registrar, mostrando o passo-a-passo de execu��o
     */
    private void trace(String texto) {
        if (registraTrace) {
            System.out.println(texto);
        }
    }

    /**
     * Insere uma nova linha na tabela PEDIDO 
     *
     * @param pedido Refer�ncia para o objeto a inserir
     * @return Primary Key
     * @throws PedidoDaoException
     */
    public PedidoPk insert(Pedido pedido) throws PedidoDaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : ResourceManager.getConnection();
            stmt = conn.prepareStatement(SQL_INSERT);
            stmt.setInt(COLUNA_ID, pedido.getId());
            stmt.setDecimal(COLUNA_TOTAL, pedido.getTotal());
            stmt.setLong(COLUNA_DATA_PEDIDO, pedido.getDataPedido());
            stmt.setInt(COLUNA_NUMERO_CONFIRMACAO, pedido.getNumeroConfirmacao());
            stmt.setInt(COLUNA_ID_CLIENTE, pedido.getIdCliente());
            trace("Executando " + SQL_INSERT + " com: " + pedido.toString());
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linha inclu�da em (" + (t2 - t1) + " ms)");
            return pedido.createPk();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new PedidoDaoException("Falha ao inserir: " + ex.getMessage(), ex);
        } finally {
            ResourceManager.close(stmt);
            if (!isConnSupplied) {
                ResourceManager.close(conn);
            }
        }
    }

    /**
     * Atualiza uma linha na tabela PEDIDO
     * @param pk Primary key
     * @param pedido Refer�ncia para o objeto a atualizar
     * @throws PedidoDaoException 
     */
    public void update(PedidoPk pk, Pedido pedido) throws PedidoDaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : ResourceManager.getConnection();
            trace("Executando " + SQL_UPDATE + " com: " + pedido.toString());
            stmt = conn.prepareStatement(SQL_UPDATE);
            stmt.setInt(COLUNA_ID, pedido.getId());
            stmt.setDecimal(COLUNA_TOTAL, pedido.getTotal());
            stmt.setLong(COLUNA_DATA_PEDIDO, pedido.getDataPedido());
            stmt.setInt(COLUNA_NUMERO_CONFIRMACAO, pedido.getNumeroConfirmacao());
            stmt.setInt(COLUNA_ID_CLIENTE, pedido.getIdCliente());
            int indexKey = NRO_COLUNAS_TABELA;
            stmt.setInt(++indexKey, pk.getId());
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linha atualizada em (" + (t2 - t1) + " ms)");
        } catch (Exception _e) {
            _e.printStackTrace();
            throw new PedidoDaoException("Falha ao atualizar: " + _e.getMessage(), _e);
        } finally {
            ResourceManager.close(stmt);
            if (!isConnSupplied) {
                ResourceManager.close(conn);
            }
        }
    }

    /**
     * Deleta um linha da tabela PEDIDO
     * @param pk Primary Key
     * @throws PedidoDaoException
     */
    public void delete(PedidoPk pk) throws PedidoDaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : ResourceManager.getConnection();
            trace("Executando " + SQL_DELETE + " com: " + pk.toString());
            stmt = conn.prepareStatement(SQL_DELETE);
            int indexKey = 0;
            stmt.setInt(++indexKey, pk.getId());
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linha exclu�da em (" + (t2 - t1) + " ms)");
        } catch (Exception _e) {
            _e.printStackTrace();
            throw new PedidoDaoException("Falha ao excluir: " + _e.getMessage(), _e);
        } finally {
            ResourceManager.close(stmt);
            if (!isConnSupplied) {
                ResourceManager.close(conn);
            }
        }
    }

    /**
     * Deleta todas as linhas da tabela PEDIDO
     * @throws PedidoDaoException 
     */
    public void deleteAll() throws PedidoDaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : ResourceManager.getConnection();
            trace("Executando " + SQL_DELETE_ALL);
            stmt = conn.prepareStatement(SQL_DELETE_ALL);
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linhas exclu�das em (" + (t2 - t1) + " ms)");
        } catch (Exception _e) {
            _e.printStackTrace();
            throw new PedidoDaoException("Falha ao excluir todas as linhas da tabela: " + _e.getMessage(), _e);
        } finally {
            ResourceManager.close(stmt);
            if (!isConnSupplied) {
                ResourceManager.close(conn);
            }
        }
    }

    /**
     * Retorna uma linha da tabela PEDIDO pela Primary Key
     *
     * @param pk Primary Key
     * @return Objeto do tipo Pedido
     * @throws PedidoDaoException
     */
    public Pedido findByPrimaryKey(PedidoPk pk) throws PedidoDaoException {
        return findByPrimaryKey(pk.getId());
    }

    /**
     * Retorna uma linha da tabela PEDIDO comparando os atributos da Primary Key
     *
     * @param id  
     * @return Array de objetos do tipo Pedido
     * @throws PedidoDaoException
     */
    public Pedido findByPrimaryKey(int id) throws PedidoDaoException {
        Pedido pedidos[] = findByDynamicSelect(SQL_SELECT + " WHERE id = ?", new Object[]{new Integer(id)});
        return pedidos.length == 0 ? null : pedidos[0];
    }




    
}
