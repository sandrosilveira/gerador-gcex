/*
 * Autor: Sandro Silveira 
 *
 */
package model;

public class Categoria {

    /**
     * 
     */
    private int id;

    /**
     * Descri��o da categoria de produtos
     */
    private String descricao;

    /**
     * Construtor principal
     *
     */
    public Categoria() {
    }

    /**
     * Seta 
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Retorna 
     */
    public int getId() {
        return id;
    }

    /**
     * Seta Descri��o da categoria de produtos
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * Retorna Descri��o da categoria de produtos
     */
    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return "Categoria{" + 
                    "id=" + id + ", " +
                    "descricao=" + descricao + "}";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Categoria outroCategoria = (Categoria) obj;
        if (this.id != outroCategoria.id) {
            return false;
        }
        if (this.descricao != outroCategoria.descricao) {
            return false;
        }
        return true;
    }

}
