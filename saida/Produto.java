/*
 * Autor: Sandro Silveira 
 *
 */
package model;

public class Produto {

    /**
     * 
     */
    private int id;

    /**
     * Nome completo
     */
    private String nome;

    /**
     * Pre�o de venda
     */
    private java.math.BigDecimal preco;

    /**
     * Descri��o detalhada
     */
    private String descricao;

    /**
     * Data da �ltima altera��o
     */
    private java.sql.Timestamp dataAlteracao;

    /**
     * Categoria
     */
    private int idCategoria;

    /**
     * Construtor principal
     *
     */
    public Produto() {
    }

    /**
     * Seta 
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Retorna 
     */
    public int getId() {
        return id;
    }

    /**
     * Seta Nome completo
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Retorna Nome completo
     */
    public String getNome() {
        return nome;
    }

    /**
     * Seta Pre�o de venda
     */
    public void setPreco(java.math.BigDecimal preco) {
        this.preco = preco;
    }

    /**
     * Retorna Pre�o de venda
     */
    public java.math.BigDecimal getPreco() {
        return preco;
    }

    /**
     * Seta Descri��o detalhada
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * Retorna Descri��o detalhada
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Seta Data da �ltima altera��o
     */
    public void setDataAlteracao(java.sql.Timestamp dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }

    /**
     * Retorna Data da �ltima altera��o
     */
    public java.sql.Timestamp getDataAlteracao() {
        return dataAlteracao;
    }

    /**
     * Seta Categoria
     */
    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    /**
     * Retorna Categoria
     */
    public int getIdCategoria() {
        return idCategoria;
    }

    @Override
    public String toString() {
        return "Produto{" + 
                    "id=" + id + ", " +
                    "nome=" + nome + ", " +
                    "preco=" + preco + ", " +
                    "descricao=" + descricao + ", " +
                    "dataAlteracao=" + dataAlteracao + ", " +
                    "idCategoria=" + idCategoria + "}";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Produto outroProduto = (Produto) obj;
        if (this.id != outroProduto.id) {
            return false;
        }
        if (this.nome != outroProduto.nome) {
            return false;
        }
        if (this.preco != outroProduto.preco) {
            return false;
        }
        if (this.descricao != outroProduto.descricao) {
            return false;
        }
        if (this.dataAlteracao != outroProduto.dataAlteracao) {
            return false;
        }
        if (this.idCategoria != outroProduto.idCategoria) {
            return false;
        }
        return true;
    }

}
