/*
 * Autor: Sandro Silveira 
 *
 */
package model;

public class ItemPedido {

    /**
     * Identificador do pedido do cliente
     */
    private int idPedido;

    /**
     * Identificador do pedido
     */
    private int idProduto;

    /**
     * Quantidade vendida do produto
     */
    private short quantidade;

    /**
     * Construtor principal
     *
     */
    public ItemPedido() {
    }

    /**
     * Seta Identificador do pedido do cliente
     */
    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    /**
     * Retorna Identificador do pedido do cliente
     */
    public int getIdPedido() {
        return idPedido;
    }

    /**
     * Seta Identificador do pedido
     */
    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    /**
     * Retorna Identificador do pedido
     */
    public int getIdProduto() {
        return idProduto;
    }

    /**
     * Seta Quantidade vendida do produto
     */
    public void setQuantidade(short quantidade) {
        this.quantidade = quantidade;
    }

    /**
     * Retorna Quantidade vendida do produto
     */
    public short getQuantidade() {
        return quantidade;
    }

    @Override
    public String toString() {
        return "ItemPedido{" + 
                    "idPedido=" + idPedido + ", " +
                    "idProduto=" + idProduto + ", " +
                    "quantidade=" + quantidade + "}";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ItemPedido outroItemPedido = (ItemPedido) obj;
        if (this.idPedido != outroItemPedido.idPedido) {
            return false;
        }
        if (this.idProduto != outroItemPedido.idProduto) {
            return false;
        }
        if (this.quantidade != outroItemPedido.quantidade) {
            return false;
        }
        return true;
    }

}
