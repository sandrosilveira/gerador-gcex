/*
 * Autor: Sandro Silveira 
 *
 */
package model;

public class Cliente {

    /**
     * 
     */
    private int id;

    /**
     * Nome completo ou raz�o social
     */
    private String nome;

    /**
     * Endere�o de e-mail
     */
    private String email;

    /**
     * N�mero de telefone
     */
    private String telefone;

    /**
     * Endere�o
     */
    private String endereco;

    /**
     * Regi�o
     */
    private String regiao;

    /**
     * N�mero do cart�o de cr�dito
     */
    private String numeroCc;

    /**
     * Construtor principal
     *
     */
    public Cliente() {
    }

    /**
     * Seta 
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Retorna 
     */
    public int getId() {
        return id;
    }

    /**
     * Seta Nome completo ou raz�o social
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Retorna Nome completo ou raz�o social
     */
    public String getNome() {
        return nome;
    }

    /**
     * Seta Endere�o de e-mail
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Retorna Endere�o de e-mail
     */
    public String getEmail() {
        return email;
    }

    /**
     * Seta N�mero de telefone
     */
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    /**
     * Retorna N�mero de telefone
     */
    public String getTelefone() {
        return telefone;
    }

    /**
     * Seta Endere�o
     */
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    /**
     * Retorna Endere�o
     */
    public String getEndereco() {
        return endereco;
    }

    /**
     * Seta Regi�o
     */
    public void setRegiao(String regiao) {
        this.regiao = regiao;
    }

    /**
     * Retorna Regi�o
     */
    public String getRegiao() {
        return regiao;
    }

    /**
     * Seta N�mero do cart�o de cr�dito
     */
    public void setNumeroCc(String numeroCc) {
        this.numeroCc = numeroCc;
    }

    /**
     * Retorna N�mero do cart�o de cr�dito
     */
    public String getNumeroCc() {
        return numeroCc;
    }

    @Override
    public String toString() {
        return "Cliente{" + 
                    "id=" + id + ", " +
                    "nome=" + nome + ", " +
                    "email=" + email + ", " +
                    "telefone=" + telefone + ", " +
                    "endereco=" + endereco + ", " +
                    "regiao=" + regiao + ", " +
                    "numeroCc=" + numeroCc + "}";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente outroCliente = (Cliente) obj;
        if (this.id != outroCliente.id) {
            return false;
        }
        if (this.nome != outroCliente.nome) {
            return false;
        }
        if (this.email != outroCliente.email) {
            return false;
        }
        if (this.telefone != outroCliente.telefone) {
            return false;
        }
        if (this.endereco != outroCliente.endereco) {
            return false;
        }
        if (this.regiao != outroCliente.regiao) {
            return false;
        }
        if (this.numeroCc != outroCliente.numeroCc) {
            return false;
        }
        return true;
    }

}
