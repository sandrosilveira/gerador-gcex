/*
 *  Copyright (C) 2012 Sandro Silveira
 *  sandrosilveira@gmail.com
 *
 *  C�digo Gerado pelo gerador de c�digo GCExp
 *
 *  Este programa foi desenvolvido para o trabalho de conclus�o de 
 *  curso apresentado como requisito parcial � obten��o do grau de
 *  Bacharel em Ci�ncia da Computa��o pela Universidade Feevale.
 *
 *  Este programa n�o pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autoriza��o do autor.
 * 
 */
package model;

import model.Categoria;
import model.CategoriaPk;
import exceptions.CategoriaDaoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Classe DAO (Data Access Object) para acesso a tabela CATEGORIA
 *
 * @author Sandro Silveira
 */
public class CategoriaDao {

    /**
     * Conex�o com o banco de dados
     */
    private Connection conexao;
    /**
     * M�todos de busca podem passar este valor para o m�todo JDBC setMaxRows
     */
    private int maxLinhas;
    /**
     * Indica se deve registrar trace de opera��es na tabela CATEGORIA
     */
    private boolean registraTrace = false;
    /**
     * Comando Select padr�o para m�todos de busca da tabela CATEGORIA
     */
    private final String SQL_SELECT = "SELECT id, descricao FROM CATEGORIA";
    /**
     * Comando Insert padr�o para a tabela CATEGORIA
     */
    protected final String SQL_INSERT = "INSERT INTO CATEGORIA (id, descricao) VALUES (?, ?)";
    /**
     * Comando Update padr�o para a tabela CATEGORIA
     */
    protected final String SQL_UPDATE = "UPDATE CATEGORIA SET id = ?, descricao = ? WHERE id = ?";
    /**
     * Comando Delete padr�o para a tabela CATEGORIA
     */
    protected final String SQL_DELETE = "DELETE FROM CATEGORIA WHERE id = ?";
    /**
     * Comando Delete para exclus�o de todos os dados da tabela CATEGORIA
     */
    protected final String SQL_DELETE_ALL = "DELETE FROM CATEGORIA";
    /**
     * Indice da coluna id
     */
    protected static final int COLUNA_ID = 1;
    /**
     * Indice da coluna descricao
     */
    protected static final int COLUNA_DESCRICAO = 2;
    /**
     * N�mero de colunas da tabela
     */
    protected static final int NRO_COLUNAS_TABELA = 2;

    /**
     * Construtor padr�o
     */
    public CategoriaaDao() {
    }
    
    /**
     * Construtor informando a conex�o com o banco de dados
     * @param conn Conex�o com o banco de dados
     */
    public CategoriaDao(Connection conexaoUsuario) {
        this.conexao = conexaoUsuario;
    }

    /**
     * Seta o n�mero m�ximo de colunas a retornar
     */
    public void setMaxRows(int maxRows) {
        this.maxLinhas = maxRows;
    }

    /**
     * Retorna o n�mero m�ximo de linhas a retornar
     */
    public int getMaxRows() {
        return maxLinhas;
    }

    /**
     * Indica que deve registrar trace de opera��es na tabela CATEGORIA
     */
    public void setTraceOn() {
        this.registraTrace = true;
    }

    /**
     * Indica que n�o deve registrar trace de opera��es na tabela CATEGORIA (default)
     */
    public void setTraceOn() {
        this.registraTrace = false;
    }

    /**
     * Registra trace de opera��es na tabela CATEGORIA
     * @param Texto a registrar, mostrando o passo-a-passo de execu��o
     */
    private void trace(String texto) {
        if (registraTrace) {
            System.out.println(texto);
        }
    }

    /**
     * Insere uma nova linha na tabela CATEGORIA 
     *
     * @param categoria Refer�ncia para o objeto a inserir
     * @return Primary Key
     * @throws CategoriaDaoException
     */
    public CategoriaPk insert(Categoria categoria) throws CategoriaDaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : ResourceManager.getConnection();
            stmt = conn.prepareStatement(SQL_INSERT);
            stmt.setInt(COLUNA_ID, categoria.getId());
            stmt.setString(COLUNA_DESCRICAO, categoria.getDescricao());
            trace("Executando " + SQL_INSERT + " com: " + categoria.toString());
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linha inclu�da em (" + (t2 - t1) + " ms)");
            return categoria.createPk();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new CategoriaDaoException("Falha ao inserir: " + ex.getMessage(), ex);
        } finally {
            ResourceManager.close(stmt);
            if (!isConnSupplied) {
                ResourceManager.close(conn);
            }
        }
    }

    /**
     * Atualiza uma linha na tabela CATEGORIA
     * @param pk Primary key
     * @param categoria Refer�ncia para o objeto a atualizar
     * @throws CategoriaDaoException 
     */
    public void update(CategoriaPk pk, Categoria categoria) throws CategoriaDaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : ResourceManager.getConnection();
            trace("Executando " + SQL_UPDATE + " com: " + categoria.toString());
            stmt = conn.prepareStatement(SQL_UPDATE);
            stmt.setInt(COLUNA_ID, categoria.getId());
            stmt.setString(COLUNA_DESCRICAO, categoria.getDescricao());
            int indexKey = NRO_COLUNAS_TABELA;
            stmt.setInt(++indexKey, pk.getId());
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linha atualizada em (" + (t2 - t1) + " ms)");
        } catch (Exception _e) {
            _e.printStackTrace();
            throw new CategoriaDaoException("Falha ao atualizar: " + _e.getMessage(), _e);
        } finally {
            ResourceManager.close(stmt);
            if (!isConnSupplied) {
                ResourceManager.close(conn);
            }
        }
    }

    /**
     * Deleta um linha da tabela CATEGORIA
     * @param pk Primary Key
     * @throws CategoriaDaoException
     */
    public void delete(CategoriaPk pk) throws CategoriaDaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : ResourceManager.getConnection();
            trace("Executando " + SQL_DELETE + " com: " + pk.toString());
            stmt = conn.prepareStatement(SQL_DELETE);
            int indexKey = 0;
            stmt.setInt(++indexKey, pk.getId());
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linha exclu�da em (" + (t2 - t1) + " ms)");
        } catch (Exception _e) {
            _e.printStackTrace();
            throw new CategoriaDaoException("Falha ao excluir: " + _e.getMessage(), _e);
        } finally {
            ResourceManager.close(stmt);
            if (!isConnSupplied) {
                ResourceManager.close(conn);
            }
        }
    }

    /**
     * Deleta todas as linhas da tabela CATEGORIA
     * @throws CategoriaDaoException 
     */
    public void deleteAll() throws CategoriaDaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : ResourceManager.getConnection();
            trace("Executando " + SQL_DELETE_ALL);
            stmt = conn.prepareStatement(SQL_DELETE_ALL);
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linhas exclu�das em (" + (t2 - t1) + " ms)");
        } catch (Exception _e) {
            _e.printStackTrace();
            throw new CategoriaDaoException("Falha ao excluir todas as linhas da tabela: " + _e.getMessage(), _e);
        } finally {
            ResourceManager.close(stmt);
            if (!isConnSupplied) {
                ResourceManager.close(conn);
            }
        }
    }

    /**
     * Retorna uma linha da tabela CATEGORIA pela Primary Key
     *
     * @param pk Primary Key
     * @return Objeto do tipo Categoria
     * @throws CategoriaDaoException
     */
    public Categoria findByPrimaryKey(CategoriaPk pk) throws CategoriaDaoException {
        return findByPrimaryKey(pk.getId());
    }

    /**
     * Retorna uma linha da tabela CATEGORIA comparando os atributos da Primary Key
     *
     * @param id  
     * @return Array de objetos do tipo Categoria
     * @throws CategoriaDaoException
     */
    public Categoria findByPrimaryKey(int id) throws CategoriaDaoException {
        Categoria categorias[] = findByDynamicSelect(SQL_SELECT + " WHERE id = ?", new Object[]{new Integer(id)});
        return categorias.length == 0 ? null : categorias[0];
    }




    
}
