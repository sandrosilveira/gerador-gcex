/*
 * Autor: Sandro Silveira
 *
 */
package model;

public class Livro {

    /**
     * Título do livro
     */
    private String titulo;

    /**
     * Autor do livro
     */
    private Autor autor;

    /**
     * Editora do livro
     */
    private Editora editora;

    /**
     * Local de edição
     */
    private String local;

    /**
     * Ano de publicação
     */
    public int ano;

    /**
     * Número de páginas
     */
    private int paginas;

    /**
     * Se está disponível para empréstimo
     */
    private boolean disponivel;

    /**
     * Seta título do livro
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * Retorna título do livro
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * Seta autor do livro
     */
    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    /**
     * Retorna autor do livro
     */
    public Autor getAutor() {
        return autor;
    }

    /**
     * Seta editora do livro
     */
    public void setEditora(Editora editora) {
        this.editora = editora;
    }

    /**
     * Retorna editora do livro
     */
    public Editora getEditora() {
        return editora;
    }

    /**
     * Seta local de edição
     */
    public void setLocal(String local) {
        this.local = local;
    }

    /**
     * Retorna local de edição
     */
    public String getLocal() {
        return local;
    }

    /**
     * Seta ano de publicação
     */
    public void setAno(int ano) {
        this.ano = ano;
    }

    /**
     * Retorna ano de publicação
     */
    public int getAno() {
        return ano;
    }

    /**
     * Seta número de páginas
     */
    public void setPaginas(int paginas) {
        this.paginas = paginas;
    }

    /**
     * Retorna número de páginas
     */
    public int getPaginas() {
        return paginas;
    }

    /**
     * Seta se está disponível para empréstimo
     */
    public void setDisponivel(boolean disponivel) {
        this.disponivel = disponivel;
    }

    /**
     * Retorna verdadeiro se está disponível para empréstimo
     */
    public boolean isDisponivel() {
        return disponivel;
    }

}
