/*
 * Autor: Sandro Silveira
 *
 */
package model;

public class Editora {

    /**
     * Nome da editora
     */
    private String nome;

    /**
     * Cidade que a editora está sediada
     */
    private String cidade;

    /**
     * Unidade da federação que a editora está sediada
     */
    private String uf;

    /**
     * Seta nome da editora
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Retorna nome da editora
     */
    public String getNome() {
        return nome;
    }

    /**
     * Seta cidade que a editora está sediada
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * Retorna cidade que a editora está sediada
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * Seta unidade da federação que a editora está sediada
     */
    public void setUf(String uf) {
        this.uf = uf;
    }

    /**
     * Retorna unidade da federação que a editora está sediada
     */
    public String getUf() {
        return uf;
    }

}
