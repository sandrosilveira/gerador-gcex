/*
 *  Copyright (C) 2012 Sandro Silveira
 *  sandrosilveira@gmail.com
 *
 *  C�digo Gerado pelo gerador de c�digo GCExp
 *
 *  Este programa foi desenvolvido para o trabalho de conclus�o de 
 *  curso apresentado como requisito parcial � obten��o do grau de
 *  Bacharel em Ci�ncia da Computa��o pela Universidade Feevale.
 *
 *  Este programa n�o pode ser utilizado, distribuido ou modificado
 *  ao todo ou em parte sem a expressa autoriza��o do autor.
 * 
 */
package model;

import model.Cliente;
import model.ClientePk;
import exceptions.ClienteDaoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Classe DAO (Data Access Object) para acesso a tabela CLIENTE
 *
 * @author Sandro Silveira
 */
public class ClienteDao {

    /**
     * Conex�o com o banco de dados
     */
    private Connection conexao;
    /**
     * M�todos de busca podem passar este valor para o m�todo JDBC setMaxRows
     */
    private int maxLinhas;
    /**
     * Indica se deve registrar trace de opera��es na tabela CLIENTE
     */
    private boolean registraTrace = false;
    /**
     * Comando Select padr�o para m�todos de busca da tabela CLIENTE
     */
    private final String SQL_SELECT = "SELECT id, nome, email, telefone, endereco, regiao, numero_cc FROM CLIENTE";
    /**
     * Comando Insert padr�o para a tabela CLIENTE
     */
    protected final String SQL_INSERT = "INSERT INTO CLIENTE (id, nome, email, telefone, endereco, regiao, numero_cc) VALUES (?, ?, ?, ?, ?, ?, ?)";
    /**
     * Comando Update padr�o para a tabela CLIENTE
     */
    protected final String SQL_UPDATE = "UPDATE CLIENTE SET id = ?, nome = ?, email = ?, telefone = ?, endereco = ?, regiao = ?, numero_cc = ? WHERE id = ?";
    /**
     * Comando Delete padr�o para a tabela CLIENTE
     */
    protected final String SQL_DELETE = "DELETE FROM CLIENTE WHERE id = ?";
    /**
     * Comando Delete para exclus�o de todos os dados da tabela CLIENTE
     */
    protected final String SQL_DELETE_ALL = "DELETE FROM CLIENTE";
    /**
     * Indice da coluna id
     */
    protected static final int COLUNA_ID = 1;
    /**
     * Indice da coluna nome
     */
    protected static final int COLUNA_NOME = 2;
    /**
     * Indice da coluna email
     */
    protected static final int COLUNA_EMAIL = 3;
    /**
     * Indice da coluna telefone
     */
    protected static final int COLUNA_TELEFONE = 4;
    /**
     * Indice da coluna endereco
     */
    protected static final int COLUNA_ENDERECO = 5;
    /**
     * Indice da coluna regiao
     */
    protected static final int COLUNA_REGIAO = 6;
    /**
     * Indice da coluna numero_cc
     */
    protected static final int COLUNA_NUMERO_CC = 7;
    /**
     * N�mero de colunas da tabela
     */
    protected static final int NRO_COLUNAS_TABELA = 7;

    /**
     * Construtor padr�o
     */
    public ClienteaDao() {
    }
    
    /**
     * Construtor informando a conex�o com o banco de dados
     * @param conn Conex�o com o banco de dados
     */
    public ClienteDao(Connection conexaoUsuario) {
        this.conexao = conexaoUsuario;
    }

    /**
     * Seta o n�mero m�ximo de colunas a retornar
     */
    public void setMaxRows(int maxRows) {
        this.maxLinhas = maxRows;
    }

    /**
     * Retorna o n�mero m�ximo de linhas a retornar
     */
    public int getMaxRows() {
        return maxLinhas;
    }

    /**
     * Indica que deve registrar trace de opera��es na tabela CLIENTE
     */
    public void setTraceOn() {
        this.registraTrace = true;
    }

    /**
     * Indica que n�o deve registrar trace de opera��es na tabela CLIENTE (default)
     */
    public void setTraceOn() {
        this.registraTrace = false;
    }

    /**
     * Registra trace de opera��es na tabela CLIENTE
     * @param Texto a registrar, mostrando o passo-a-passo de execu��o
     */
    private void trace(String texto) {
        if (registraTrace) {
            System.out.println(texto);
        }
    }

    /**
     * Insere uma nova linha na tabela CLIENTE 
     *
     * @param cliente Refer�ncia para o objeto a inserir
     * @return Primary Key
     * @throws ClienteDaoException
     */
    public ClientePk insert(Cliente cliente) throws ClienteDaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : ResourceManager.getConnection();
            stmt = conn.prepareStatement(SQL_INSERT);
            stmt.setInt(COLUNA_ID, cliente.getId());
            stmt.setString(COLUNA_NOME, cliente.getNome());
            stmt.setString(COLUNA_EMAIL, cliente.getEmail());
            stmt.setString(COLUNA_TELEFONE, cliente.getTelefone());
            stmt.setString(COLUNA_ENDERECO, cliente.getEndereco());
            stmt.setString(COLUNA_REGIAO, cliente.getRegiao());
            stmt.setString(COLUNA_NUMERO_CC, cliente.getNumeroCc());
            trace("Executando " + SQL_INSERT + " com: " + cliente.toString());
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linha inclu�da em (" + (t2 - t1) + " ms)");
            return cliente.createPk();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ClienteDaoException("Falha ao inserir: " + ex.getMessage(), ex);
        } finally {
            ResourceManager.close(stmt);
            if (!isConnSupplied) {
                ResourceManager.close(conn);
            }
        }
    }

    /**
     * Atualiza uma linha na tabela CLIENTE
     * @param pk Primary key
     * @param cliente Refer�ncia para o objeto a atualizar
     * @throws ClienteDaoException 
     */
    public void update(ClientePk pk, Cliente cliente) throws ClienteDaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : ResourceManager.getConnection();
            trace("Executando " + SQL_UPDATE + " com: " + cliente.toString());
            stmt = conn.prepareStatement(SQL_UPDATE);
            stmt.setInt(COLUNA_ID, cliente.getId());
            stmt.setString(COLUNA_NOME, cliente.getNome());
            stmt.setString(COLUNA_EMAIL, cliente.getEmail());
            stmt.setString(COLUNA_TELEFONE, cliente.getTelefone());
            stmt.setString(COLUNA_ENDERECO, cliente.getEndereco());
            stmt.setString(COLUNA_REGIAO, cliente.getRegiao());
            stmt.setString(COLUNA_NUMERO_CC, cliente.getNumeroCc());
            int indexKey = NRO_COLUNAS_TABELA;
            stmt.setInt(++indexKey, pk.getId());
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linha atualizada em (" + (t2 - t1) + " ms)");
        } catch (Exception _e) {
            _e.printStackTrace();
            throw new ClienteDaoException("Falha ao atualizar: " + _e.getMessage(), _e);
        } finally {
            ResourceManager.close(stmt);
            if (!isConnSupplied) {
                ResourceManager.close(conn);
            }
        }
    }

    /**
     * Deleta um linha da tabela CLIENTE
     * @param pk Primary Key
     * @throws ClienteDaoException
     */
    public void delete(ClientePk pk) throws ClienteDaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : ResourceManager.getConnection();
            trace("Executando " + SQL_DELETE + " com: " + pk.toString());
            stmt = conn.prepareStatement(SQL_DELETE);
            int indexKey = 0;
            stmt.setInt(++indexKey, pk.getId());
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linha exclu�da em (" + (t2 - t1) + " ms)");
        } catch (Exception _e) {
            _e.printStackTrace();
            throw new ClienteDaoException("Falha ao excluir: " + _e.getMessage(), _e);
        } finally {
            ResourceManager.close(stmt);
            if (!isConnSupplied) {
                ResourceManager.close(conn);
            }
        }
    }

    /**
     * Deleta todas as linhas da tabela CLIENTE
     * @throws ClienteDaoException 
     */
    public void deleteAll() throws ClienteDaoException {
        long t1 = System.currentTimeMillis();
        final boolean isConnSupplied = (conexao != null);
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = isConnSupplied ? this.conexao : ResourceManager.getConnection();
            trace("Executando " + SQL_DELETE_ALL);
            stmt = conn.prepareStatement(SQL_DELETE_ALL);
            int rows = stmt.executeUpdate();
            long t2 = System.currentTimeMillis();
            trace(rows + " linhas exclu�das em (" + (t2 - t1) + " ms)");
        } catch (Exception _e) {
            _e.printStackTrace();
            throw new ClienteDaoException("Falha ao excluir todas as linhas da tabela: " + _e.getMessage(), _e);
        } finally {
            ResourceManager.close(stmt);
            if (!isConnSupplied) {
                ResourceManager.close(conn);
            }
        }
    }

    /**
     * Retorna uma linha da tabela CLIENTE pela Primary Key
     *
     * @param pk Primary Key
     * @return Objeto do tipo Cliente
     * @throws ClienteDaoException
     */
    public Cliente findByPrimaryKey(ClientePk pk) throws ClienteDaoException {
        return findByPrimaryKey(pk.getId());
    }

    /**
     * Retorna uma linha da tabela CLIENTE comparando os atributos da Primary Key
     *
     * @param id  
     * @return Array de objetos do tipo Cliente
     * @throws ClienteDaoException
     */
    public Cliente findByPrimaryKey(int id) throws ClienteDaoException {
        Cliente clientes[] = findByDynamicSelect(SQL_SELECT + " WHERE id = ?", new Object[]{new Integer(id)});
        return clientes.length == 0 ? null : clientes[0];
    }




    
}
